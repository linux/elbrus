
/* òÅÄÁËÃÉÑ ÆÁÊÌÁ mcap_io.h:
			éí÷ó - 10.02.05; home - 16.05.04 
01.02.05 ×ÏÚ×ÒÁÔ Ë ×ÅÒÓÉÉ ÄÒÁÊ×ÅÒÁ íð .05 ÏÔ 29.06.04			
10.02.05 ××ÏÄ ×ÅÒÓÉÉ ÄÒÁÊ×ÅÒÁ íð .06 ÏÔ 10.02.05
13.03.05 ××ÏÄ ×ÅÒÓÉÉ ÄÒÁÊ×ÅÒÁ íð .07 ÏÔ 04.03.05				
*/

/* ïÐÒÅÄÅÌÅÎÉÑ É ÓÔÒÕËÔÕÒÙ, ÉÓÐÏÌØÚÕÅÍÙÅ ÄÒÁÊ×ÅÒÏÍ ÷ë
  É ÐÏÌØÚÏ×ÁÔÅÌØÓËÉÍÉ ÐÒÏÇÒÁÍÍÁÍÉ */

#ifndef	_UAPI__LINUX_MCAP_IO_H__
#define	_UAPI__LINUX_MCAP_IO_H__

#ifdef	__cplusplus
extern "C" {
#endif

#include <linux/mcst/linux_me90_io.h>

/* ÷ÅÒÓÉÑ ÍÏÄÕÌÑ */
#ifdef MCAP_OLD_VERSION
#define	VERSION_MODULE_MC19		0x04030507
/* òÁÂÏÞÉÅ ×ÁÒÉÁÎÔÙ */
#define	work_var_drv_MP			10
#else
#define VERSION_MODULE_MCAP             0x14010608
/* òÁÂÏÞÉÅ ×ÁÒÉÁÎÔÙ */
#define work_var_drv_MP                 11
#endif /* MCAP_OLD_VERSION */
#ifdef MCAP_OLD_VERSION
#define	work_var_module			work_var_drv_MP
#else
#define	work_var_module_MCAP		work_var_drv_MP	
#endif /* MCAP_OLD_VERSION */

/* óÐÉÓÏË ËÏÍÁÎÄ ÒÅÁÌÉÚÏ×ÁÎÎÙÈ × ÄÒÁÊ×ÅÒÅ ÍÏÄÕÌÑ MCAP É ÉÓÐÏÌÎÑÅÍÙÈ
   ÐÏÓÒÅÄÓÔ×ÏÍ ÓÉÓÔÅÍÎÏÇÏ ×ÙÚÏ×Á ioctl()
*/

#ifdef MCAP_OLD_VERSION
#define MCAP_IO			('M' << 8)
#define	MCAPIO_READ_DEVICE_REG			(MCAP_IO | 1)
#define	MCAPIO_WRITE_DEVICE_REG			(MCAP_IO | 2)
#define	MCAPIO_INIT_BUFERS_EXCHANGE		(MCAP_IO | 3)
#define	MCAPIO_HALT_TRANSFER_MODES		(MCAP_IO | 4)
#define	MCAPIO_GET_DEVICE_INFO			(MCAP_IO | 5)
#define	MCAPIO_INTR_TIME_WAIT			(MCAP_IO | 6)
#define	MCAPIO_MESSAGE_NOTE			(MCAP_IO | 7)

#define MCAP_CNTR_ST_REG_SET_OFFSET		0x10000 /* óÍÅÝÅÎÉÅ ÏÂÌÁÓÔÉ ÒÅÇÉÓÔÒÏ× */
#define MCAP_CNTR_ST_REG_SET_LEN       		0x100	/* òÁÚÍÅÒ ÏÂÌÁÓÔÉ ÒÅÇÉÓÔÒÏ× */
#define	MCAP_BMEM_REG_SET_OFFSET		0x40000 /* óÍÅÝÅÎÉÅ ÏÓÎÏ×ÎÏÊ ÐÁÍÑÔÉ */
#define	MCAP_BMEM_REG_SET_LEN			0x20000 /* òÁÚÍÅÒ ÏÂÌÁÓÔÉ ÏÓÎÏ×ÎÏÊ ÐÁÍÑÔÉ */
#define MCAP_DRV_CMN_AREA_BMEM_ADDR		0x01990	/* îÁÞÁÌØÎÙÊ ÁÄÒÅÓ ÕÐÒÁ×ÌÑÀÝÅÊ */
							/* ÉÎÆÏÒÍÁÃÉÉ ÂÕÆÅÒÏ× ÄÁÎÎÙÈ */
#define MCAP_AREA_BMEM_ADDR			0xD0	/* îÁÞÁÌØÎÙÊ ÁÄÒÅÓ ÉÎÆÏÒÍÁÃÉÉ */
							/* âïúõ */
#else
#define MCAP_IO                 ('M' << 8)
#define MCAPIO_LOAD_MP_DRV_CODE                 (MCAP_IO | 1)
#define MCAPIO_STARTUP_MP_DRV                   (MCAP_IO | 2)
#define MCAPIO_STARTUP_MP_ROM_DRV               (MCAP_IO | 3)
#define MCAPIO_RESET_MP                         (MCAP_IO | 4)
#define MCAPIO_GET_DRIVER_INFO                  (MCAP_IO | 5)
/************************************************************************/
#define MCAPIO_READ_DEVICE_REG                  (MCAP_IO | 6)
#define MCAPIO_WRITE_DEVICE_REG                 (MCAP_IO | 7)
#define MCAPIO_INIT_BUFERS_EXCHANGE             (MCAP_IO | 8)
#define MCAPIO_HALT_TRANSFER_MODES              (MCAP_IO | 9)
#define MCAPIO_GET_DEVICE_INFO                  (MCAP_IO | 10)
#define MCAPIO_INTR_TIME_WAIT                   (MCAP_IO | 11)
#define MCAPIO_MESSAGE_NOTE                     (MCAP_IO | 12)
/************************************************************************/
#define MCAPIO_NUM_INTR_ROSH                    (MCAP_IO | 13)
                                                                                                              
#define MCAP_ROOT_E90_NAME                      "ROOT_E90"
#define MCAP_MP_INIT_DEFAULT_CODE       	{0xfaea0000, 0x00000000}
                                                /* CLI JUMP, JUMP_address end */
#define MCAP_MP_INIT_JUMP_ADDR_OFF              2
#define MCAP_MP_INIT_AREA_BMEM_ADDR             0x1fff0
/************************************************************************/
#define MCAP_BMEM_REG_SET_OFFSET                0x40000 /* óÍÅÝÅÎÉÅ ÏÓÎÏ×ÎÏÊ ÐÁÍÑÔÉ */
#define MCAP_BMEM_REG_SET_LEN                   0x20000 /* òÁÚÍÅÒ ÏÂÌÁÓÔÉ ïð */
/************************************************************************/
#define MCAP_DRV_CMN_AREA_BMEM_ADDR     	0x01990 /* îÁÞÁÌØÎÙÊ ÁÄÒÅÓ ÕÐÒÁ×ÌÑÀÝÅÊ */
                                                        /* ÉÎÆÏÒÍÁÃÉÑ ÂÕÆÅÒÏ× ÄÁÎÎÙÈ */
#define MCAP_AREA_BMEM_ADDR                     0xD0    /* îÁÞÁÌØÎÙÊ ÁÄÒÅÓ ÉÎÆÏÒÍÁÃÉÉ */
                                                        /* âïúõ */
#endif /* MCAP_OLD_VERSION */


/* 
   óÐÉÓÏË ËÏÄÏ× ÏÛÉÂÏË, ×ÙÒÁÂÁÔÙ×ÁÅÍÙÈ ÄÒÁÊ×ÅÒÏÍ (× ÏÓÎÏ×ÎÏÍ ÅÇÏ íð-ÞÁÓÔØÀ)
   ÐÒÉ ÉÓÐÏÌÎÅÎÉÉ ËÏÍÁÎÄ ÒÅÁÌÉÚÏ×ÁÎÎÙÈ ÞÅÒÅÚ ioctl()
   (×ÏÚÍÏÖÎÙÅ ÚÎÁÞÅÎÉÑ ÐÏÌÑ drv_error_code × ÉÓÐÏÌØÚÕÅÍÙÈ ÄÌÑ ÜÔÉÈ ËÏÍÁÎÄ
   ÓÔÒÕËÔÕÒÁÈ)
*/

#define FINISH_TRANS       1 /* úÁ×ÅÒÛÅÎÁ ×ÙÄÁÞÁ ÄÁÎÎÙÈ × ËÁÎÁÌ */
#define START_RECIV        2 /* îÁÞÁÔ ÐÒÉÅÍ ÐÁËÅÔÁ ÄÁÎÎÙÈ ÉÚ ËÁÎÁÌÁ */
#define FINISH_RECIV       3 /* úÁ×ÅÒÛÅÎ ÐÒÉÅÍ ÐÁËÅÔÁ ÄÁÎÎÙÈ ÉÚ ËÁÎÁÌÁ */

#define	BCW_HARDWARE_MCAP_ERROR		12

/* ëÏÄÙ ÏÛÉÂÏË */
#define NO_ERROR                 0 /* ëÁÎÁÌ ÉÎÉÃÉÁÌÉÚÉÒÏ×ÁÎ (ÏÔËÒÙÔ) */
#define ERROR_INIT_BUFFERS       0x01 /* ÷ÅÒÓÉÑ ÁÄÁÐÔÅÒÁ ÎÅ ÓÏÏÔ×ÅÔÓÔ×ÕÅÔ */
									  /* ×ÅÒÓÉÉ ÄÒÁÊ×ÅÒÁ íð */
#define ERROR_PLACE_MODE_MNIR    0x02 /* îÅ ÐÒÏÉÚ×ÅÄÅÎÏ ÏÔËÌÀÞÅÎÉÅ ËÁÎÁÌÏ× */
									  /* ÏÔ ÌÉÎÉÊ Ó×ÑÚÉ */
#define ERROR_PLACE_MODE_BATTLE  0x04 /* îÅ ÐÒÏÉÚ×ÅÄÅÎÏ ÐÏÄËÌÀÞÅÎÉÅ ËÁÎÁÌÁÍ */
									  /* Ë ÌÉÎÉÑÍ Ó×ÑÚÉ */
#define ERROR_HALT_CHANNEL       0x08 /* ëÁÎÁÌ ÎÅ ÉÎÉÃÉÁÌÉÚÉÒÏ×ÁÎ (ÚÁËÒÙÔ) */
#define FAILURE_TRANS         	 0x10 /* óÂÏÊ × ËÁÎÁÌÅ ÐÒÉ ÐÅÒÅÄÁÞÅ ÄÁÎÎÙÈ */
#define FAILURE_RECIV		 0x20 /* óÂÏÊ × ËÁÎÁÌÅ ÐÒÉ ÐÒÉÅÍÅ ÄÁÎÎÙÈ */

 /* úÎÁÞÅÎÉÑ ÂÉÔÏ× ÒÅÇÉÓÔÒÁ ÓÏÓÔÏÑÎÉÑ ÐÅÒÅÄÁÔÞÉËÁ */
#define bit_trns		0xFC /* ËÏÄ ×ÙÄÅÌÅÎÉÑ ÂÉÔÏ× ÐÒÉ ÐÅÒÅÄÁÞÅ ÄÁÎÎÙÈ */
#define err_trns		0x0C /* ËÏÄ ÏÛÉÂÏË ÐÒÉ ÐÅÒÅÄÁÞÅ ÄÁÎÎÙÈ */

#define sig_warning		0x80 /* ÐÒÉÎÑÔ ÓÉÇÎÁÌ ðòåäõðòåöäåîéå */
#define trans_word		0x40 /* ÐÅÒÅÄÁÎÏ ÏÞÅÒÅÄÎÏÅ ÓÌÏ×Ï × ËÁÎÁÌ */
#define user_data_tr		0x20 /* × ËÁÎÁÌ ÐÅÒÅÄÁÅÔÓÑ ÉÎÆ-ÃÉÑ ÐÏÌØÚÏ×ÁÔÅÌÑ */
#define end_array_tr		0x10 /* × ËÁÎÁÌ ÐÅÒÅÄÁÎ ÐÏÓÌÅÄÎÉÊ ÂÁÊÔ */
				     /* ÍÁÓÓÉ×Á ÐÏÌØÚÏ×ÁÔÅÌÑ */
#define sign_pbozy_tr		0x08 /* ÐÒÉÎÑÔ ÓÉÇÎÁÌ ABOZU */
#define false			0x04 /* ÐÏÓÌÅ ÐÅÒÅÄÁÞÉ ÓÌÏ×Á ÎÅÔ ÓÉÇÎÁÌÁ ÷åòîï */
#ifdef MCAP_OLD_VERSION
#define not_trans_2		0x02 /* ÒÅÚÅÒ× */
#else
#define congestion_prd  	0x02 /* ÐÅÒÅÇÒÕÚËÁ ÓÉÓÔÅÍÙ ÏÂÍÅÎÁ ÄÁÎÎÙÍÉ */
#endif /* MCAP_OLD_VERSION */
#define not_trans_1	    	0x01 /* ÒÅÚÅÒ× */

/* úÎÁÞÅÎÉÑ ÂÉÔÏ× ÒÅÇÉÓÔÒÁ ÓÏÓÔÏÑÎÉÑ ÐÒÉÅÍÎÉËÁ */
#define bit_rcv			0xFE /* ËÏÄ ×ÙÄÅÌÅÎÉÑ ÂÉÔÏ× ÐÒÉ ÐÒÉÅÍÅ ÄÁÎÎÙÈ */
#define err_rcv			0x6A /* ËÏÄ ÏÛÉÂÏË ÐÒÉ ÐÅÒÅÄÁÞÅ ÄÁÎÎÙÈ */

#define end_array_rv		0x80 /* ÐÒÉÎÑÔ ÐÏÓÌÅÄÎÉÊ ÂÁÊÔ × ÂÕÆÅÒ */
							 /* ÄÁÎÎÙÈ ÐÒÉÅÍÎÉËÁ */
#define sign_pbozy_rv		0x40 /* ÐÒÉÎÑÔ ÓÉÇÎÁÌ ABOZU */
#define busy_buf		0x20 /* ÚÁÎÑÔ ÂÕÆÅÒ ÄÌÑ ÓÌÅÄÕÀÝÅÇÏ ÓÌÏ×Á */
#define reaciv_word		0x10 /* ÐÒÉÎÑÔÏ ÓÌÏ×Ï ÄÁÎÎÙÈ */
#define no_tfr			0x08 /* ÏÔÓÕÔÓÔ×ÕÅÔ ÔÁËÔÏ×ÁÑ ÞÁÓÔÏÔÁ ÐÒÉÅÍÎÉËÁ */
#define user_data_rv		0x04 /* ÉÚ ËÁÎÁÌÁ ÐÒÉÎÑÔÁ ÉÎÆ-ÃÉÑ ÐÏÌØÚÏ×ÁÔÅÌÑ */
#define mod_err			0x02 /* ÐÒÉÎÑÔÏ ÓÌÏ×Ï ÄÁÎÎÙÈ Ó ÏÛÉÂËÏÊ ÐÏ mod3 */
#define not_reciv		0x01 /* ÒÅÚÅÒ× */

#define	MCAP_SUBDEV_BUF_NUM		4 	/* ËÏÌ-×Ï ÂÕÆÅÒÏ× ÏÂÍÅÎÁ (ËÁÎÁÌÏ×) */
#define	PACKET_FREQUENCY		0x0200	/* ôþ÷ ðÄ ×ÙÄÁÅÔÓÑ × áðä ÐÁÞËÁÍÉ */
#define	CONSTANT_FREQUENCY		0 	/* ôþ÷ ðÄ ×ÙÄÁÅÔÓÑ × áðä ÎÅÐÒÅÒÙ×ÎÏ */
#define	MCAP_HUNGUP_TIMER_INTERVAL	360000
									/* ÚÎÁÞÅÎÉÅ ÔÁÊÍÅÒÁ ÄÌÑ */
									/* ÏÖÉÄÁÎÉÑ ÚÁ×ÅÒÛÅÎÉÑ ÏÂÍÅÎÏ× */
									/* (× ÍÉËÒÏÓÅËÕÎÄÁÈ) */

#define	MCAP_NOT_CHANGE		 0	/* ÎÅ ÉÚÍÅÎÑÔØ */
#define	MCAP_IO_WRITE		 0x01	/* ÚÁÐÉÓØ (ÐÅÒÅÄÁÞÁ × ËÁÎÁÌ) */
#define	MCAP_IO_READ		 0x02	/* ÞÔÅÎÉÅ (ÐÒÉÅÍ ÉÚ ËÁÎÁÌÁ) */
#define	MCAP_WRITE_READ		 0x03	/* ÚÁÐÉÓØ/ÞÔÅÎÉÅ */

#define	GISTOGR			   8   	/* òÁÚÍÅÒ ÍÁÓÓÉ×Á ÒÁÓÐÒÅÄÅÌÅÎÉÑ ×ÒÅÍÅÎÉ */
/* òÅÖÉÍÙ ÆÕÎËÃÉÏÎÉÒÏ×ÁÎÑ íó19 */
#define	MODE_COUPLED		   1   	/* òÁÂÏÔÁ ÏÓÎÏ×ÎÏÇÏ ÷ë Ó áðä Ó */
				       	/* ÒÁÚÒÅÛÅÎÉÅÍ ÒÅÚÅÒ×ÎÏÍ ÷ë ÐÒÉÅÍ */
				       	/* ÉÎÆÏÒÍÁÃÉÉ */
#define	MODE_COUPLED_COMPUTERS 0x0400 

#define	MODE_STANDBY		   2	/* òÁÂÏÔÁ ÒÅÚÅÒ×ÎÏÇÏ ÷ë Ó áðä ÔÏÌØËÏ */
					/* ÐÏ ÐÒÉÅÍÕ ÉÎÆÏÒÍÁÃÉÉ */
#define	MODE_STANDBY_COMPUTER  0x0100

#define	MODE_SINGLE		   3	/* òÁÂÏÔÁ ÏÓÎÏ×ÎÏÇÏ ÷ë Ó áðä Ó */
					/* Ó ÚÁÐÒÅÔÏÍ ÐÏÄËÌÀÞÅÎÉÑ ÒÅÚÅÒ×ÎÏÇÏ ÷ë */
#define	MODE_SINGLE_COMPUTER   0x0600

#define	AFM   			   4	/* îÁÌÁÄËÁ ÍÏÄÕÌÑ, ËÏÍÐ-ÎÁÑ ÎÁÌÁÄËÁ ÷ë */
#define	MODE_AFM	       0x0700 
#define	MODE_US19   		   5	/* òÁÂÏÔÁ ÷ë ÐÏ ÐÒÏ×ÅÒËÅ õó19 */
#define	AUDIT_TE	   	   6 	/* ðÒÏ×ÅÒËÁ ÍÏÄÕÌÑ ÐÏ ôõ */

#define	NOT_RESTORE   		   0	/* ÎÅ ×ÏÓÓÔÁÎÁ×ÌÉ×ÁÔØ õóë, óëâ É áó0 */
#define	RESTORE  	           1	/* ×ÏÓÓÔÁÎÏ×ÉÔØ õóë, óëâ É áó0 ËÁÎÁÌÁ */

#define	NOT_EXCHANGE_DATAS	   0	/* ÎÅ ×ÙÄÁ×ÁÔØ × ËÁÎÁÌ ÓÌÏ×Á ÐÁËÅÔÁ */
					/* ÄÁÎÎÙÈ ÐÏÓÌÅ ÆÉËÓÁÃÉÉ ÏÛÉÂËÉ ÏÂÍÅÎÁ */
#define	EXCHANGE_DATAS         	   1	/* ×ÙÄÁ×ÁÔØ × ËÁÎÁÌ ÓÌÏ×Á ÐÁËÅÔÁ ÄÁÎÎÙÈ */
					/* ÐÏÓÌÅ ÆÉËÓÁÃÉÉ ÏÛÉÂËÉ ÏÂÍÅÎÁ */
#define	NOT_PRODUCE_INTR	   0	/* ÄÒÁÊ×ÅÒ íð ÎÅ ÄÏÌÖÅÎ ×ÙÄÁ×ÁÔØ */
					/* ÐÒÅÒÙ×ÁÎÉÑ */
#define	PRODUCE_INTR           	   1	/* ÄÒÁÊ×ÅÒ ÄÏÌÖÅÎ íð ×ÙÄÁ×ÁÔØ ÐÒÅÒÙ×ÁÎÉÑ */

#define	MCAP_DMA_BURST_SIZE	   8*4	/* ÒÁÚÍÅÒ (× ÂÁÊÔÁÈ) ÂÌÏËÁ ÏÂÍÅÎÁ */

#ifdef MCAP_OLD_VERSION
#define	MCAP_MAX_NUM_WORD	0xFFF
#define	MCAP_MAX_WORD_DATA_BUF_TRANS	256 	/* ÍÁËÉÍÁÌØÎÁÑ ÄÌÉÎÁ × ÓÌÏ×ÁÈ ÐÅÒÅÄÁÀÝÅÇÏ ÂÕÆÅÒÁ ÏÂÍÅÎÁ ËÁÎÁÌÁ */
#define	MCAP_MAX_WORD_DATA_BUF_RECIV	3568 	/* ÍÁËÉÍÁÌØÎÁÑ ÄÌÉÎÁ × ÓÌÏ×ÁÈ ÐÒÉÅÍÎÏÇÏ ÂÕÆÅÒÁ ÏÂÍÅÎÁ */
#else
#define MCAP_MAX_WORD_DATA_BUF_TRANS    4088    /* ÍÁËÉÍÁÌØÎÁÑ ÄÌÉÎÁ × ÓÌÏ×ÁÈ ÐÅÒÅÄÁÀÝÅÇÏ ÂÕÆÅÒÁ ÏÂÍÅÎÁ ËÁÎÁÌÁ */
#define MCAP_MAX_WORD_DATA_BUF_RECIV    4095    /* ÍÁËÉÍÁÌØÎÁÑ ÄÌÉÎÁ × ÓÌÏ×ÁÈ ÐÒÉÅÍÎÏÇÏ ÂÕÆÅÒÁ ÏÂÍÅÎÁ */
#endif /* MCAP_OLD_VERSION */
#define	MCAP_MAX_DATA_BUF_SIZE		MCAP_MAX_WORD_DATA_BUF_RECIV*4	/* ÍÁËÉÍÁÌØÎÁÑ ÄÌÉÎÁ × ÂÁÊÔÁÈ */
									/* ÐÒÉÅÍÎÏÇÏ ÂÕÆÅÒÁ ÏÂÍÅÎÁ ËÁÎÁÌÁ */
/* ëÏÄÙ ÓÏÏÂÝÅÎÉÊ ÏÂ ÏÛÉÂËÁÈ É ÓÂÏÑÈ */
/* ÷ÅÒÓÉÑ ÄÒÁÊ×ÅÒÁ ÷ë ÎÅ ÓÏÏÔ×ÅÔÓÔ×ÕÅÔ ×ÅÒÓÉÉ ÍÏÄÕÌÑ */
#define ERRORVERDRVVK		164


/*	íÁËÒÏÓ ÄÌÑ ÐÒÅÏÂÒÁÚÏ×ÁÎÉÑ ÒÁÚÍÅÒÁ ÍÁÓÓÉ×Á ÉÚ ÜÌÅÍÅÎÔÏ× ÎÅËÏÔÏÒÏÊ ÓÔÒÕËÔÕÒÙ
	Ó ÕÞÅÔÏÍ ËÒÁÔÎÏÓÔÉ ÂÌÏËÕ ÏÂÍÅÎÁ × ÒÅÖÉÍÅ DMA.
	nelem - ÉÓÈÏÄÎÏÅ ÞÉÓÌÏ ÜÌÅÍÅÎÔÏ× × ÍÁÓÓÉ×Å
	elsize - ÒÁÚÍÅÒ ÜÌÅÍÅÎÔÁ × ÂÁÊÔÁÈ
	off - ÓÍÅÝÅÎÉÅ ÍÁÓÓÉ×Á ÏÔÎÏÓÉÔÅÌØÎÏ ÎÁÞÁÌÁ ÏÂÌÁÓÔÉ × ËÏÔÏÒÏÊ ÏÎ ÎÁÈÏÄÉÔÓÑ
	bsize - ÒÁÚÍÅÒ ÂÌÏËÁ ÏÂÍÅÎÁ × ÒÅÖÉÍÅ DMA
	ÒÅÚÕÌØÔÁÔ - ÓËÏÒÒÅËÔÉÒÏ×ÁÎÎÏÅ ÞÉÓÌÏ ÜÌÅÍÅÎÔÏ× × ÍÁÓÓÉ×Å ÄÌÑ ÏÂÅÓÐÅÞÅÎÉÑ
	ËÒÁÔÎÏÓÔÉ MCAP_DMA_BURST_SIZE */

#define	TU_MCAP_DMA_BURST_SIZE_ALIGN(nelem, elsize, off, bsize) ((((((nelem) * \
		(elsize) + (off) + ((bsize)-1)) / (bsize)) * (bsize)) \
		- (off)) / (elsize))

 /*  ðÒÉ ÏÂÍÅÎÁÈ Ó ËÁÎÁÌÁÍÉ MCAP ÉÓÐÏÌØÚÕÅÔÓÑ ËÁÒÔÁ ÏÂÍÅÎÏ×. üÌÅÍÅÎÔÁÍÉ ËÁÒÔÙ
 	Ñ×ÌÑÀÔÓÑ ÐÏÄÕÓÔÒÏÊÓÔ×Á. ëÁÖÄÙÊ ÉÚ ÎÉÈ ÉÍÅÅÔ ÓÏÂÓÔ×ÅÎÎÙÊ ÎÏÍÅÒ É ÔÉÐ - 
 	ÐÒÉÅÍÎÉË ÉÌÉ ÐÅÒÅÄÁÔÞÉË. ðÏÄÕÓÔÒÏÊÓÔ×Ï Ó ÏÄÎÉÍ ÎÏÍÅÒÏÍ ÍÏÖÅÔ ÂÙÔØ É 
 	ÐÒÉÅÍÎÉËÏÍ, É ÐÅÒÅÄÁÔÞÉËÏÍ, × ÌÀÂÏÍ ÓÌÕÞÁÅ ÏÎÉ ÒÁÓÓÍÁÔÒÉ×ÁÀÔÓÑ ËÁË
	ÎÅÚÁ×ÉÓÉÍÙÅ É ËÁÖÄÙÊ Ñ×ÌÑÅÔÓÑ ÐÏÌÎÏÃÅÎÎÙÍ ÜÌÅÍÅÎÔÏÍ ËÁÒÔÙ ÏÂÍÅÎÏ×.
	äÌÑ ÏÒÇÁÎÉÚÁÃÉÉ ÏÂÍÅÎÏ× Ó ÌÀÂÙÍ ÐÏÄÕÓÔÒÏÊÓÔ×ÏÍ ÉÓÐÏÌØÚÕÀÔÓÑ ÂÕÆÅÒÁ, ÉÍÅÀÝÉÅ
	ÓÌÅÄÕÀÝÕÀ ÓÔÒÕËÔÕÒÕ:
						  ____________________________________
	 mcap_iosubd_desc -> |     ÚÁÇÏÌÏ×ÏË ÂÕÆÅÒÁ ÏÂÍÅÎÁ      |
						 |      (ÓÏÄÅÒÖÉÔ ÄÅÓËÒÉÐÔÏÒ        |
						 |       ÒÅÚÕÌØÔÁÔÏ× ÏÂÍÅÎÁ)        |
						 |----------------------------------|
	 mcap_data_buf    -> |      ÂÕÆÅÒ, ÐÒÉÎÉÍÁÅÍÙÈ ÉÚ       |
						 |     ËÁÎÁÌÁ ÉÌÉ ÐÅÒÅÄÁ×ÁÅÍÙÈ      |
						 |        × ËÁÎÁÌ ÄÁÎÎÙÈ            |
						 |__________________________________|

	   úÁÇÏÌÏ×ÏË ÂÕÆÅÒÁ ÏÂÍÅÎÁ Ñ×ÌÑÅÔÓÑ ÏÂÑÚÁÔÅÌØÎÙÍ ÜÌÅÍÅÎÔÏÍ ËÁÖÄÏÇÏ ÂÕÆÅÒÁ,
	ÎÁÈÏÄÉÔÓÑ × ÅÇÏ ÎÁÞÁÌÅ, ÓÏÄÅÒÖÉÔ ÎÅËÏÔÏÒÕÀ ÉÎÆÏÒÍÁÃÉÀ ÏÂ ÏÂÍÅÎÅ É ÅÇÏ
	ÒÅÚÕÌØÔÁÔÁÈ É ÉÍÅÅÔ ÆÉËÓÉÒÏ×ÁÎÎÙÊ ÒÁÚÍÅÒ - 8 ÓÌÏ× (32 ÂÁÊÔÁ), ÞÔÏ ÓÏ×ÐÁÄÁÅÔ
	Ó ÒÁÚÍÅÒÏÍ ÂÌÏËÁ ÏÂÍÅÎÁ ÍÅÖÄÕ ËÁÎÁÌÏÍ É ÏÓÎÏ×ÎÏÊ ÐÁÍÑÔØÀ × ÒÅÖÉÍÅ
	ÎÅÐÏÓÒÅÄÓÔ×ÅÎÎÏÇÏ ÄÏÓÔÕÐÁ (DMA). îÅÐÏÓÒÅÄÓÔ×ÅÎÎÏ ÄÁÎÎÙÅ,
	ÐÒÉÎÑÔÙÅ ÉÚ ËÁÎÁÌÁ ÉÌÉ ÐÅÒÅÄÁ×ÁÅÍÙÅ × ËÁÎÁÌ, ÓÌÅÄÕÀÔ ×ÓÌÅÄ ÚÁ ÚÁÇÏÌÏ×ËÏÍ.
	îÁ ÏÓÎÏ×Å ÚÎÁÞÅÎÉÑ ÐÏÌÅÊ ÄÅÓËÒÉÐÔÏÒÁ ÒÅÚÕÌØÔÁÔÏ× ÍÏÖÎÏ ÓÌÅÄÉÔØ ÚÁ ÔÅÍ -
	ÚÁËÏÎÞÅÎ ÏÂÍÅÎ ÉÌÉ ÎÅÔ (ÓÍ. ÄÁÌÅÅ ÏÐÉÓÁÎÉÅ ÓÔÒÕËÔÕÒÙ ÚÁÇÏÌÏ×ËÁ).
	   ðÒÉÍÅÒ ÏÐÉÓÁÎÉÑ ÔÁËÏÇÏ ÒÏÄÁ ÓÔÒÕËÔÕÒÙ ÐÒÉ×ÅÄÅÎ ÎÉÖÅ É ÒÅËÏÍÅÎÄÕÅÔÓÑ ÄÌÑ
	ÉÓÐÏÌØÚÏ×ÁÎ × ÐÏÌØÚÏ×ÁÔÅÌØÓËÉÈ ÐÒÏÇÒÁÍÍÁÈ, ÐÏÓËÏÌØËÕ ÔÁËÁÑ ÖÅ ÓÔÒÕËÔÕÒÁ
	ÂÕÆÅÒÁ ÉÓÐÏÌØÚÕÅÔÓÑ × ÄÒÁÊ×ÅÒÅ É ÂÉÂÌÉÏÔÅÞÎÙÈ ÆÕÎËÃÉÑÈ 'open_mcap_drv.h'.
	   âÕÆÅÒÁ ÏÂÍÅÎÏ× ÄÌÑ ×ÓÅÈ ÐÏÄÕÓÔÒÏÊÓÔ×, ÓÏÓÔÁ×ÌÑÀÝÉÈ ËÁÒÔÕ ÏÂÍÅÎÏ× Ó
	ËÁÎÁÌÏÍ, × ÎÅÏÂÈÏÄÉÍÏÍ ËÏÌÉÞÅÓÔ×Å ÂÕÄÕÔ ×ÙÄÅÌÅÎÙ ÐÒÉ ÉÎÉÃÉÁÌÉÚÁÃÉÉ ÒÅÖÉÍÁ
	ÏÂÍÅÎÏ× (ÏÔËÒÙÔÉÉ ËÁÎÁÌÁ MCAP, ÓÍ ÄÁÌÅÅ). õËÁÚÁÔÅÌÉ
	ÎÁ ÓÏÚÄÁÎÎÙÅ ÂÕÆÅÒÁ ËÁÒÔÙ ×ÏÚ×ÒÁÝÁÀÔÓÑ ËÁË ÒÅÚÕÌØÔÁÔ ÉÎÉÃÉÁÌÉÚÁÃÉÉ,
	× ×ÉÄÅ ÍÁÓÓÉ×Á, × ËÏÔÏÒÏÍ ÉÎÄÅËÓ ÕËÁÚÁÔÅÌÑ Ñ×ÌÑÅÔÓÑ ÎÏÍÅÒÏÍ ÄÁÎÎÏÇÏ ÂÕÆÅÒÁ
	× ÏÂÝÅÍ ÐÕÌÅ ÂÕÆÅÒÏ×. âÕÆÅÒ ËÁÒÔÙ ÏÂÍÅÎÏ× ÐÒÅÄÓÔÁ×ÌÑÅÔ ÓÏÂÏÊ ÍÁÓÓÉ×
	ÂÕÆÅÒÏ×. äÌÑ ËÁÖÄÏÇÏ ÐÏÄÕÓÔÒÏÊÓÔ×Á ÏÔ×ÅÄÅÎÙ ÐÏ Ä×Á
	ÂÕÆÅÒÁ - ÄÌÑ ÐÒÉÅÍÎÉËÁ É ÄÌÑ ÐÅÒÅÄÁÔÞÉËÁ. ëÁÖÄÙÊ ÂÕÆÅÒ ÏÂÍÅÎÁ
	ÁÂÏÎÅÎÔÁ ÉÎÉÃÉÁÌÉÚÉÒÏ×ÁÎ ËÁË ËÏÎËÒÅÔÎÙÊ ÜËÚÅÍÐÌÑÒ ÓÔÒÕËÔÕÒÙ
	'mcap_iosubdbuf_t' ÏÐÉÓÁÎÎÏÊ ÄÁÌÅÅ, ÇÄÅ ÒÁÚÍÅÒ ÂÕÆÅÒÁ ÄÁÎÎÙÈ
	'MCAP_MAX_DATA_BUF_SIZE' ÐÏÌÁÇÁÀÔÓÑ ÒÁ×ÎÙÍÉ ÚÎÁÞÅÎÉÀ, ÐÏÄÁÎÎÏÍÕ ×
	ËÁÞÅÓÔ×Å ÐÁÒÁÍÅÔÒÁ ÉÎÉÃÉÁÌÉÚÁÃÉÉ ËÁÎÁÌÁ. ÷ ÚÁÇÏÌÏ×ËÅ ÂÕÆÅÒÁ ÕÓÔÁÎÏ×ÌÅÎÙ
	× ÓÏÏÔ×ÅÔÓÔ×ÕÀÝÉÅ ÚÎÁÞÅÎÉÑ ×ÓÅ ÆÉËÓÉÒÏ×ÁÎÎÙÅ ÐÏÌÑ É × ÎÁÞÁÌØÎÏÅ
	(ÎÕÌÅ×ÏÅ) ÓÏÓÔÏÑÎÉÅ - ÐÏÌÑ ÄÅÓËÒÉÐÔÏÒÁ ÒÅÚÕÌØÔÁÔÏ× ÏÂÍÅÎÁ (ÓÍ. ÄÁÌÅÅ
	ÏÐÉÓÁÎÉÅ ÚÁÇÏÌÏ×ËÁ, ÇÄÅ ÄÌÑ ËÁÖÄÏÇÏ ÐÏÌÑ ÕËÁÚÁÎÏ ËÁËÉÍ ÏÂÒÁÚÏÍ É ËÅÍ ÏÎÏ
	ÉÎÉÃÉÁÌÉÚÉÒÕÅÔÓÑ).
*/

/*  óÔÒÕËÔÕÒÁ ÄÅÓËÒÉÐÔÏÒÁ ÂÕÆÅÒÁ (ÚÁÇÏÌÏ×ÏË ÂÕÆÅÒÁ ÏÂÍÅÎÁ), ÉÚ
	ËÏÔÏÒÙÈ ÓÏÓÔÏÉÔ ËÁÒÔÁ ÏÂÍÅÎÏ× ÄÌÑ ËÁÎÁÌÁ MCAP ×ËÌÀÞÁÅÔ × ÓÅÂÑ ÏÐÉÓÁÎÉÅ
	ÚÁÑ×ËÉ ÎÁ ÏÂÍÅÎ, Á ÔÁËÖÅ ÐÏÌÑ ÒÅÚÕÌØÔÁÔÏ× ÅÇÏ ÉÓÐÏÌÎÅÎÉÑ, Ô.Å. 
	ÄÅÓËÒÉÐÔÏÒ ÒÅÚÕÌØÔÁÔÏ× ÏÂÍÅÎÁ.
		úÁÇÏÌÏ×ÏË ÂÕÆÅÒÁ ÏÂÍÅÎÁ Ñ×ÌÑÅÔÓÑ ÏÂÑÚÁÔÅÌØÎÙÍ ÜÌÅÍÅÎÔÏÍ ËÁÖÄÏÇÏ ÂÕÆÅÒÁ
	ÁÂÏÎÅÎÔÁ, ÎÁÈÏÄÉÔÓÑ × ÅÇÏ ÎÁÞÁÌÅ É ÄÏÌÖÅÎ ÉÍÅÔØ ÆÉËÓÉÒÏ×ÁÎÎÙÊ
	ÒÁÚÍÅÒ - 8 ÓÌÏ× (32 ÂÁÊÔÁ), ÞÔÏ ÓÏ×ÐÁÄÁÅÔ Ó ÒÁÚÍÅÒÏÍ ÂÌÏËÁ ÏÂÍÅÎÁ ÍÅÖÄÕ
	ËÁÎÁÌÏÍ É ÏÓÎÏ×ÎÏÊ ÐÁÍÑÔØÀ × ÒÅÖÉÍÅ ÎÅÐÏÓÒÅÄÓÔ×ÅÎÎÏÇÏ ÄÏÓÔÕÐÁ (DMA).
	÷ Ó×ÑÚÉ Ó ÜÔÉÍ ÓÔÒÕËÔÕÒÁ ÄÏÐÏÌÎÑÅÔÓÑ ÄÏ 8 ÓÌÏ× ÎÅÉÓÐÏÌØÚÕÅÍÙÍÉ ÐÏÌÑÍÉ.
	îÅÐÏÓÒÅÄÓÔ×ÅÎÎÏ ÄÁÎÎÙÅ ÐÒÉÎÑÔÙÅ ÉÚ ËÁÎÁÌÁ ÉÌÉ ÐÅÒÅÄÁ×ÁÅÍÙÅ × ËÁÎÁÌ ÓÌÅÄÕÀÔ
	×ÓÌÅÄ ÚÁ ÚÁÇÏÌÏ×ËÏÍ.
		îÁ ÏÓÎÏ×Å ÚÎÁÞÅÎÉÑ ÐÏÌÅÊ ÄÅÓËÒÉÐÔÏÒÁ ÒÅÚÕÌØÔÁÔÏ× ÍÏÖÎÏ ÓÌÅÄÉÔØ ÚÁ ÔÅÍ
	ÚÁËÏÎÞÅÎ ÏÂÍÅÎ ÉÌÉ ÎÅÔ.
	ðÏÓÌÅ ÚÁ×ÅÒÛÅÎÉÑ ÏÞÅÒÅÄÎÏÇÏ ÏÂÍÅÎÁ Ó ÄÁÎÎÙÍ ÂÕÆÅÒÏÍ ÄÏÌÖÅÎ ÏÂÎÕÌÑÔØÓÑ
	ÐÒÉÚÎÁË ÚÁ×ÅÒÛÅÎÉÑ ÏÂÍÅÎÁ Ó ÂÕÆÅÒÏÍ.
	üÔÏ ÐÏÚ×ÏÌÑÅÔ ÐÏÌØÚÏ×ÁÔÅÌØÓËÏÊ ÆÕÎËÃÉÉ ÓÌÅÄÉÔØ ÚÁ ËÏÎÃÏÍ
	ÏÂÍÅÎÁ Ó ÄÁÎÎÙÍ ÂÕÆÅÒÏÍ ÎÁ ÏÓÎÏ×Å ÚÎÁÞÅÎÉÑ ÐÒÉÚÎÁËÁ:
	transfer_completed
	åÓÌÉ ÐÒÉÚÎÁË - ÎÅÎÕÌÅ×ÏÅ, ÔÏ ÏÂÍÅÎ ÚÁ×ÅÒÛÅÎ.
*/
#ifdef MY_DRIVER_BIG_ENDIAN
typedef union reg_general_mcap
{
	u_int          rdwr_reg_general;
	struct
		{
			u_int  bit3129 : 3; /* ÎÅ ÉÓÐÏÌØÚÕÅÔÓÑ */
			u_int  bit2824 : 5; /* òîëûû - RNKSH */
			u_int  bit2321 : 3; /* ÎÅ ÉÓÐÏÌØÚÕÅÔÓÑ */
			u_int  bit2016 : 5; /* òïû - ROSH */
			u_int  bit1507 : 9; /* ÎÅ ÉÓÐÏÌØÚÕÅÔÓÑ */
			u_int  bit06   : 1; /* ôðþóû - TPCHSH */
			u_int  bit05   : 1; /* ôóú - TSZ */
			u_int  bit04   : 1; /* ôðû - TPSH */
			u_int  bit03   : 1; /* ôâì - TBL */
			u_int  bit02   : 1; /* ôóí - TSM */
			u_int  bit0100 : 2; /* ÎÅ ÉÓÐÏÌØÚÕÅÔÓÑ */
		} as_bits_0;
} reg_general_mcap_t;
#else
typedef union reg_general_mcap
{
	u_int          rdwr_reg_general;
	struct
		{
			u_int  bit0100 : 2; /* ÎÅ ÉÓÐÏÌØÚÕÅÔÓÑ */
			u_int  bit02   : 1; /* ôóí - TSM */
			u_int  bit03   : 1; /* ôâì - TBL */
			u_int  bit04   : 1; /* ôðû - TPSH */
			u_int  bit05   : 1; /* ôóú - TSZ */
			u_int  bit06   : 1; /* ôðþóû - TPCHSH */
			u_int  bit1507 : 9; /* ÎÅ ÉÓÐÏÌØÚÕÅÔÓÑ */
			u_int  bit2016 : 5; /* òïû - ROSH */
			u_int  bit2321 : 3; /* ÎÅ ÉÓÐÏÌØÚÕÅÔÓÑ */
			u_int  bit2824 : 5; /* òîëûû - RNKSH */
			u_int  bit3129 : 3; /* ÎÅ ÉÓÐÏÌØÚÕÅÔÓÑ */
		} as_bits_0;
} reg_general_mcap_t;
#endif /* MY_DRIVER_BIG_ENDIAN */

#define	reg_RNKSH		as_bits_0.bit2824 	/* òîëûû - RNKSH */
#define	reg_ROSH		as_bits_0.bit2016 	/* òïû - ROSH */
#define	trg_TPCHSH		as_bits_0.bit06 	/* ôðþóû - TPCHSH */
#define	trg_TSZ			as_bits_0.bit05 	/* ôóú - TSZ */
#define	trg_TPSH		as_bits_0.bit04 	/* ôðû - TPSH */
#define	trg_TBL			as_bits_0.bit03 	/* ôâì - TBL */
#define	trg_TSM			as_bits_0.bit02 	/* ôóí - TSM */

#define MCAP_TSM       	0x08 /* ôÒÉÇÇÅÒ ÓÂÒÏÓÁ ÍÏÄÕÌÑ */
#define MCAP_TBL       	0x0C /* ôÒÉÇÇÅÒ ÂÌÏËÉÒÏ×ËÉ ÏÛÉÂÏË */
#define MCAP_TPSH      	0x10 /* ôÒÉÇÇÅÒ ÐÒÅÒÙ×ÁÎÉÑ ÐÒÏÇÒÁÍÍÙ ÐÏÌØÚÏ×ÁÔÅÌÑ */
#define MCAP_TSZ      	0x14 /* ôÒÉÇÇÅÒ ÓÂÒÏÓÁ ÚÎÁÞÉÍÏÓÔÉ ÒÅÇÉÓÔÒÁ õóë */
#define MCAP_TPCHSH     0x18 /* ôÒÉÇÇÅÒ ÐÒÉÚÎÁËÁ ÞÅÔÎÏÓÔÉ ÓÉÓÔÅÍÎÏÊ ÛÉÎÙ */
#define MCAP_RERR     	0x1C /* ROSH É òÅÇÉÓÔÒ ÎÏÍÅÒÁ ËÁÎÁÌÁ × ÛÉÎÅ */
#define MCAP_TZM       	0x20 /* ôÒÉÇÇÅÒ ÏÂÎÕÌÅÎÉÑ ÍÏÄÕÌÑ */

/* þÔÅÎÉÑ/ÚÁÐÉÓØ ÓÏÄÅÒÖÉÍÏÇÏ ÒÅÇÉÓÔÒÁ MCAP */
typedef struct arg_reg {
	int		reg_addr;	/* ÁÄÒÅÓ ÒÅÇÉÓÔÒÁ */
	u_short		reg_value;	/* ×ÏÚ×ÒÁÝÁÅÍÁÑ/ÐÅÒÅÄÁ×ÁÅÍÁÑ ×ÅÌÉÞÉÎÁ */
} mcap_arg_reg_t;

typedef struct mcap_iosubd_desc {
	u_short	transfer_completed;     /* ÐÒÉÚÎÁË ×ÙÐÏÌÎÅÎÉÑ ÏÂÍÅÎ */
	u_short	channel_check_word;	/* ÓÏÓÔÏÑÎÉÅ ËÁÎÁÌÁ (ÏÛÉÂËÉ ÏÂÍÅÎÁ) */
	u_short	data_size_exchange; 	/* ËÏÌÉÞÅÓÔ×Ï ÓÌÏ× ÏÂÍÅÎÁ */
	u_short	first_error; 	 	/* ÎÏÍÅÒ ÓÌÏ×Á, ÐÒÉ ×ÙÄÁÞÅ (ÐÒÉÅÍÅ) */
					/* ËÏÔÏÒÏÇÏ ÚÁÆÉËÓÉÒÏ×ÁÎÁ ÐÅÒ×ÁÑ ÏÛÉÂËÁ */
					/* ÏÂÍÅÎÁ */
	u_short	num_error; 	 	/* ËÏÌÉÞÅÓÔ×Ï ÏÛÉÂÏË ÏÂÍÅÎÁ */
	u_short exchange_error_code;    /* ËÏÄ ÏÛÉÂËÉ, ÏÂÎÁÒÕÖÅÎÎÏÊ ÄÒÁÊ×ÅÒÏÍ Mð */
	u_short signal_adapter;         /* ËÏÌ-×Ï ÓÉÇÎÁÌÏ× úïë (éîé) */
	u_short	cur_ease_code;          /* ÔÅËÕÝÅÅ ËÏÌ-×Ï ÐÒÉÎÑÔÙÈ ëð */
	short	buf_num;        	/* ÎÏÍÅÒ ÂÕÆÅÒÁ ÏÂÍÅÎÁ */
	u_short io_flags; 		/* ËÏÄ ÏÐÅÒÁÃÉÉ ÏÂÍÅÎÁ */
	u_short	data_size; 		/* ÍÁËÓÉÍÁÌØÎÁÑ ÄÌÉÎÁ × ÂÁÊÔÁÈ ÍÁÓÓÉ×Á ÏÂÍÅÎÁ */
	short	unused2; 		/* ÎÅ ÉÓÐÏÌØÚÕÅÔÓÑ */
	int	unused_word6; 		/* ÎÅ ÉÓÐÏÌØÚÕÅÔÓÑ */
	int	unused_word7; 		/* ÎÅ ÉÓÐÏÌØÚÕÅÔÓÑ */
} mcap_iosubd_desc_t;

/*	óÔÒÕËÔÕÒÁ ÂÕÆÅÒÁ ÄÁÎÎÙÈ ÄÌÑ ÏÂÍÅÎÁ.
	âÕÆÅÒ ÄÁÎÎÙÈ ÐÒÅÄÓÔÁ×ÌÑÅÔ ÓÏÂÏÊ ÏÂÙÞÎÙÊ ÍÁÓÓÉ× É × ÄÁÎÎÏÊ ÓÔÒÕËÔÕÒÅ ÏÎ
	ÐÒÅÄÓÔÁ×ÌÅÎ × ×ÉÄÅ ÏÂßÅÄÉÎÅÎÉÑ ÍÁÓÓÉ×Ï× ÒÁÚÎÙÈ ÆÏÒÍÁÔÏ×.
	òÁÚÍÅÒ ÂÕÆÅÒÁ ÐÒÉ ÎÅÏÂÈÏÄÉÍÏÓÔÉ ËÏÒÒÅËÔÉÒÕÅÔÓÑ ÄÌÑ ÏÂÅÓÐÅÞÅÎÉÑ
	ËÒÁÔÎÏÓÔÉ ÂÌÏËÕ ÏÂÍÅÎÁ
*/
typedef union mcap_data_buf_ {
	u_short
								/* ÍÁÓÓÉ× ÓÌÏ× ÄÁÎÎÙÈ ÏÂÍÅÎÁ */
								/* ÓÌÏ×Ï × ÓÍÙÓÌÅ ËÁÎÁÌÁ MCAP */
								/* (2 ÂÁÊÔÁ, 16 ÂÉÔ) */
		words[TU_MCAP_DMA_BURST_SIZE_ALIGN(MCAP_MAX_DATA_BUF_SIZE /
				sizeof(u_short),
			sizeof(u_short), 0, MCAP_DMA_BURST_SIZE)];
	u_int
								/* ÍÁÓÓÉ× ÓÌÏ× ÏÓÎÏ×ÎÏÊ */
								/* ÐÁÍÑÔÉ (4 ÂÁÊÔÁ 32 ÂÉÔ) */
		longs[TU_MCAP_DMA_BURST_SIZE_ALIGN(MCAP_MAX_DATA_BUF_SIZE /
			sizeof(u_int), sizeof(u_int), 0,
			MCAP_DMA_BURST_SIZE)];
	u_char
								/* ÍÁÓÓÉ× ÂÁÊÔÏ× */
		bytes[TU_MCAP_DMA_BURST_SIZE_ALIGN(MCAP_MAX_DATA_BUF_SIZE,
			sizeof(u_char), 0, MCAP_DMA_BURST_SIZE)]; 
} mcap_data_buf_t;

/*  ïÐÉÓÁÎÉÑ ÓÔÒÕËÔÕÒÙ ÂÕÆÅÒÁ ÏÂÍÅÎÁ.
	äÒÁÊ×ÅÒ ÓÏÚÄÁÅÔ É ÉÎÉÃÉÁÌÉÚÉÒÕÅÔ ÏÂÝÉÅ
	ÂÕÆÅÒÁ ËÁË ËÏÎËÒÅÔÎÙÅ ÜËÚÅÍÐÌÑÒÙ ÉÍÅÎÎÏ ÄÁÎÎÏÊ ÓÔÒÕËÔÕÒÙ. ðÒÉ ÜÔÏÍ 
	× ÚÁÇÏÌÏ×ËÅ ÂÕÆÅÒÁ 	ÉÎÉÃÉÁÌÉÚÉÒÏ×ÁÎÙ ×ÓÅ ÐÏÌÑ Ó ÆÉËÓÉÒÏ×ÁÎÎÙÍÉ É 
	ÐÏÓÔÏÑÎÎÙÍÉ ÚÎÁÞÅÎÉÑÍÉ. ÷ ÎÕÌÅ×ÏÅ ÓÏÓÔÏÑÎÉÅ ÕÓÔÁÎÏ×ÌÅÎÙ ÒÅÚÕÌØÔÁÔÏ× ÏÂÍÅÎÁ.
*/
typedef struct mcap_iosubdbuf {
	mcap_iosubd_desc_t
			buf_desc;  /* ÄÅÓËÒÉÐÔÏÒ ÂÕÆÅÒÁ É ÒÅÚÕÌØÔÁÔÏ× */
	mcap_data_buf_t
			data_buf;  /* ÏÂÌÁÓÔØ ÂÕÆÅÒÁ ÄÁÎÎÙÈ ÄÌÑ ÐÅÒÅÄÁÞÉ × ËÁÎÁÌ */
} mcap_iosubdbuf_t;

/*  ïÐÉÓÁÎÉÑ ÓÔÒÕËÔÕÒÙ ÜÌÅÍÅÎÔÁ ËÁÒÔÙ ÏÂÍÅÎÏ×.
	äÁÎÎÁÑ ÓÔÒÕËÔÕÒÁ Ñ×ÌÑÅÔÓÑ ×ÎÕÔÒÅÎÎÉÍ ÐÒÅÄÓÔÁ×ÌÅÎÉÅÍ ËÁÒÔÙ ÏÂÍÅÎÏ×
	É ÎÁ ÐÏÌØÚÏ×ÁÔÅÌÑ ÎÅÐÏÓÒÅÄÓÔ×ÅÎÎÏ ÎÅ ×ÙÈÏÄÉÔ.
*/
typedef struct mcap_iomap_subd {
	mcap_iosubdbuf_t	write;  /* ÂÕÆÅÒ ÐÅÒÅÄÁÔÞÉËÁ */
	mcap_iosubdbuf_t	read;	/* ÂÕÆÅÒ ÐÒÉÅÍÎÉËÁ */

} mcap_iomap_subd_t;

/*  ïÐÉÓÁÎÉÑ ËÁÒÔÙ ÏÂÍÅÎÏ× - ÍÁÓÓÉ× ÂÕÆÅÒÏ× ×ÓÅÈ ÐÏÄÕÓÔÒÏÊÓÔ×.
	äÁÎÎÁÑ ÓÔÒÕËÔÕÒÁ Ñ×ÌÑÅÔÓÑ ×ÎÕÔÒÅÎÎÉÍ ÐÒÅÄÓÔÁ×ÌÅÎÉÅÍ ËÁÒÔÙ ÏÂÍÅÎÏ×
	ÏËÏÎÅÞÎÉËÁ É ÎÁ ÐÏÌØÚÏ×ÁÔÅÌÑ ÎÅÐÏÓÒÅÄÓÔ×ÅÎÎÏ ÎÅ ×ÙÈÏÄÉÔ.
*/
typedef mcap_iomap_subd_t		mcap_iomap_t;

/* ïÐÉÓÁÎÉÅ ÓÔÒÕËÔÕÒÙ ÐÁÒÁÍÅÔÒÏ× ÉÎÉÃÉÁÌÉÚÁÃÉÉ ÂÕÆÅÒÏ× ÏÂÍÅÎÁ ÄÁÎÎÙÍÉ */
typedef struct mcap_init_iomap {
	u_short	buf_num;			/* ÞÉÓÌÏ ÂÕÆÅÒÏ× ÏÂÍÅÎÁ ÄÁÎÎÙÍÉ */
	u_short	max_data_buf_trans_size;	/* ÍÁËÓÉÍÁÌØÎÙÊ ÒÁÚÍÅÒ ÐÅÒÅÄÁÀÝÅÇÏ ÂÕÆÅÒÁ ÄÁÎÎÙÈ */
	u_short	max_data_buf_reciv_size;	/* ÍÁËÓÉÍÁÌØÎÙÊ ÒÁÚÍÅÒ ÐÒÉÅÍÎÏÇÏ ÂÕÆÅÒÁ ÄÁÎÎÙÈ */
	size_t		*real_buf_size_p;
								/* ÕËÁÚÁÔÅÌØ ÎÁ ÐÅÒÅÍÅÎÎÕÀ */
								/* ÄÌÑ ÚÁÐÉÓÉ ÒÅÁÌØÎÏÇÏ */
								/* ÒÁÚÍÅÒÁ ÂÕÆÅÒÁ ËÁÒÔÙ */
								/* ÏÂÍÅÎÏ× c ÕÞÅÔÏÍ */
								/* ÎÅÏÂÈÏÄÉÍÙÈ ËÒÁÔÎÏÓÔÅÊ */
								/* ÁÄÒÅÓÏ× É ÒÁÚÍÅÒÏ×*/
	int		*error_code_p;				/* ÕËÁÚÁÔÅÌØ ÎÁ ÐÅÒÅÍÅÎÎÕÀ */
								/* ÄÌÑ ÚÁÐÉÓÉ ËÏÄÁ ÏÛÉÂËÉ, */
								/* ÅÓÌÉ ÔÁËÏ×ÁÑ ÂÕÄÅÔ */
								/* ÏÂÎÁÒÕÖÅÎÁ × ÐÒÏÃÅÓÓÅ */
								/* ÉÎÉÃÉÁÌÉÚÁÃÉÉ */
} mcap_init_iomap_t;


/* ïÐÉÓÁÎÉÅ ÓÔÒÕËÔÕÒÙ ÐÁÒÁÍÅÔÒÏ× ÏÓÔÁÎÏ×Á ÏÂÍÅÎÏ× É ÚÁËÒÙÔÉÑ ËÁÎÁÌÁ */
#ifdef MCAP_OLD_VERSION
typedef struct mcap_halt_trans {
	int		waiting_time;				/* ×ÒÅÍÑ ÏÖÉÄÁÎÉÑ ÚÁ×ÅÒÛÅÎÉÑ */
								/* ÐÏÓÌÅÄÎÅÇÏ ÏÂÍÅÎÁ É */
								/* ÚÁËÒÙÔÉÑ ËÁÎÁÌÁ, ÐÏÓÌÅ */
								/* ËÏÔÏÒÏÇÏ ×ÓÅ ÂÕÆÅÒÁ */
								/* ÏÂÍÅÎÏ× ÕÄÁÌÑÀÔÓÑ */
								/* ÅÓÌÉ 0, ÔÏ ÏÖÉÄÁÎÉÑ ÎÅÔ */
								/* ÂÕÆÅÒÁ ÂÕÄÕÔ ÕÄÁÌÅÎÙ ÐÒÉ */
								/* ÓÌÅÄÕÀÝÅÊ ÉÎÉÃÉÁÌÉÚÁÃÉÉ */
								/* ÉÌÉ ÐÒÉ ÚÁËÒÙÔÉÉ ÕÓÔ-×Á */
								/* ( close() ) */
								/* ÅÓÌÉ < 0, ÔÏ ÏÖÉÄÁÔØ */
								/* ÓÌÅÄÕÅÔ ÎÅ ÂÏÌÅÅ ×ÒÅÍÅÎÉ */
								/* ÚÁÄÁÎÎÏÇÏ ÐÒÉ ÏÔËÒÙÔÉÉ */
								/* × ËÁÞÅÓÔ×Å ÚÎÁÞÅÎÉÑ */
								/* ÔÁÊÍÅÒÁ ÄÌÑ ËÏÎÔÒÏÌÑ */
								/* ÚÁ×ÉÓÁÎÉÑ ÏÂÍÅÎÏ× */
								/* ÅÓÌÉ > 0, ÔÏ ÜÔÏ ×ÒÅÍÑ × */
								/* ÍÉËÒÏÓÅËÕÎÄÁÈ, ÐÏÓÌÅ */
								/* ËÏÔÏÒÏÇÏ ×ÓÅ ÂÕÆÅÒÁ  */
								/* ÏÂÍÅÎÏ× ÕÄÁÌÑÀÔÓÑ */
} mcap_halt_trans_t;
#else
typedef struct mcap_halt_trans {
        int             flag_close;             		/* ÅÓÌÉ = 1, ÔÏ ÐÒÏÉÚ×ÅÓÔÉ ÏÂÝÉÊ ÓÂÒÏÓ ÍÏÄÕÌÑ, */
                                                        	/* × ÐÒÏÔÉ×ÎÏÍ ÓÌÕÞÁÅ ×ÙÄÁÔØ ËÏÍÁÎÄÕ */
                                                        	/* ÄÒÁÊ×ÅÒÕ íð ÎÁ ÚÁËÒÙÔÉÅ ËÁÎÁÌÁ */
} mcap_halt_trans_t;
#endif /* MCAP_OLD_VERSION */

/*  óÔÒÕËÔÕÒÁ, ÏÐÉÓÙ×ÁÀÝÁÑ ÉÎÆÏÒÍÁÃÉÀ Ï Ó×ÑÚÉ ÕÓÔÒÏÊÓÔ×Á Ó ÏÔËÒÙÔÙÍ
    ÄÅÓËÒÉÐÔÏÒÏÍ ÆÁÊÌÁ.
    ðÏÄÁÅÔÓÑ ÄÌÑ ÚÁÐÏÌÎÅÎÉÑ × ÓÏÏÔ×ÅÔÓÔ×ÕÀÝÕÀ ËÏÍÁÎÄÕ, ÒÅÁÌÉÚÏ×ÁÎÎÕÀ ÞÅÒÅÚ
    ioctl() ×ÙÚÏ×
*/
typedef	struct mcap_dev_info {
	int			instance;	/* ÜËÚÅÍÐÌÑÒ MCAP */
	int			channel;	/* ÎÏÍÅÒ ËÁÎÁÌÁ */
} mcap_dev_info_t;

#ifdef MCAP_OLD_VERSION
typedef struct mcap_drv_info
{
	int			sbus_clock_freq;	/* ÞÁÓÔÏÔÁ ÓÉÎÈÒÏÎÉÚÁÃÉÉ SBus */
	int			sbus_nsec_cycle;	/* ÐÅÒÉÏÄ ÓÌÅÄÏ×ÁÎÉÑ tick-Ï× SBus */
	int			mp_clock_freq;		/* ÞÁÓÔÏÔÁ ÓÉÎÈÒÏÎÉÚÁÃÉÉ SBus */
							/* ÍÉËÒÏÐÒÏÃÅÓÓÏÒÁ */
	int			mp_nsec_cycle;		/* ÐÅÒÉÏÄ ÓÌÅÄÏ×ÁÎÉÑ tick-Ï× íð */
	e90_unit_t		device_type;		/* ÔÉÐ ÕÓÔÒÏÊÓÔ×Á */
	int			mp_rom_drv_enable;	/* ÏÔËÒÙÔÉÅ ÄÒÁÊ×ÅÒÁ ðúõ */
	hrtime_t    		cur_hr_time;		/* ÔÅËÕÝÅÅ ×ÒÅÍÑ × ÎÓÅË */
}	mcap_drv_info_t;
#else
typedef struct mcap_drv_info
{
        int                     sbus_clock_freq;        /* ÞÁÓÔÏÔÁ ÓÉÎÈÒÏÎÉÚÁÃÉÉ SBus */
        int                     sbus_nsec_cycle;        /* ÐÅÒÉÏÄ ÓÌÅÄÏ×ÁÎÉÑ tick-Ï× SBus */
        int                     mp_clock_freq;          /* ÞÁÓÔÏÔÁ ÓÉÎÈÒÏÎÉÚÁÃÉÉ SBus */
                                                        /* ÍÉËÒÏÐÒÏÃÅÓÓÏÒÁ */
        int                     mp_nsec_cycle;          /* ÐÅÒÉÏÄ ÓÌÅÄÏ×ÁÎÉÑ tick-Ï× íð */
        int                     mp_rom_drv_enable;      /* ÏÔËÒÙÔÉÅ ÄÒÁÊ×ÅÒÁ ðúõ */
        hrtime_t    		cur_hr_time;            /* ÔÅËÕÝÅÅ ×ÒÅÍÑ × ÎÓÅË */
}       mcap_drv_info_t;

typedef struct mcap_bmem_trans_desk
{
        caddr_t      mem_address;               /* SPARC memory address */
        caddr_t      mp_bmem_address;   	/* MP base memory address */
        size_t       byte_size;                 /* byte size of loaded code */
 }      mcap_bmem_trans_desk_t;

#endif /* MCAP_OLD_VERSION */

/* óÔÒÕËÔÕÒÁ, ÏÐÉÓÙ×ÁÀÝÁÑ ÉÎÆÏÒÍÁÃÉÀ ÄÌÑ ËÏÍÁÎÄÙ MCAPIO_INTR_TIME_WAIT */
#ifdef MCAP_OLD_VERSION
typedef struct mcap_intr_wait {
	u_long	intr_wait_time;			/* ×ÒÅÍÑ ÏÖÉÄÁÎÉÑ ÐÒÅÒÙ×ÁÎÉÑ (ÍËÓÅË) */
	u_short	event_intr;			/* ËÏÄ ÓÏÂÙÔÉÑ */
	u_short	event_intr_trans[MCAP_SUBDEV_BUF_NUM];	/* ËÏÄ ÓÏÂÙÔÉÑ ÐÅÒÅÄÁÀÝÉÈ ËÁÎÁÌÏ× */
	u_short	event_intr_reciv[MCAP_SUBDEV_BUF_NUM];	/* ËÏÄ ÓÏÂÙÔÉÑ ÐÒÉÅÍÎÙÈ ËÁÎÁÌÏ× */
	hrtime_t	time_get_intr_device; 		/* ô ÐÏÌÕÞÅÎÉÑ ÐÒÅÒÙ×ÁÎÉÑ ÏÔ ÁÄÁÐÔÅÒÁ */
} mcap_intr_wait_t;
#else
typedef struct mcap_intr_wait {
        u_long  intr_wait_time;                 /* ×ÒÅÍÑ ÏÖÉÄÁÎÉÑ ÐÒÅÒÙ×ÁÎÉÑ (ÍËÓÅË) */
        u_short event_intr;                     /* ËÏÄ ÓÏÂÙÔÉÑ */
        u_short event_intr_trans[MCAP_SUBDEV_BUF_NUM];   /* ËÏÄ ÓÏÂÙÔÉÑ ÐÅÒÅÄÁÀÝÉÈ ËÁÎÁÌÏ× */
        u_short event_intr_reciv[MCAP_SUBDEV_BUF_NUM];   /* ËÏÄ ÓÏÂÙÔÉÑ ÐÒÉ£ÍÎÙÈ ËÁÎÁÌÏ× */
        hrtime_t        time_get_intr_device; 		 /* ô ÐÏÌÕÞÅÎÉÑ ÐÒÅÒÙ×ÁÎÉÑ ÏÔ ÁÄÁÐÔÅÒÁ */
        u_short         num_intr_rosh;  		 /* ËÏÌ-×Ï ÐÒÅÒÙ×ÁÎÉÊ ÐÏ òïû */
} mcap_intr_wait_t;
#endif /* MCAP_OLD_VERSION */

/* óÔÒÕËÔÕÒÁ, ÏÐÉÓÙ×ÁÀÝÁÑ ÉÎÆÏÒÍÁÃÉÀ Ï ÒÅÚÕÌØÔÁÔÅ ÐÒÉÅÍÁ ÚÁÑ×ËÉ ÎÁ ×ÙÄÁÞÕ */
/* ÐÁËÅÔ ÄÁÎÎÙÈ × ËÁÎÁÌ */
typedef struct user_inform_trans_ch {
	u_short		trans_error_code;  	/* ËÏÄ ÏÛÉÂËÉ, ÏÂÎÁÒÕÖÅÎÎÏÊ ÄÒÁÊ×ÅÒÏÍ Mð */
	u_short		word_state_channel; 	/* ÓÏÓÔÏÑÎÉÅ ËÁÎÁÌÁ (ÏÛÉÂËÉ ÏÂÍÅÎÁ) */
	u_short		num_zok_channel;    	/* ËÏÌ-×Ï úïë */
} get_user_inform_trans_channel_t;

/* óÔÒÕËÔÕÒÁ, ÏÐÉÓÙ×ÁÀÝÁÑ ÉÎÆÏÒÍÁÃÉÀ Ï ÓÏÓÔÏÑÎÉÉ ËÁÎÁÌÁ ÏÂÍÅÎÁ */
typedef struct user_inform_state_ch {
	u_short	word_state_trans_ch; 		/* ÚÎÁÞÅÎÉÅ ÒÅÇÉÓÔÒÁ ÓÏÓÔÏÑÎÉÑ */
						/* ÐÅÒÅÄÁÔÞÉËÁ ËÁÎÁÌÁ (ÏÛÉÂËÉ ÏÂÍÅÎÁ) */
	u_short	number_trans_words;  		/* ËÏÌÉÞÅÓÔ×Ï ÓÌÏ×Á, ÐÅÒÅÄÁÎÎÙÈ × ËÁÎÁÌ */
	u_short	word_state_reciv_ch; 		/* ÚÎÁÞÅÎÉÅ ÒÅÇÉÓÔÒÁ ÓÏÓÔÏÑÎÉÑ */
						/* ÐÒÉÅÍÎÉËÁ ËÁÎÁÌÁ (ÏÛÉÂËÉ ÏÂÍÅÎÁ) */
	u_short	number_reciv_words;  		/* ÔÅËÕÝÉÊ ÎÏÍÅÒ ÚÁÐÉÓÁÎÎÏÇÏ ÓÌÏ×Á × */
						/* ÂÕÆÅÒ ÐÏÌØÚÏ×ÁÔÅÌÑ ÏÔÎÏÓÉÔÅÌØÎÏ */
						/* ÎÁÞÁÌÁ ÄÁÎÎÏÇÏ ÂÕÆÅÒÁ */
	u_short	checked_error_code;  		/* ËÏÄ ÏÛÉÂËÉ, ÏÂÎÁÒÕÖÅÎÎÏÊ ÄÒÁÊ×ÅÒÏÍ Mð */
	u_short	num_zok;  			/* ËÏÌ-×Ï úïë, ÐÏÌÕÞÅÎÎÙÈ ÁÄÁÐÔÅÒÏÍ */
	u_short	num_ini;  			/* ËÏÌ-×Ï éîé, ÐÏÌÕÞÅÎÎÙÈ ÁÄÁÐÔÅÒÏÍ */
} get_user_inform_state_channel_t;

/* óÔÒÕËÔÕÒÁ, ÏÐÉÓÙ×ÁÀÝÁÑ ÉÎÆÏÒÍÁÃÉÀ Ï ËÁÎÁÌÁÈ */
typedef struct inform_of_chs {
/* ôÅËÕÝÅÅ ËÏÌ-×Ï ÐÅÒÅÄÁÎÎÙÈ ËÏÄÏ× ÐÏËÏÑ ÁÄÁÐÔÅÒÏÍ */
	u_short		cur_num_ecode_trans[MCAP_SUBDEV_BUF_NUM];
/* ôÅËÕÝÅÅ ËÏÌ-×Ï ÐÒÉÎÑÔÙÈ ËÏÄÏ× ÐÏËÏÑ ÁÄÁÐÔÅÒÏÍ */
	u_short		cur_num_ecode_reciv[MCAP_SUBDEV_BUF_NUM];
/* ôÅËÕÝÅÅ ËÏÌ-×Ï ÐÅÒÅÄÁÎÎÙÈ ÓÌÏ× ÄÁÎÎÙÈ */
	u_short		cur_num_trans_word[MCAP_SUBDEV_BUF_NUM];
/* ôÅËÕÝÅÅ ËÏÌ-×Ï ÐÒÉÎÑÔÙÈ ÓÌÏ× ÄÁÎÎÙÈ */
	u_short		cur_num_reciv_word[MCAP_SUBDEV_BUF_NUM];
/* ôÅËÕÝÉÊ ÎÏÍÅÒ ÐÒÉÎÑÔÏÇÏ ÐÁËÅÔÁ ÄÁÎÎÙÈ */
	u_short		cur_num_accept_packet[MCAP_SUBDEV_BUF_NUM];
/* CÕÍÍÁÒÎÏÅ ËÏÌ-×Ï ÐÒÉÎÑÔÙÈ ËÏÄÏ× ÐÏËÏÑ × ÐÒÅÄÙÄÕÝÅÍ ÓÅÁÎÓÅ  */
	u_short		amount_ease_code[MCAP_SUBDEV_BUF_NUM];
/* ôÅËÕÝÉÊ ÕËÁÚÁÔÅÌØ ÐÒÉÓÍÎÏÇÏ ÂÕÆÅÒÁ */
	u_short		cur_pointer_reciv[MCAP_SUBDEV_BUF_NUM];
} get_user_inform_of_chs_t;

#ifndef MCAP_OLD_VERSION
typedef struct mcap_rezult_reciv {
        u_short amount_code_secrete;    /* ËÏÌ-×Ï ËÏÄÏ× ÎÁÞÁÌÁ ÓÏÏÂÝÅÎÉÑ */
        u_short escape_index_write;     /* ÎÁÞÁÌØÎÙÊ ÉÎÄÅËÓ ÚÁÐÉÓÉ ÄÁÎÎÙÈ × âð */
        u_short indent_data_size;       /* ÔÒÅÂÕÅÍÏÅ ËÏÌÉÞÅÓÔ×Ï ÓÌÏ× */
        u_short real_data_size;         /* ÒÅÁÌØÎÏÅ ËÏÌ-×Ï ÐÒÉÎÑÔÙÈ ÓÌÏ× */
        u_short not_process_word;       /* ËÏÌ-×Ï îå ÐÅÒÅÐÉÓÁÎÎÙÈ ÓÌÏ× ÉÚ ðâë */
        u_short channel_check_word;     /* ÓÏÓÔÏÑÎÉÅ ËÁÎÁÌÁ (ÏÛÉÂËÉ ÏÂÍÅÎÁ) */
        u_short first_error;            /* ÎÏÍÅÒ ÓÌÏ×Á, ÐÒÉ ×ÙÄÁÞÅ (ÐÒÉ£ÍÅ) */
                                        /* ËÏÔÏÒÏÇÏ ÚÁÆÉËÓÉÒÏ×ÁÎÁ ÐÅÒ×ÁÑ ÏÛÉÂËÁ */
                                        /* ÏÂÍÅÎÁ */
        u_short num_error;              /* ËÏÌÉÞÅÓÔ×Ï ÏÛÉÂÏË ÏÂÍÅÎÁ */
        u_short exchange_error_code;    /* ËÏÄ ÏÛÉÂËÉ, ÏÂÎÁÒÕÖÅÎÎÏÊ ÄÒÁÊ×ÅÒÏÍ Mð */
        u_short signal_adapter;         /* ËÏÌ-×Ï ÓÉÇÎÁÌÏ× éîé */
        u_short cur_ease_code;          /* ÔÅËÕÝÅÅ ËÏÌ-×Ï ÐÒÉÎÑÔÙÈ ëð */
        u_short n_process;              /* ËÏÌ-×Ï ÐÅÒÅÐÉÓÁÎÎÙÈ ÓÌÏ× */
        u_short n_read;                 /* ËÏÌ-×Ï ÓÌÏ× × âðä */
} mcap_rezult_reciv_t;

#endif /* MCAP_OLD_VERSION */

/* óÔÒÕËÔÕÒÁ, ÏÐÉÓÙ×ÁÀÝÁÑ ÉÎÆÏÒÍÁÃÉÀ Ï ÒÅÚÕÌØÔÁÔÅ ÏÔËÌÀÞÅÎÉÑ ËÁÎÁÌÏ× */
/* ÏÔ ÌÉÎÉÊ Ó×ÑÚÉ */
typedef struct user_inform_turn_off_ch {
	u_short	turn_error;  	/* ËÏÄ ÏÛÉÂËÉ, ÏÂÎÁÒÕÖÅÎÎÏÊ ÄÒÁÊ×ÅÒÏÍ Mð */
} get_user_inform_turn_off_ch_t;

/* éÎÉÃÉÁÌÉÚÁÃÉÉ ÂÕÆÅÒÏ× ÏÂÍÅÎÁ ÄÁÎÎÙÍÉ */
typedef struct delivery_note_message
{
	char		code_msg[128];		/* ÔÅËÓÔ ÐÒÅÄÕÐÒÅÖÄÅÎÉÑ */
	char		name_user[32];		/* ÉÍÑ ÐÏÌØÚÏ×ÁÔÅÌÑ */
} delivery_note_message_t;

#ifndef MCAP_OLD_VERSION
/* ðÏÌÕÞÅÎÉÅ ÉÎÆÏÒÍÁÃÉÉ Ï ÐÒÅÒÙ×ÁÎÉÑÈ ÐÏ òïû */
typedef struct intr_rosh
{
        u_short         num_intr_rosh;  /* ËÏÌ-×Ï ÐÒÅÒÙ×ÁÎÉÊ ÐÏ òïû */
} mcap_intr_rosh_t;
#endif /* MCAP_OLD_VERSION */

/* âÕÆÅÒÁ ÄÁÎÎÙÈ ÁÄÁÐÔÅÒÁ */
typedef struct buf_data {
	u_int	area_subbuf0[16];
	u_int	area_subbuf1[16];
} buf_data_t;

/* õÐÒÁ×ÌÑÀÝÁÑ ÉÎÆÏÒÍÁÃÉÑ ÂÕÆÅÒÏ× ÁÄÁÐÔÅÒÁ */
typedef struct buf_args {
	u_int	USK; /* õóë */
	u_int	AC0; /* áó0 */
	u_int	SKB; /* óëâ */
	u_int	AC1; /* áó1 */
} buf_args_t;

/* ó×ÑÚØ ÐÒÏÇÒÁÍÍÙ ÔÅÓÔÁ É âïúõ */
typedef struct mcap_area_bozu {
	u_int		val_reg[40]; 			/* úÎÁÞÅÎÉÑ ÒÅÇÉÓÔÒÏ× É ÔÒÉÇÇÅÒÏ× */
	u_int		val_arg[24]; 			/* ðÁÒÁÍÅÔÒÙ ÚÁÄÁÎÉÊ */
	u_int		var_prog[76]; 			/* ðÅÒÅÍÅÎÎÙÅ ÄÒÁÊ×ÅÒÁ íð */
	u_int		mac_stat_mp[67]; 		/* ïÂÌÁÓÔØ ÓÔÁÔÉÓÔÉËÉ */
	u_int		bd_stat_drv[765]; 		/* ïÂÌÁÓÔØ ÔÒÁÓÓÙ íð */
	buf_data_t  	buf_data[MCAP_SUBDEV_BUF_NUM*4];/* éÎÆÏÒÍÁÃÉÑ ÂÕÆÅÒÏ× ÄÁÎÎÙÈ ÁÄÁÐÔÅÒÁ */
	buf_args_t  	init_buf_data[MCAP_SUBDEV_BUF_NUM*4];	/* õÐÒÁ×ÌÑÀÝÁÑ ÉÎÆÏÒÍÁÃÉÑ ÂÕÆÅÒÏ× ÄÁÎÎÙÈ ÁÄÁÐÔÅÒÁ */
	buf_data_t  	buf_mp;				/* éÎÆÏÒÍÁÃÉÑ ÂÕÆÅÒÁ íð */
	buf_args_t  	init_buf_mp;			/* õÐÒÁ×ÌÑÀÝÁÑ ÉÎÆÏÒÍÁÃÉÑ ÂÕÆÅÒÁ íð */
	u_int		task[36]; /* úÁÄÁÎÉÅ ÄÌÑ ÄÒÁÊ×ÅÒÁ íð */
	u_int		answer[16]; /* úÁÄÁÎÉÅ ÄÌÑ ÄÒÁÊ×ÅÒÁ ÷ë */
	u_int		flag_drv; /* ðÒÉÚÎÁË ÒÁÂÏÔÙ íð */
	u_int		ANSWER_INTR[2]; /* ðÒÅÒÙ×ÁÎÉÅ ÄÒÁÊ×ÅÒÁ ÷ë */
	u_int		flag_intr; /* ðÒÉÚÎÁË ×ÙÄÁÞÉ ÐÒÅÒÙ×ÁÎÉÑ ÄÒÁÊ×ÅÒÕ ÷ë */
} mcap_area_bozu_t;

#ifdef	__cplusplus
}
#endif

#endif	/* _UAPI__LINUX_MCAP_IO_H__ */
