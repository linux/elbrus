#ifndef _UAPI__USER_INTF_H__
#define _UAPI__USER_INTF_H__

#ifdef	__cplusplus
extern "C" {
#endif

/* user_intf.h

	result 	reqlen acclen errno

*/


#define MAX_CHANNEL	4 /* 8 eai => MAX_CHANNEL in*/


/*
	 ÂÌÏË ÐÁÒÁÍÅÔÒÏ× IOCTL-ÏÐÅÒÁÃÉÊ
*/
typedef struct mbkp_ioc_parm {
	size_t	reqlen;		/* ÚÁÐÒÏÛÅÎÎÏÅ ÞÉÓÌÏ ÂÁÊÔ I/O 	*/
	size_t	acclen;		/* ÆÁËÔÉÞÅÓËÏÅ ÞÉÓÌÏ ÂÁÊÔ I/O 	*/
	char	err_no;		/* ÏÔ ÄÒÁÊ×ÅÒÁ: ËÏÄ ÏÛÉÂËÉ MBKP_E_... */ 
	char	rwmode;		/* ÄÒÁÊ×ÅÒÕ:    ÍÏÄÉÆÉËÁÔÏÒ IOCTL-ÏÐÅÒÁÃÉÊ */
} mbkp_ioc_parm_t;

/*
	ÍÏÄÏÆÉËÁÔÏÒ IOCTL ÏÐÅÒÁÃÉÊ ( × ÐÏÌÅ mbkp_ioc_parmÀ.errno)
*/ 

#define MBKP_IOC_WAIT	     1 /* ÓÉÎÈÒÏÎÎÙÊ ÏÂÍÅÎ 			*/
#define MBKP_IOC_NOWAIT	     2 /* ÁÓÉÎÈÒÏÎÎÙÊ ÏÂÍÅÎ 			*/
#define MBKP_IOC_CHECK	     3 /* ÖÄÁÔØ ÚÁ×ÅÒÛÅÎÉÅ ÎÁÞÁÔÏÇÏ ÏÂÍÅÎÁ !#k!	*/
#define MBKP_IOC_POLL	     4 /* ÏÐÒÏÓ ËÁÎÁÌÁ tr/rcv 			*/

/*
	ËÏÄÙ IOCTL ÏÐÅÒÁÃÉÊ
*/ 

#define MBKP_IOC_ALLOCB	     2 /* ×ÙÄÅÌÉÔØ ÂÕÆÅÒ ÄÌÉÎÙ reqlen 		*/
#define MBKP_IOC_READ	     3 /* ÞÉÔÁÔØ ÂÌÏË ÉÚ ÐÅÒ×ÏÇÏ ËÁÎÁÌÁ	 	*/
#define MBKP_IOC_WRITE	     4 /* ÐÉÓÁÔØ ÂÌÏË ÄÌÉÎÙ reqlen 		*/
#define MBKP_TIMER_FOR_READ  5 /* ÕÓÔÁÎÏ×ÉÔØ ÔÁÊÍÅÒ ÞÔÅÎÉÑ reqlen ÍËÓÅË */
#define MBKP_TIMER_FOR_WRITE 6 /* ÕÓÔÁÎÏ×ÉÔØ ÔÁÊÍÅÒ ÚÁÐÉÓÉ reqlen ÍËÓÅË */
#define MBKP_IOC_DE 	     7 /* ÒÁÚÒÅÛÉÔØ ÐÒÉÅÍ ÄÅÓËÒÉÐÔÏÒÁ 		*/
#define MBKP_IOC_DW	     8 /* ÐÉÓÁÔØ ÄÅÓËÒÉÐÔÏÒ	 		*/
#define MBKP_IOC_DR	     9 /* ÞÉÔÁÔØ ÄÅÓËÒÉÐÔÏÒ	 		*/
#define MBKP_SELF_TEST	     0x99

/* ++ */
#define MBKP_IOC_SETRES	    10 /* ×ËÌÀÞÉÔØ ÒÅÚÅÒ×ÎÙÅ ËÁÎÁÌÙ		*/
#define MBKP_IOC_RETMAIN    11 /* ×ËÌÀÞÉÔØ ÏÓÎÏ×ÎÙÅ ËÁÎÁÌÙ		*/
#define MBKP_IOC_SETTRBA    12 /* ÒÁÚÒÅÛÉÔØ ÐÅÒÅÄÁÞÕ ÄÅÓËÒÉÐÔÏÒÏ×		*/
#define MBKP_IOC_WRCMD	    14 /* ÐÉÓÁÔØ ËÏÍÁÎÄÕ ÉÚ reqlen		*/

#define MBKP_IOC_RDR 	    15
#define MBKP_IOC_WRR 	    16

#define MBKP_IOC_RDALT	    23 /* ÞÉÔÁÔØ ÂÌÏË ÉÚ 2ÇÏ ËÁÎÁÌÁ 		*/
#define MBKP_IOC_RDESCALT   29 /* ÞÉÔÁÔØ ÄÅÓËÒÉÐÔÏÒ ÉÚ 2ÇÏ ËÁÎÁÌÁ	*/

#define MBKP_IOC_DEBUG	    30 

#define MBKP_IOC_IOTIME     28

/* ÄÏÐÏÌÎÉÔÅÌØÎÙÅ ÂÉÔÙ ÓÏÓÔÏÑÎÉÑ ÚÁÐÕÝÅÎÎÏÊ ÏÐÅÒÁÃÉÉ */
/* min ÂÉÔ MBKP_IO_FINISHED ÌÅ×ÅÅ ÓÔÁÒÛÅÇÏ ÂÉÔÁ MBKP_IOC_... */

#define MBKP_IO_FINISHED    0x0100	/* ÁÓÉÎÈÒ ÏÐÅÒ ÚÁ×ÅÒÛÅÎÁ ÐÏ ÐÒÅÒ */


/*
	ËÏÄÙ ÚÁ×ÅÒÛÅÎÉÑ ÏÐÅÒÁÃÉÊ × ÐÏÌÅ mbkp_ioc_parm_t.errno
	errno!= MBKP_E_NORMAL ÐÒÉ 
*/ 

#define MBKP_E_NORMAL 	 0 /* ÎÏÒÍÁÌØÎÏÅ ÚÁ×ÅÒÛÅÎÉÅ ÏÐÅÒÁÃÉÉ 	 */
#define MBKP_E_INVOP  	 1 /* ÎÅÔ ÏÐÅÒÁÃÉÉ ÉÌÉ ÁÒÇÕÍÅÎÔ 	 */
#define MBKP_E_INVAL  	 2 /* ÎÅÄÏÐÕÓÔÉÍÁÑ ÏÐÅÒÁÃÉÑ ÉÌÉ ÁÒÇÕÍÅÎÔ */
#define MBKP_E_NOBUF 	 5 /* ÎÅ×ÏÚÍÏÖÎÏ ×ÙÄÅÌÉÔØ ÂÕÆÅÒ	MBKP_IOC_ALLOCB */

#define MBKP_E_URGENT 	10 /* ÐÏ ÏÐÅÒÁÃÉÉ readf ÐÒÉÎÑÔ ÄÅÓËÒÉÐÔÏÒ */
#define MBKP_E_PENDING 	11 /* ÏÐÅÒÁÃÉÑ Ó ÍÁÓÓÉ×ÏÍ ÎÅ ÏËÏÎÞÅÎÁ	 */
#define MBKP_E_TIMER 	12 /* ÏÐÅÒÁÃÉÑ ÐÒÅËÒÁÝÅÎÁ ÐÏ ÔÁÊÍÅÒÕ 	 */	

#define MBKP_IOC_NOTRUN 14 /* ÏÐÅÒÁÃÉÑ ÎÅ ÂÙÌÁ ÎÁÞÁÔÁ 		*/ 
#define MBKP_IOC_DIFCH  15 /* ÏÐÅÒÁÃÉÑ ÂÙÌÁ ÎÁÞÁÔÁ ÐÏ ÄÒÕÇÏÍÕ ËÁÎÁÌÕ*/ 
#define MBKP_DESC_DISABLED 16 /* ÏÐÅÒ ÚÁÐ ÄÅÓË ÎÅ ÏËÏÎÞÅÎÁ ÉÌÉ ~ôòâá */

#define MBKP_ERREAD  20 /* ÏÛÉÂËÉ ÐÒÉ ÐÒÉÅÍÅ ÐÏ ËÁÎÁÌÕ 0 */
#define MBKP_ERWRITE 21 /* ÏÛÉÂËÉ ÐÒÉ ÐÒÉÅÍÅ 		 */

#define MBKP_ERREAD1 30 /* ÏÛÉÂËÉ ÐÒÉ ÐÒÉÅÍÅ ÐÏ ËÁÎÁÌÕ 1 */


struct code_msg {
	int code;
	char * msg;
};

typedef struct code_msg code_msg_t; 

code_msg_t iocerrs[] = {
	{MBKP_E_INVOP, "MBKP_E_INVOP"},
	{MBKP_E_INVAL, "MBKP_E_INVAL"},
	{MBKP_E_NOBUF, "MBKP_E_NOBUF"},
	
	{MBKP_E_PENDING, "MBKP_E_PENDING"},
	{MBKP_E_TIMER, "MBKP_E_TIMER"},
	{MBKP_DESC_DISABLED, "MBKP_DESC_DISABLED"},
	{MBKP_IOC_DIFCH, "MBKP_IOC_DIFCH"},
	{MBKP_IOC_NOTRUN, "MBKP_IOC_NOTRUN"},
	{MBKP_ERREAD, "MBKP_ERREAD"},
	{MBKP_ERREAD1, "MBKP_ERREAD1"},
	{MBKP_ERWRITE, "MBKP_ERWRITE"},
	{MBKP_E_URGENT, "MBKP_E_URGENT"},
};

code_msg_t ioctls[] = {
	{MBKP_TIMER_FOR_READ, "MBKP_TIMER_FOR_READ"}, 
	{MBKP_TIMER_FOR_WRITE, "MBKP_TIMER_FOR_WRITE"},
	{MBKP_IOC_ALLOCB, "MBKP_IOC_ALLOCB"},
	{MBKP_IOC_READ, "MBKP_IOC_READ"},
	{MBKP_IOC_WRITE, "MBKP_IOC_WRITE"},
	{MBKP_IOC_DE, "MBKP_IOC_DE"},
	{MBKP_IOC_DW, "MBKP_IOC_DW"},
	{MBKP_IOC_RDR, "MBKP_IOC_RDR"},
	{MBKP_IOC_WRR, "MBKP_IOC_WRR"},
	
};

code_msg_t rwmods[] = {
	{MBKP_IOC_WAIT, "MBKP_IOC_WAIT"}, 
	{MBKP_IOC_NOWAIT, "MBKP_IOC_NOWAIT"},
	{MBKP_IOC_CHECK, "MBKP_IOC_CHECK"},
	{MBKP_IOC_POLL, "MBKP_IOC_POLL"},
};

char * msg_by_code (int code, code_msg_t * v, int len) {
	code_msg_t * p;
	int i;
	for (i=0; i < len ; i++) {
		p = v + i;
		if (p->code == code) 
			return p->msg;
	}
	return " code=? ";
}

#ifdef	__cplusplus
}
#endif

#endif /* _UAPI__USER_INTF_H__ */
