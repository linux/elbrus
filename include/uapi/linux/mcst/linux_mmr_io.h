/*
 *
 * Ported in Linux by Alexey V. Sitnikov, alexmipt@mcst.ru, MCST, 2004
 *
 */

/* òÅÄÁËÃÉÑ ÆÁÊÌÁ mmr_io.h:
				éí÷ó - 14.07.04; home - 19.04.04.
*/
/* 12.07.04 - ×ÏÚ×ÒÁÔ Ë ÏÄÎÏÐÏÄÍÁÓÓÉ×ÎÙÍ ÂÕÆÅÒÁÍ */

/* ïÐÒÅÄÅÌÅÎÉÑ É ÓÔÒÕËÔÕÒÙ, ÉÓÐÏÌØÚÕÅÍÙÅ ÄÒÁÊ×ÅÒÏÍ ÷ë
  É ÐÏÌØÚÏ×ÁÔÅÌØÓËÉÍÉ ÐÒÏÇÒÁÍÍÁÍÉ */

#ifndef	_UAPI_LINUX_MMR_IO_H__
#define	_UAPI_LINUX_MMR_IO_H__

#ifdef	__cplusplus
extern "C" {
#endif

#include <linux/mcst/linux_me90_io.h>

#define	VERSION_LIB_PROCEDURE		0x15040401
/* ÷ÅÒÓÉÑ ÂÉÂÌÉÏÔÅÞÎÙÈ ÐÒÏÃÅÄÕÒ */

#ifndef MMR_OLD_VERSION
/* ÷ÅÒÓÉÑ ÍÏÄÕÌÑ */
#define	VERSION_MODULE_MMR	0x17040504
/* òÁÂÏÞÉÅ ×ÁÒÉÁÎÔÙ */
#define	work_var_module			16
#endif /* MMR_OLD_VERSION */

/* óÐÉÓÏË ËÏÍÁÎÄ ÒÅÁÌÉÚÏ×ÁÎÎÙÈ × ÄÒÁÊ×ÅÒÅ ÍÏÄÕÌÑ MMR É ÉÓÐÏÌÎÑÅÍÙÈ
*   ÐÏÓÒÅÄÓÔ×ÏÍ ÓÉÓÔÅÍÎÏÇÏ ×ÙÚÏ×Á ioctl()
*/

#ifdef MMR_OLD_VERSION
#define	MMRIO_READ_DEVICE_REG				(ME90_IO | 1)
#define	MMRIO_WRITE_DEVICE_REG				(ME90_IO | 2)
#define	MMRIO_INIT_BUFERS_EXCHANGE			(ME90_IO | 3)
#define	MMRIO_INIT_DEVICE				(ME90_IO | 4)
#define	MMRIO_HALT_TRANSFER_MODES			(ME90_IO | 5)
#define	MMRIO_GET_DEVICE_INFO				(ME90_IO | 6)
#define	MMRIO_INTR_TIME_WAIT				(ME90_IO | 7)

#define MMR_CNTR_ST_REG_SET_OFFSET		0x10000 /* óÍÅÝÅÎÉÅ ÏÂÌÁÓÔÉ ÒÅÇÉÓÔÒÏ× */
#define MMR_CNTR_ST_REG_SET_LEN        		0x100	/* òÁÚÍÅÒ ÏÂÌÁÓÔÉ ÒÅÇÉÓÔÒÏ× */
#define	MMR_BMEM_REG_SET_OFFSET			0x40000 /* óÍÅÝÅÎÉÅ ÏÓÎÏ×ÎÏÊ ÐÁÍÑÔÉ */
#define	MMR_BMEM_REG_SET_LEN			0x20000 /* òÁÚÍÅÒ ÏÂÌÁÓÔÉ ÏÓÎÏ×ÎÏÊ ÐÁÍÑÔÉ */

#define	BCW_HARDWARE_MMR_ERROR		12
#else
#define MMR_IO			('M' << 8)
#define	MMRIO_GET_DRIVER_INFO				(MMR_IO | 1)
#define	MMRIO_READ_DEVICE_REG				(MMR_IO | 2)
#define	MMRIO_WRITE_DEVICE_REG				(MMR_IO | 3)
#define	MMRIO_INIT_BUFERS_EXCHANGE			(MMR_IO | 4)
#define	MMRIO_INIT_DEVICE				(MMR_IO | 5)
#define	MMRIO_HALT_TRANSFER_MODES			(MMR_IO | 6)
#define	MMRIO_GET_DEVICE_INFO				(MMR_IO | 7)
#define	MMRIO_INTR_TIME_WAIT				(MMR_IO | 8)
#define	MMRIO_NUM_INTR_ROSH				(MMR_IO | 9)

#define MMR_ADDR_CNTRL_INFRM_BUFFERS_DATAS	0x1000 	/* ÁÄÒÅÓ ÏÂÌÁÓÔÉ ÐÁÍÑÔÉ */
													/* ÕÐÒÁ×ÌÑÀÝÅÊ ÉÎÆÏÒÍÁÃÉÉ */
													/* ÂÕÆÅÒÏ× ÄÁÎÎÙÈ */
#define	MMR_BMEM_REG_SET_OFFSET		0x40000 /* óÍÅÝÅÎÉÅ ÏÓÎÏ×ÎÏÊ ÐÁÍÑÔÉ */
#define	MMR_BMEM_REG_SET_LEN		0x20000 /* òÁÚÍÅÒ ÏÂÌÁÓÔÉ ïð */

#define	MMR_TIMER_INTE			1000000	/* ÚÎÁÞÅÎÉÅ ÉÎÔÅÒ×ÁÌÁ ×ÒÅÍÅÎÉ */
						/* ÏÖÉÄÁÎÉÑ ÓÏÂÙÔÉÑ (ÐÒÅÒÙ×ÁÎÉÑ) */
						/* (× ÍÉËÒÏÓÅËÕÎÄÁÈ) */
#endif /* MMR_OLD_VERSION */

//#define	MMR_HUNGUP_TIMER_INTERVAL	720000
#define	MMR_HUNGUP_TIMER_INTERVAL	360000
									/* ÚÎÁÞÅÎÉÅ ÔÁÊÍÅÒÁ ÄÌÑ */
									/* ÏÖÉÄÁÎÉÑ ÚÁ×ÅÒÛÅÎÉÑ ÏÂÍÅÎÏ× */
									/* (× ÍÉËÒÏÓÅËÕÎÄÁÈ) */

#define	MMR_IO_WRITE		0x01		/* ÚÁÐÉÓØ (ÐÅÒÅÄÁÞÁ) */
#define	MMR_IO_READ		0x02		/* ÞÔÅÎÉÅ (ÐÒÉÅÍ) */

#ifndef MMR_OLD_VERSION
#define	BLOCK_ERROR_DEV  	1   /* ÂÌÏËÉÒÏ×ÁÔØ ÏÛÉÂËÉ ÁÄÁÐÔÅÒÁ */
#define	NO_BLOCK_ERROR_DEV  	0   /* ÎÅ ÂÌÏËÉÒÏ×ÁÔØ ÏÛÉÂËÉ ÁÄÁÐÔÅÒÁ */
#define	BLOCK_ERROR_ROSH  	1   /* ÂÌÏËÉÒÏ×ÁÔØ ÐÒÅÒÙ×ÁÎÉÑ ðÒð */
				    /* ÐÏ ÚÎÁÞÅÎÉÀ òïû */
#define	NO_BLOCK_ERROR_ROSH 	0   /* ÎÅ ÂÌÏËÉÒÏ×ÁÔØ ÐÒÅÒÙ×ÁÎÉÑ ðÒð */
				    /* ÐÏ ÚÎÁÞÅÎÉÀ òïû */
#define	GISTOGR			8   /* òÁÚÍÅÒ ÍÁÓÓÉ×Á ÒÁÓÐÒÅÄÅÌÅÎÉÑ ×ÒÅÍÅÎÉ */
#define	GISTOGR_COMM		16  /* òÁÚÍÅÒ ÍÁÓÓÉ×Á ÒÁÓÐÒÅÄÅÌÅÎÉÑ ËÏÍÁÎÄ */
#endif /* MMR_OLD_VERSION */

/* òÅÖÉÍÙ ÆÕÎËÃÉÏÎÉÒÏ×ÁÎÑ ííò */
#define	MODE_CONTROLLER		   1
#define	MODE_TERMINAL		   2
#define	MODE_MONITOR		   3

#ifndef MMR_OLD_VERSION
/* ÷ÉÄÙ ÒÁÂÏÔ ÍÏÄÕÌÑ ííò */
#define	CHECKOUT		   1 /* ÎÁÌÁÄËÁ ÍÏÄÕÌÑ, ËÏÍÐÌÅËÓÎÁÑ ÎÁÌÁÄËÁ ÷ë */
#define	AUDIT_TE		   2 /* ÐÒÏ×ÅÒËÁ ÍÏÄÕÌÑ ÐÏ ôõ */
#define	ENABLE		   	   3 /* ×ËÌÀÞÅÎÉÅ ÷ë, ÐÅÒÅÚÁÇÒÕÚËÁ ÷ë */
#define	AUDIT_MNTR		   4 /* ÐÒÏ×ÅÒËÁ ÍÏÄÕÌÑ × ÒÅÖÉÍÅ ÍÏÎÉÔÏÒÁ ÐÏ ôõ */
#define	AUDIT_ONE_module   	   5 /* ÐÒÏ×ÅÒËÁ ÏÄÉÎÏÞÎÏÇÏ ÍÏÄÕÌÑ */
#endif /* MMR_OLD_VERSION */

#ifndef MMR_OLD_VERSION
/* ôÉÐ ÐÒÏ×ÅÒÏË ÍÏÄÕÌÑ ííò */
/* ðÒÏ×ÅÒËÁ ÉÓÐÏÌÎÅÎÉÑ ËÏÍÁÎÄ ÕÐÒÁ×ÌÅÎÉÑ */
#define	  fulfilment_command_cntrl 	1
/* ðÒÏ×ÅÒËÁ ÉÓÐÏÌÎÅÎÉÑ çë ÕÐÒÁ×ÌÅÎÉÑ */
#define	  realiz_generic_comm_cntrl	2
/* éÓÐÏÌÎÅÎÉÅ ËÏÍÁÎÄ ÏÂÍÅÎÁ ÄÁÎÎÙÍÉ ÍÅÖÄÕ ËÏÎÔÒÏÌÌÅÒÏÍ É
   ÏËÏÎÅÞÎÙÍ ÕÓÔÒÏÊÓÔ×ÏÍ */
#define	 exchange_between_cntrl_term	3
/* éÓÐÏÌÎÅÎÉÅ ÇÒÕÐÐÏ×ÙÈ ËÏÍÁÎÄ ÐÅÒÅÄÁÞÉ ÄÁÎÎÙÈ ËÏÎÔÒÏÌÌÅÒÏÍ
   ÏËÏÎÅÞÎÙÍ ÕÓÔÒÏÊÓÔ×ÁÍ */
#define	 transfer_from_cntrl_terminals	4
/* éÓÐÏÌÎÅÎÉÅ ËÏÍÁÎÄ ÏÂÍÅÎÁ ÄÁÎÎÙÍÉ ÍÅÖÄÕ ïõ */
#define	 exchange_between_terminals		5
/* éÓÐÏÌÎÅÎÉÅ ÇÒÕÐÐÏ×ÙÈ ËÏÍÁÎÄ ÐÅÒÅÄÁÞÁ ÄÁÎÎÙÈ ÏÔ ÏËÏÎÅÞÎÏÇÏ 
   ÕÓÔÒÏÊÓÔ×Á ÏËÏÎÅÞÎÙÍ ÕÓÔÒÏÊÓÔ×ÁÍ */
#define	 transfer_from_term_terminals	6
#endif /* MMR_OLD_VERSION */

#ifdef MMR_OLD_VERSION
#define	EXPRESS_MODE		   4
#endif /* MMR_OLD_VERSION */

#define	MMR_MAX_NUM_TERMINAL		31  /* ËÏÌ-×Ï ÏËÏÎÅÞÎÙÈ ÕÓÔÒÏÊÓÔ× */
#define	MMR_SUBADRR_NUM_TERMINAL	30  /* ËÏÌ-×Ï ÐÏÄÁÄÒÅÓÏ× ïõ */
#define	MMR_BUF_USER_NUM		33  /* ËÏÌ-×Ï ÂÕÆÅÒÏ× ÏÂÍÅÎÁ ÐÏÌØÚÏ×ÁÔÅÌÑ */
#define	MMR_BUF_ADAPTER_NUM		32  /* ËÏÌ-×Ï ÂÕÆÅÒÏ× ÏÂÍÅÎÁ ÁÄÁÐÔÅÒÁ */

#define	MMR_DMA_BURST_SIZE		8*4 /* ÒÁÚÍÅÒ ÂÌÏËÁ ÏÂÍÅÎÁ - char */

#ifdef MMR_OLD_VERSION
#define	MMR_MAX_WORD_DATA_BUF_RECIV	16  /* ÍÁËÉÍÁÌØÎÁÑ ÄÌÉÎÁ ÷ä - longs */
#define	MMR_MAX_NUM_BUF_COMM		16  /* ÏÂßÅÍ ÂÕÆÅÒÁ ËÏÍÁÎÄ ïõ - longs */
#define	MMR_MAX_NUM_SUBARRAY		1   /* ËÏÌ-×Ï ÐÏÄÍÁÓÓÉ×Ï× × ÷ä */
#define	MMR_SUBARRAY			32  /* ÒÁÚÍÅÒ ÐÏÄÍÁÓÓÉ×Á × âä - words */
#define	ADDR_STATIST_CTRL		16  /* áÄÒÅÓ ÓÔÁÔÉÓÔÉËÉ ËÏÎÔÒ-ÒÁ - words */										 
#define	MMR_MAX_WORD_DATA_BUF_TRANS	MMR_MAX_WORD_DATA_BUF_RECIV
									/* ÍÁËÉÍÁÌØÎÁÑ ÄÌÉÎÁ × ÓÌÏ×ÁÈ */
									/* ÐÅÒÅÄÁÀÝÅÇÏ ÂÕÆÅÒÁ ÏÂÍÅÎÁ */
#define	MMR_MAX_DATA_BUF_SIZE		MMR_MAX_WORD_DATA_BUF_RECIV*4
									/* ÍÁËÉÍÁÌØÎÁÑ ÄÌÉÎÁ × ÂÁÊÔÁÈ */
									/* ÂÕÆÅÒÁ ÏÂÍÅÎÁ */
#else
#define	MMR_MAX_LONGS_DATA_BUF_RECIV  16  /* ÍÁËÉÍÁÌØÎÁÑ ÄÌÉÎÁ ÷ä - long */
#define	MMR_MAX_WORDS_DATA_BUF_RECIV  32  /* ÍÁËÉÍÁÌØÎÁÑ ÄÌÉÎÁ ÷ä - short */
#define	MMR_MAX_NUM_BUF_COMM	      16  /* ÏÂßÅÍ ÂÕÆÅÒÁ ËÏÍÁÎÄ ïõ - long */
#define	ADDR_STATIST_CTRL	      16  /* áÄÒÅÓ ÓÔÁÔÉÓÔÉËÉ ËÏÎÔÒ-ÒÁ - word */
#define	MMR_MAX_LONGS_DATA_BUF_TRANS	MMR_MAX_LONGS_DATA_BUF_RECIV
									/* ÍÁËÉÍÁÌØÎÁÑ ÄÌÉÎÁ × ÓÌÏ×ÁÈ */
									/* ÐÅÒÅÄÁÀÝÅÇÏ ÂÕÆÅÒÁ ÏÂÍÅÎÁ */
#define	MMR_MAX_DATA_BUF_SIZE		MMR_MAX_LONGS_DATA_BUF_RECIV*4
									/* ÍÁËÉÍÁÌØÎÁÑ ÄÌÉÎÁ × ÂÁÊÔÁÈ */
									/* ÂÕÆÅÒÁ ÏÂÍÅÎÁ */
#endif /* MMR_OLD_VERSION */

#ifdef MMR_OLD_VERSION
#define	USK_TRANS_buf		0x36000401	/* õóë ÐÅÒÅÄÁÔÞÉËÁ ÄÌÑ ÂÕÆÅÒÏ× ÄÁÎÎÙÈ */
#define	USK_RECIV_buf		0x14000001	/* õóë ÐÒÉÅÍÎÉËÁ ÄÌÑ ÂÕÆÅÒÏ× ÄÁÎÎÙÈ */
#define	USK_CTRL_buf_comm	0x34200001	/* õóë ËÏÎÔÒÏÌÌÅÒÁ ÂÕÆÅÒÁ ËÏÍÁÎÄ */
#define	USK_TERM_buf_comm	0x00200001	/* õóë ïõ ÂÕÆÅÒÁ ËÏÍÁÎÄ */
#define	SKB_buf_comm_CNTR	0x00010001	/* óëâ ÂÕÆÅÒÁ ËÏÍÁÎÄ ËÏÎÔÒÏÌÌÅÒÁ */
#else
#define	USK_TRANS_buf		0x36000401	/* õóë ÐÅÒÅÄÁÔÞÉËÁ ÄÌÑ ÂÕÆÅÒÏ× ÄÁÎÎÙÈ */
#define	USK_RECIV_buf		0x14000001	/* õóë ÐÒÉÅÍÎÉËÁ ÄÌÑ ÂÕÆÅÒÏ× ÄÁÎÎÙÈ */
#define	USK_CTRL_buf_comm	0x34200001	/* õóë ËÏÎÔÒÏÌÌÅÒÁ ÂÕÆÅÒÁ ËÏÍÁÎÄ */
#define	USK_TERM_buf_comm	0x00200001	/* õóë ïõ ÂÕÆÅÒÁ ËÏÍÁÎÄ */
#define	SKB_buf_comm_CNTR	0x01000202	/* óëâ ÂÕÆÅÒÁ ËÏÍÁÎÄ ËÏÎÔÒÏÌÌÅÒÁ */
#define	SKB_buf_comm_MNTR	0x01001010	/* óëâ ÂÕÆÅÒÁ ËÏÍÁÎÄ ÍÏÎÔÉÏÒÁ */
#define	SKB_buf_date		0x01000202	/* óëâ ÂÕÆÅÒÁ ÄÁÎÎÙÈ */
#endif /* MMR_OLD_VERSION */

#ifdef MMR_OLD_VERSION
#define MMR_ADDR_CNTRL_INFRM_BUFFERS_DATAS		0x1000	/* ÁÄÒÅÓ ÏÂÌÁÓÔÉ ÐÁÍÑÔÉ */
								/* ÕÐÒÁ×ÌÑÀÝÅÊ ÉÎÆÏÒÍÁÃÉÉ */
								/* ÂÕÆÅÒÏ× ÄÁÎÎÙÈ */
#endif /* MMR_OLD_VERSION */

/* ëÏÄÙ ÓÏÏÂÝÅÎÉÊ ÏÂ ÏÛÉÂËÁÈ É ÓÂÏÑÈ */
#ifdef MMR_OLD_VERSION
/* ïÛÉÂÏÞÎÙÊ ÐÁÒÁÍÅÔÒ */
#define ERRPARAM  			1
/* ïÛÉÂËÉ ÓÉÓÔÅÍÙ ËÏÎÔÒÏÌÑ */
#define ERRMNTRSYS  		2
/* õÔÏÞÎÉÔØ ÓÏÓÔÏÑÎÉÅ ïõ */
#define SPECCONDTERM  		3
/* ïó ÎÅ ÓÏÏÔ×ÅÔÓÔ×ÕÅÔ ÁÄÒÅÓÕ ïõ */
#define RECWORDNOTADDTERM  	4
/* ïÛÉÂËÁ ÐÒÉ ÏÂÒÁÝÅÎÉÉ Ë ioctl */
#define ERROR_ACCESS_IOCTL 	5
/* ïÛÉÂËÁ ÐÒÉ ÉÓÐÏÌÎÅÎÉÉ ioctl() */
#define ERROR_IOCTL         7
/* ïÛÉÂËÁ ÐÒÉ ÓÌÉÞÅÎÉÉ ÉÎÆÏÒÍÁÃÉÉ */
#define ERROR_COMPARED      8
/* îÅ ÐÏÌÕÞÅÎ ÐÒÉÚÎÁË úÁÐÒÏÓ ÎÁ ÏÂÓÌÕÖÉ×ÁÎÉÅ */
#define NOT_UPKEEP      	9
/* îÏÍÅÒ ÐÏÄÍÁÓÓÉ×Á ÁÄÁÐÔÅÒÁ != ÎÏÍÅÒÕ ÐÏÄÍÁÓÓÉ× æð */
#define DIFF_NUM_SUBARRAYS 	10
#else
/* ïÛÉÂÏÞÎÙÊ ÐÁÒÁÍÅÔÒ */
#define ERRPARAM  			1
/* ïÛÉÂËÉ ÓÉÓÔÅÍÙ ËÏÎÔÒÏÌÑ */
#define ERRMNTRSYS  		2
/* õÔÏÞÎÉÔØ ÓÏÓÔÏÑÎÉÅ ïõ */
#define SPECCONDTERM  		3
/* ïó ÎÅ ÓÏÏÔ×ÅÔÓÔ×ÕÅÔ ÁÄÒÅÓÕ ïõ */
#define RECWORDNOTADDTERM  	4
/* ïÛÉÂËÁ ÐÒÉ ÏÂÒÁÝÅÎÉÉ Ë ioctl */
#define ERROR_ACCESS_IOCTL 	5
/* îÅ ÐÏÌÕÞÅÎÏ ÐÒÅÒÙ×ÁÎÉÅ É ÁÄÁÐÔÅÒ ÎÅ ÏÂÎÕÌÉÌ ÂÉÔ */
#define NOT_INTR_NO_STOP    6
/* AÄÁÐÔÅÒ ÎÅ ÏÂÎÕÌÉÌ ÂÉÔ */
#define NO_STOP_ADAPTER    7
/* ïÛÉÂËÁ ÐÒÉ ÉÓÐÏÌÎÅÎÉÉ ioctl() */
#define ERROR_IOCTL         8
/* îÅ ÐÏÌÕÞÅÎÏ ÐÒÅÒÙ×ÁÎÉÅ */
#define NOT_INTR         	9
/* ïÛÉÂËÁ òïû */
#define ERROR_ROCH          10
/* ïÛÉÂËÁ ÐÒÉ ÓÌÉÞÅÎÉÉ ÉÎÆÏÒÍÁÃÉÉ */
#define ERROR_COMPARED      11
/* îÅ ÐÏÌÕÞÅÎ ÐÒÉÚÎÁË úÁÐÒÏÓ ÎÁ ÏÂÓÌÕÖÉ×ÁÎÉÅ */
#define NOT_UPKEEP      	12
/* îÏÍÅÒ ÐÏÄÍÁÓÓÉ×Á ÁÄÁÐÔÅÒÁ != ÎÏÍÅÒÕ ÐÏÄÍÁÓÓÉ× æð */
#define DIFF_NUM_SUBARRAYS 	13
/* ÷ÅÒÓÉÑ ÄÒÁÊ×ÅÒÁ ÷ë ÎÅ ÓÏÏÔ×ÅÔÓÔ×ÕÅÔ ×ÅÒÓÉÉ ÍÏÄÕÌÑ */
#define ERRORVERDRVVK		164
#endif /* MMR_OLD_VERSION */

/* ôÉÐÙ ËÏÍÁÎÄÎÙÈ ÓÌÏ× */
#define UNARY_COMM_WORD      0
#define DOUBLE_COMM_WORD     1

/*	íÁËÒÏÓ ÄÌÑ ÐÒÅÏÂÒÁÚÏ×ÁÎÉÑ ÒÁÚÍÅÒÁ ÍÁÓÓÉ×Á ÉÚ ÜÌÅÍÅÎÔÏ× ÎÅËÏÔÏÒÏÊ ÓÔÒÕËÔÕÒÙ
	Ó ÕÞÅÔÏÍ ËÒÁÔÎÏÓÔÉ ÂÌÏËÕ ÏÂÍÅÎÁ × ÒÅÖÉÍÅ DMA.
	nelem - ÉÓÈÏÄÎÏÅ ÞÉÓÌÏ ÜÌÅÍÅÎÔÏ× × ÍÁÓÓÉ×Å
	elsize - ÒÁÚÍÅÒ ÜÌÅÍÅÎÔÁ × ÂÁÊÔÁÈ
	off - ÓÍÅÝÅÎÉÅ ÍÁÓÓÉ×Á ÏÔÎÏÓÉÔÅÌØÎÏ ÎÁÞÁÌÁ ÏÂÌÁÓÔÉ × ËÏÔÏÒÏÊ ÏÎ ÎÁÈÏÄÉÔÓÑ
	bsize - ÒÁÚÍÅÒ ÂÌÏËÁ ÏÂÍÅÎÁ × ÒÅÖÉÍÅ DMA
	ÒÅÚÕÌØÔÁÔ - ÓËÏÒÒÅËÔÉÒÏ×ÁÎÎÏÅ ÞÉÓÌÏ ÜÌÅÍÅÎÔÏ× × ÍÁÓÓÉ×Å ÄÌÑ ÏÂÅÓÐÅÞÅÎÉÑ
	ËÒÁÔÎÏÓÔÉ MMR_DMA_BURST_SIZE */

#define	TU_MMR_DMA_BURST_SIZE_ALIGN(nelem, elsize, off, bsize) ((((((nelem) * \
		(elsize) + (off) + ((bsize)-1)) / (bsize)) * (bsize)) \
		- (off)) / (elsize))

/*  ðÒÉ ÏÂÍÅÎÁÈ ÉÓÐÏÌØÚÕÅÔÓÑ ËÁÒÔÁ ÏÂÍÅÎÏ×. üÌÅÍÅÎÔÁÍÉ ËÁÒÔÙ
	Ñ×ÌÑÀÔÓÑ ÐÏÄÕÓÔÒÏÊÓÔ×Á. ëÁÖÄÙÊ ÉÚ ÎÉÈ ÉÍÅÅÔ ÓÏÂÓÔ×ÅÎÎÙÊ ÎÏÍÅÒ É ÔÉÐ -
	ÐÒÉÅÍÎÉË ÉÌÉ ÐÅÒÅÄÁÔÞÉË. ðÏÄÕÓÔÒÏÊÓÔ×Ï Ó ÏÄÎÉÍ ÎÏÍÅÒÏÍ ÍÏÖÅÔ ÂÙÔØ É
	ÐÒÉÅÍÎÉËÏÍ, É ÐÅÒÅÄÁÔÞÉËÏÍ, × ÌÀÂÏÍ ÓÌÕÞÁÅ ÏÎÉ ÒÁÓÓÍÁÔÒÉ×ÁÀÔÓÑ ËÁË
	ÎÅÚÁ×ÉÓÉÍÙÅ É ËÁÖÄÙÊ Ñ×ÌÑÅÔÓÑ ÐÏÌÎÏÃÅÎÎÙÍ ÜÌÅÍÅÎÔÏÍ ËÁÒÔÙ ÏÂÍÅÎÏ×.
	äÌÑ ÏÒÇÁÎÉÚÁÃÉÉ ÏÂÍÅÎÏ× Ó ÌÀÂÙÍ ÐÏÄÕÓÔÒÏÊÓÔ×ÏÍ ÉÓÐÏÌØÚÕÀÔÓÑ ÂÕÆÅÒÁ, ÉÍÅÀÝÉÅ
	ÓÌÅÄÕÀÝÕÀ ÓÔÒÕËÔÕÒÕ:
						  ____________________________________
	 mmr_iosubd_desc -> |     ÚÁÇÏÌÏ×ÏË ÂÕÆÅÒÁ ÏÂÍÅÎÁ      |
						|      (ÓÏÄÅÒÖÉÔ ÄÅÓËÒÉÐÔÏÒ        |
						|       ÒÅÚÕÌØÔÁÔÏ× ÏÂÍÅÎÁ)        |
						|----------------------------------|
	 mmr_data_buf    -> |      ÂÕÆÅÒ, ÐÒÉÎÉÍÁÅÍÙÈ          |
						|     ÉÌÉ ÐÅÒÅÄÁ×ÁÅÍÙÈ ÄÁÎÎÙÈ      |
						|__________________________________|

	   úÁÇÏÌÏ×ÏË ÂÕÆÅÒÁ ÏÂÍÅÎÁ Ñ×ÌÑÅÔÓÑ ÏÂÑÚÁÔÅÌØÎÙÍ ÜÌÅÍÅÎÔÏÍ ËÁÖÄÏÇÏ ÂÕÆÅÒÁ,
	ÎÁÈÏÄÉÔÓÑ × ÅÇÏ ÎÁÞÁÌÅ, ÓÏÄÅÒÖÉÔ ÎÅËÏÔÏÒÕÀ ÉÎÆÏÒÍÁÃÉÀ ÏÂ ÏÂÍÅÎÅ
	É ÉÍÅÅÔ ÆÉËÓÉÒÏ×ÁÎÎÙÊ ÒÁÚÍÅÒ - 8 ÓÌÏ× (32 ÂÁÊÔÁ), ÞÔÏ ÓÏ×ÐÁÄÁÅÔ
	Ó ÒÁÚÍÅÒÏÍ ÂÌÏËÁ ÏÂÍÅÎÁ ÍÅÖÄÕ ËÁÎÁÌÏÍ É ÏÓÎÏ×ÎÏÊ ÐÁÍÑÔØÀ × ÒÅÖÉÍÅ
	ÎÅÐÏÓÒÅÄÓÔ×ÅÎÎÏÇÏ ÄÏÓÔÕÐÁ (DMA). îÅÐÏÓÒÅÄÓÔ×ÅÎÎÏ ÄÁÎÎÙÅ,
	ÐÒÉÎÑÔÙÅ ÉÚ ËÁÎÁÌÁ ÉÌÉ ÐÅÒÅÄÁ×ÁÅÍÙÅ × ËÁÎÁÌ, ÓÌÅÄÕÀÔ ×ÓÌÅÄ ÚÁ ÚÁÇÏÌÏ×ËÏÍ.
	   ðÒÉÍÅÒ ÏÐÉÓÁÎÉÑ ÔÁËÏÇÏ ÒÏÄÁ ÓÔÒÕËÔÕÒÙ ÐÒÉ×ÅÄÅÎ ÎÉÖÅ É ÒÅËÏÍÅÎÄÕÅÔÓÑ ÄÌÑ
	ÉÓÐÏÌØÚÏ×ÁÎ × ÐÏÌØÚÏ×ÁÔÅÌØÓËÉÈ ÐÒÏÇÒÁÍÍÁÈ, ÐÏÓËÏÌØËÕ ÔÁËÁÑ ÖÅ ÓÔÒÕËÔÕÒÁ
	ÂÕÆÅÒÁ ÉÓÐÏÌØÚÕÅÔÓÑ × ÄÒÁÊ×ÅÒÅ É ÂÉÂÌÉÏÔÅÞÎÙÈ ÆÕÎËÃÉÑÈ 'open_mmr_drv.h'.
	   âÕÆÅÒÁ ÏÂÍÅÎÏ× ÄÌÑ ×ÓÅÈ ÐÏÄÕÓÔÒÏÊÓÔ×, ÓÏÓÔÁ×ÌÑÀÝÉÈ ËÁÒÔÕ ÏÂÍÅÎÏ× Ó
	ËÁÎÁÌÏÍ, × ÎÅÏÂÈÏÄÉÍÏÍ ËÏÌÉÞÅÓÔ×Å ÂÕÄÕÔ ×ÙÄÅÌÅÎÙ ÐÒÉ ÉÎÉÃÉÁÌÉÚÁÃÉÉ ÒÅÖÉÍÁ
	ÏÂÍÅÎÏ× (ÏÔËÒÙÔÉÉ ËÁÎÁÌÁ MMR, ÓÍ ÄÁÌÅÅ). õËÁÚÁÔÅÌÉ
	ÎÁ ÓÏÚÄÁÎÎÙÅ ÂÕÆÅÒÁ ËÁÒÔÙ ×ÏÚ×ÒÁÝÁÀÔÓÑ ËÁË ÒÅÚÕÌØÔÁÔ ÉÎÉÃÉÁÌÉÚÁÃÉÉ,
	× ×ÉÄÅ ÍÁÓÓÉ×Á, × ËÏÔÏÒÏÍ ÉÎÄÅËÓ ÕËÁÚÁÔÅÌÑ Ñ×ÌÑÅÔÓÑ ÎÏÍÅÒÏÍ ÄÁÎÎÏÇÏ ÂÕÆÅÒÁ
	× ÏÂÝÅÍ ÐÕÌÅ ÂÕÆÅÒÏ×. âÕÆÅÒ ËÁÒÔÙ ÏÂÍÅÎÏ× ÐÒÅÄÓÔÁ×ÌÑÅÔ ÓÏÂÏÊ ÍÁÓÓÉ×
	ÂÕÆÅÒÏ×. äÌÑ ËÁÖÄÏÇÏ ÐÏÄÕÓÔÒÏÊÓÔ×Á ÏÔ×ÅÄÅÎÙ ÐÏ Ä×Á
	ÂÕÆÅÒÁ - ÄÌÑ ÐÒÉÅÍÎÉËÁ É ÄÌÑ ÐÅÒÅÄÁÔÞÉËÁ. ëÁÖÄÙÊ ÂÕÆÅÒ ÏÂÍÅÎÁ
	ÁÂÏÎÅÎÔÁ ÉÎÉÃÉÁÌÉÚÉÒÏ×ÁÎ ËÁË ËÏÎËÒÅÔÎÙÊ ÜËÚÅÍÐÌÑÒ ÓÔÒÕËÔÕÒÙ
	'mmr_iosubdbuf_t' ÏÐÉÓÁÎÎÏÊ ÄÁÌÅÅ, ÇÄÅ ÒÁÚÍÅÒ ÂÕÆÅÒÁ ÄÁÎÎÙÈ
	MMR_MAX_DATA_BUF_SIZE ÐÏÌÁÇÁÀÔÓÑ ÒÁ×ÎÙÍÉ ÚÎÁÞÅÎÉÀ, ÐÏÄÁÎÎÏÍÕ ×
	ËÁÞÅÓÔ×Å ÐÁÒÁÍÅÔÒÁ ÉÎÉÃÉÁÌÉÚÁÃÉÉ ËÁÎÁÌÁ. ÷ ÚÁÇÏÌÏ×ËÅ ÂÕÆÅÒÁ ÕÓÔÁÎÏ×ÌÅÎÙ
	× ÓÏÏÔ×ÅÔÓÔ×ÕÀÝÉÅ ÚÎÁÞÅÎÉÑ ×ÓÅ ÆÉËÓÉÒÏ×ÁÎÎÙÅ ÐÏÌÑ).
*/

/*  óÔÒÕËÔÕÒÁ ÄÅÓËÒÉÐÔÏÒÁ ÂÕÆÅÒÁ (ÚÁÇÏÌÏ×ÏË ÂÕÆÅÒÁ ÏÂÍÅÎÁ), ÉÚ
	ËÏÔÏÒÙÈ ÓÏÓÔÏÉÔ ËÁÒÔÁ ÏÂÍÅÎÏ× ÄÌÑ  MMR ×ËÌÀÞÁÅÔ × ÓÅÂÑ ÏÐÉÓÁÎÉÅ
	ÚÁÑ×ËÉ ÎÁ ÏÂÍÅÎ.
		úÁÇÏÌÏ×ÏË ÂÕÆÅÒÁ ÏÂÍÅÎÁ Ñ×ÌÑÅÔÓÑ ÏÂÑÚÁÔÅÌØÎÙÍ ÜÌÅÍÅÎÔÏÍ ËÁÖÄÏÇÏ ÂÕÆÅÒÁ
	ÁÂÏÎÅÎÔÁ, ÎÁÈÏÄÉÔÓÑ × ÅÇÏ ÎÁÞÁÌÅ É ÄÏÌÖÅÎ ÉÍÅÔØ ÆÉËÓÉÒÏ×ÁÎÎÙÊ
	ÒÁÚÍÅÒ - 8 ÓÌÏ× (32 ÂÁÊÔÁ), ÞÔÏ ÓÏ×ÐÁÄÁÅÔ Ó ÒÁÚÍÅÒÏÍ ÂÌÏËÁ ÏÂÍÅÎÁ ÍÅÖÄÕ
	ËÁÎÁÌÏÍ É ÏÓÎÏ×ÎÏÊ ÐÁÍÑÔØÀ × ÒÅÖÉÍÅ ÎÅÐÏÓÒÅÄÓÔ×ÅÎÎÏÇÏ ÄÏÓÔÕÐÁ (DMA).
	÷ Ó×ÑÚÉ Ó ÜÔÉÍ ÓÔÒÕËÔÕÒÁ ÄÏÐÏÌÎÑÅÔÓÑ ÄÏ 8 ÓÌÏ× ÎÅÉÓÐÏÌØÚÕÅÍÙÍÉ ÐÏÌÑÍÉ.
	îÅÐÏÓÒÅÄÓÔ×ÅÎÎÏ ÄÁÎÎÙÅ ÐÒÉÎÑÔÙÅ ÉÚ ËÁÎÁÌÁ ÉÌÉ ÐÅÒÅÄÁ×ÁÅÍÙÅ × ËÁÎÁÌ ÓÌÅÄÕÀÔ
	×ÓÌÅÄ ÚÁ ÚÁÇÏÌÏ×ËÏÍ.
 */

#define MMR_TBLPPP	 0x00 /* ôÒÉÇÇÅÒ ÂÌÏËÉÒÏ×ËÉ ÐÒÅÒÙ×ÁÎÉÑ ðð ÐÏ RERR[0] != 0 */
#define MMR_TPPP     0x04 /* ôÒÉÇÇÅÒ ÐÒÅÒÙ×ÁÎÉÑ ÐÒÏÇÒÁÍÍÙ ÐÏÌØÚÏ×ÁÔÅÌÑ (ðð) */
#define MMR_TPCHSH   0x08 /* ôÒÉÇÇÅÒ ÐÒÉÚÎÁËÁ ÞÅÔÎÏÓÔÉ ÓÉÓÔÅÍÎÏÊ ÛÉÎÙ */
#define MMR_TBZ      0x0C /* ôÒÉÇÇÅÒ ÂÌÏËÉÒÏ×ËÉ ÚÎÁÞÉÍÏÓÔÉ ÒÅÇÉÓÔÒÁ õóë */
#define MMR_TZCH     0x14 /* ôÒÉÇÇÅÒ ÚÁÐÒÏÓÁ × SBus ÛÉÎÕ */
#define MMR_RERR     0x10 /* òÅÇÉÓÔÒ ÏÛÉÂÏË */
#define MMR_RNKSH    0x18 /* òÅÇÉÓÔÒ ÎÏÍÅÒÁ ËÁÎÁÌÁ × ÛÉÎÅ */
#define MMR_TZM      0x20 /* ôÒÉÇÇÅÒ ÏÂÎÕÌÅÎÉÑ ÍÏÄÕÌÑ */
#define MMR_REG_CTRL 0x24 /* òÅÇÉÓÔÒ ÕÐÒÁ×ÌÅÎÉÑ */

#ifdef MMR_OLD_VERSION
#define base_reg   		0x300001 /* âÁÚÏ×ÏÅ ÚÎÁÞÅÎÉÅ òïâ */
#else
#define base_reg   		0x300101 /* âÁÚÏ×ÏÅ ÚÎÁÞÅÎÉÅ òïâ v 4 */
#define base_reg_v5   		0x300141 /* âÁÚÏ×ÏÅ ÚÎÁÞÅÎÉÅ òïâ v 5 */
#endif /* MMR_OLD_VERSION */
#define err_answer_word		0x0405  /* éÎÔÅÇÒÉÒÏ×ÁÎÎÁÑ ÏÛÉÂËÁ ïó */
#define err_word_mntr		0x4bdf  /* éÎÔÅÇÒÉÒÏ×ÁÎÎÁÑ ÏÛÉÂËÁ ÷óë */

/* óÔÒÕËÔÕÒÁ òïâ */
#ifdef MY_DRIVER_BIG_ENDIAN
typedef union mmr_reg_general
{
	u_int          rdwr_reg_general;
	struct
		{
			u_int  bit3129 : 3; /* ôÉÐ ËÏÍÁÎÄÎÙÈ ÓÌÏ× */
			u_int  bit2824 : 5; /* áÄÒÅÓ ïõ */
			u_int  bit23	: 1; /* âÌÏËÉÒÏ×ÁÔØ ÚÁÐÉÓØ ËÏÍÁÎÄ × âë ïõ */
			u_int  bit22 	: 1; /* úÁÎÑÔÏ ïõ */
			u_int  bit21	: 1; /* âÌÏËÉÒÏ×ÁÔØ 1-Ê ËÁÎÁÌ ËÏÎÔÒÏÌÌÅÒÁ */
			u_int  bit20 	: 1; /* âÌÏËÉÒÏ×ÁÔØ 0-Ê ËÁÎÁÌ ËÏÎÔÒÏÌÌÅÒÁ */
			u_int  bit19 	: 1; /* ïõ ÔÒÅÂÕÅÔ ÏÂÓÌÕÖÉ×ÁÎÉÑ */
			u_int  bit18 	: 1; /* òÁÚÒÅÛÅÎÉÅ ÎÁ ÐÒÉÎÑÔÉÅ ÕÐÒÁ×ÌÅÎÉÑ */
			u_int  bit17 	: 1; /* âÌÏËÉÒÏ×ÁÔØ ÏÛÉÂËÉ */
			u_int  bit16 	: 1; /* éÓËÁÖÁÔØ ÞÅÔÎÏÓÔØ ÄÁÎÎÙÈ */
			u_int  bit15 	: 1; /* òÅÖÉÍ ÍÏÎÉÔÏÒÁ (ÚÁÐÕÓË) */
			u_int  bit14 	: 1; /* òÅÖÉÍ ïõ (ÚÁÐÕÓË ÎÁ ×ÙÐÏÌÎÅÎÉÅ */
			u_int  bit13 	: 1; /* òÅÖÉÍ ËÏÎÔÒÏÌÌÅÒÁ (ÚÁÐÕÓË) */
			u_int  bit1206 : 7; /* òîëû - RNKSH */
			u_int  bit0503 : 3; /* òïû - RERR */
			u_int  bit02   : 1; /* ôðþóû - TPCHSH */
			u_int  bit01   : 1; /* ôððð - TPPP */
			u_int  bit00   : 1; /* ôâìððð - TBLPPP */
		} as_bits_0;
} mmr_reg_general_t;
#else
typedef union mmr_reg_general
{
	u_int          rdwr_reg_general;
	struct
		{
			u_int  bit00   : 1; /* ôâìððð - TBLPPP */
			u_int  bit01   : 1; /* ôððð - TPPP */
			u_int  bit02   : 1; /* ôðþóû - TPCHSH */
			u_int  bit0503 : 3; /* òïû - RERR */
			u_int  bit1206 : 7; /* òîëû - RNKSH */
			u_int  bit13   : 1; /* òÅÖÉÍ ËÏÎÔÒÏÌÌÅÒÁ (ÚÁÐÕÓË) */
			u_int  bit14   : 1; /* òÅÖÉÍ ïõ (ÚÁÐÕÓË ÎÁ ×ÙÐÏÌÎÅÎÉÅ */	
			u_int  bit15   : 1; /* òÅÖÉÍ ÍÏÎÉÔÏÒÁ (ÚÁÐÕÓË) */
			u_int  bit16   : 1; /* éÓËÁÖÁÔØ ÞÅÔÎÏÓÔØ ÄÁÎÎÙÈ */
			u_int  bit17   : 1; /* âÌÏËÉÒÏ×ÁÔØ ÏÛÉÂËÉ */
			u_int  bit18   : 1; /* òÁÚÒÅÛÅÎÉÅ ÎÁ ÐÒÉÎÑÔÉÅ ÕÐÒÁ×ÌÅÎÉÑ */
			u_int  bit19   : 1; /* ïõ ÔÒÅÂÕÅÔ ÏÂÓÌÕÖÉ×ÁÎÉÑ */
			u_int  bit20   : 1; /* âÌÏËÉÒÏ×ÁÔØ 0-Ê ËÁÎÁÌ ËÏÎÔÒÏÌÌÅÒÁ */
			u_int  bit21   : 1; /* âÌÏËÉÒÏ×ÁÔØ 1-Ê ËÁÎÁÌ ËÏÎÔÒÏÌÌÅÒÁ */
			u_int  bit22   : 1; /* úÁÎÑÔÏ ïõ */
			u_int  bit23   : 1; /* âÌÏËÉÒÏ×ÁÔØ ÚÁÐÉÓØ ËÏÍÁÎÄ × âë ïõ */
			u_int  bit2824 : 5; /* áÄÒÅÓ ïõ */
			u_int  bit3129 : 3; /* ôÉÐ ËÏÍÁÎÄÎÙÈ ÓÌÏ× */
		} as_bits_0;
} mmr_reg_general_t;
#endif /* MY_DRIVER_BIG_ENDIAN */

/* îÏÍÅÒ ÐÏÄÍÁÓÓÉ×Á ÂÕÆÅÒÁ ÏÂÍÅÎÁ 
#define num_subarray_buf	as_bits_0.bit3129 */
/* áÄÒÅÓ ïõ */
#define address_terminal 	as_bits_0.bit2824
/* âÌÏËÉÒÏ×ÁÔØ ÚÁÐÉÓØ ËÏÍÁÎÄ × âë ïõ */
#define blokade_read_command as_bits_0.bit23
/* úÁÎÑÔÏ ïõ */
#define busy_terminal	   	as_bits_0.bit22
/* âÌÏËÉÒÏ×ÁÔØ 1-Ê ËÁÎÁÌ ËÏÎÔÒÏÌÌÅÒÁ */
#define blokade_channel1   	as_bits_0.bit21
/* âÌÏËÉÒÏ×ÁÔØ 0-Ê ËÁÎÁÌ ËÏÎÔÒÏÌÌÅÒÁ */
#define blokade_channel0   	as_bits_0.bit20
/* ïõ ÔÒÅÂÕÅÔ ÏÂÓÌÕÖÉ×ÁÎÉÑ */
#define service_terminal 	as_bits_0.bit19
/* òÁÚÒÅÛÅÎÉÅ ÎÁ ÐÒÉÎÑÔÉÅ ÕÐÒÁ×ÌÅÎÉÑ */
#define accept_control 		as_bits_0.bit18
/* âÌÏËÉÒÏ×ÁÔØ ÏÛÉÂËÉ */
#define blokade_error  		as_bits_0.bit17
/* éÓËÁÖÁÔØ ÞÅÔÎÏÓÔØ ÄÁÎÎÙÈ */
#define broren_parity  		as_bits_0.bit16
/* òÅÖÉÍ ÍÏÎÉÔÏÒÁ (ÚÁÐÕÓË ÎÁ ×ÙÐÏÌÎÅÎÉÅ) */
#define run_monitor   		as_bits_0.bit15
/* òÅÖÉÍ ïõ (ÚÁÐÕÓË ÎÁ ×ÙÐÏÌÎÅÎÉÅ */
#define run_terminal 		as_bits_0.bit14
/* òÅÖÉÍ ËÏÎÔÒÏÌÌÅÒÁ (ÚÁÐÕÓË ÎÁ ×ÙÐÏÌÎÅÎÉÅ ÏÞÅÒÅÄÎÏÊ çë) */
#define run_controller		as_bits_0.bit13
#define	reg_RNKSH			as_bits_0.bit1206 	/* òîëû - RNKSH */
#define	reg_RERR			as_bits_0.bit0503 	/* òïû - RERR */
#define	trg_TPCHSH			as_bits_0.bit02 	/* ôðþóû - TPCHSH */
#define	trg_TPPP			as_bits_0.bit01 	/* ôððð - TPPP */
#define	trg_TBLPPP			as_bits_0.bit00 	/* ôâìððð - TBLPPP */

/* óÔÒÕËÔÕÒÁ ÒÅÇÉÓÔÒÁ ÕÐÒÁ×ÌÅÎÉÑ */
#ifdef MY_DRIVER_BIG_ENDIAN
typedef union mmr_reg_cntrl
{
	u_int          wr_mmr_reg_cntrl;
	struct
		{
			u_int  bit3119 : 13; /* ÎÅ ÉÓÐ */
			u_int  bit1816 : 3;  /* ôÉÐ ËÏÍÁÎÄÎÙÈ ÓÌÏ× */
			u_int  bit1511 : 5;  /* áÄÒÅÓ ïõ */
			u_int  bit10   : 1;  /* âÌÏËÉÒÏ×ÁÔØ ÚÁÐÉÓØ ËÏÍÁÎÄ × âë ïõ */
			u_int  bit09   : 1;  /* úÁÎÑÔÏ ïõ */
			u_int  bit08   : 1;  /* âÌÏËÉÒÏ×ÁÔØ 1-Ê ËÁÎÁÌ ËÏÎÔÒÏÌÌÅÒÁ */
			u_int  bit07   : 1;  /* âÌÏËÉÒÏ×ÁÔØ 0-Ê ËÁÎÁÌ ËÏÎÔÒÏÌÌÅÒÁ */
			u_int  bit06   : 1;  /* ïõ ÔÒÅÂÕÅÔ ÏÂÓÌÕÖÉ×ÁÎÉÑ */
			u_int  bit05   : 1;  /* òÁÚÒÅÛÅÎÉÅ ÎÁ ÐÒÉÎÑÔÉÅ ÕÐÒÁ×ÌÅÎÉÑ */
			u_int  bit04   : 1;  /* âÌÏËÉÒÏ×ÁÔØ ÏÛÉÂËÉ */
			u_int  bit03   : 1;  /* éÓËÁÖÁÔØ ÞÅÔÎÏÓÔØ ÄÁÎÎÙÈ */
			u_int  bit02   : 1;  /* òÅÖÉÍ ÍÏÎÉÔÏÒÁ (ÚÁÐÕÓË ÎÁ ×ÙÐÏÌÎÅÎÉÅ) */
			u_int  bit01   : 1;  /* òÅÖÉÍ ïõ (ÚÁÐÕÓË ÎÁ ×ÙÐÏÌÎÅÎÉÅ */
			u_int  bit00   : 1;  /* òÅÖÉÍ ËÏÎÔÒÏÌÌÅÒÁ (ÚÁÐÕÓË ÎÁ ×ÙÐÏÌÎÅÎÉÅ ÏÞÅÒÅÄÎÏÊ çë) */
		} as_bits_1;
} mmr_reg_cntrl_t;
#else
typedef union mmr_reg_cntrl
{
	u_int          wr_mmr_reg_cntrl;
	struct
		{
			u_int  bit00   : 1;  /* òÅÖÉÍ ËÏÎÔÒÏÌÌÅÒÁ (ÚÁÐÕÓË ÎÁ ×ÙÐÏÌÎÅÎÉÅ ÏÞÅÒÅÄÎÏÊ çë) */
			u_int  bit01   : 1;  /* òÅÖÉÍ ïõ (ÚÁÐÕÓË ÎÁ ×ÙÐÏÌÎÅÎÉÅ */
			u_int  bit02   : 1;  /* òÅÖÉÍ ÍÏÎÉÔÏÒÁ (ÚÁÐÕÓË ÎÁ ×ÙÐÏÌÎÅÎÉÅ) */
			u_int  bit03   : 1;  /* éÓËÁÖÁÔØ ÞÅÔÎÏÓÔØ ÄÁÎÎÙÈ */
			u_int  bit04   : 1;  /* âÌÏËÉÒÏ×ÁÔØ ÏÛÉÂËÉ */
			u_int  bit05   : 1;  /* òÁÚÒÅÛÅÎÉÅ ÎÁ ÐÒÉÎÑÔÉÅ ÕÐÒÁ×ÌÅÎÉÑ */
			u_int  bit06   : 1;  /* ïõ ÔÒÅÂÕÅÔ ÏÂÓÌÕÖÉ×ÁÎÉÑ */
			u_int  bit07   : 1;  /* âÌÏËÉÒÏ×ÁÔØ 0-Ê ËÁÎÁÌ ËÏÎÔÒÏÌÌÅÒÁ */
			u_int  bit08   : 1;  /* âÌÏËÉÒÏ×ÁÔØ 1-Ê ËÁÎÁÌ ËÏÎÔÒÏÌÌÅÒÁ */
			u_int  bit09   : 1;  /* úÁÎÑÔÏ ïõ */
			u_int  bit10   : 1;  /* âÌÏËÉÒÏ×ÁÔØ ÚÁÐÉÓØ ËÏÍÁÎÄ × âë ïõ */
			u_int  bit1511 : 5;  /* áÄÒÅÓ ïõ */
			u_int  bit1816 : 3;  /* ôÉÐ ËÏÍÁÎÄÎÙÈ ÓÌÏ× */
			u_int  bit3119 : 13; /* ÎÅ ÉÓÐ */
		} as_bits_1;
} mmr_reg_cntrl_t;
#endif /* MY_DRIVER_BIG_ENDIAN */

/* ôÉÐ ËÏÍÁÎÄÎÙÈ ÓÌÏ× */
#define type_comm_word   	as_bits_1.bit1816 
/* áÄÒÅÓ ïõ */
#define address_term      	as_bits_1.bit1511
/* âÌÏËÉÒÏ×ÁÔØ ÚÁÐÉÓØ ËÏÍÁÎÄ × âë ïõ */
#define blokade_read_comm   as_bits_1.bit10
/* úÁÎÑÔÏ ïõ */
#define busy_term		   	as_bits_1.bit09
/* âÌÏËÉÒÏ×ÁÔØ 1-Ê ËÁÎÁÌ ËÏÎÔÒÏÌÌÅÒÁ */
#define blokade_ch1   		as_bits_1.bit08
/* âÌÏËÉÒÏ×ÁÔØ 0-Ê ËÁÎÁÌ ËÏÎÔÒÏÌÌÅÒÁ */
#define blokade_ch0   		as_bits_1.bit07
/* ïõ ÔÒÅÂÕÅÔ ÏÂÓÌÕÖÉ×ÁÎÉÑ */
#define service_term   		as_bits_1.bit06
/* òÁÚÒÅÛÅÎÉÅ ÎÁ ÐÒÉÎÑÔÉÅ ÕÐÒÁ×ÌÅÎÉÑ */
#define accept_cntrl   		as_bits_1.bit05
/* âÌÏËÉÒÏ×ÁÔØ ÏÛÉÂËÉ */
#define blokade_err		   	as_bits_1.bit04
/* éÓËÁÖÁÔØ ÞÅÔÎÏÓÔØ ÄÁÎÎÙÈ */
#define broren_part		   	as_bits_1.bit03
/* òÅÖÉÍ ÍÏÎÉÔÏÒÁ (ÚÁÐÕÓË ÎÁ ×ÙÐÏÌÎÅÎÉÅ) */
#define run_mntr   			as_bits_1.bit02
/* òÅÖÉÍ ïõ (ÚÁÐÕÓË ÎÁ ×ÙÐÏÌÎÅÎÉÅ */
#define run_term   			as_bits_1.bit01
/* òÅÖÉÍ ËÏÎÔÒÏÌÌÅÒÁ (ÚÁÐÕÓË ÎÁ ×ÙÐÏÌÎÅÎÉÅ ÏÞÅÒÅÄÎÏÊ çë) */
#define run_ctrl   			as_bits_1.bit00

/* óÔÒÕËÔÕÒÁ ÒÅÇÉÓÔÒÁ ËÏÍÁÎÄÙ ÕÐÒÁ×ÌÅÎÉÑ É ÏÂÍÅÎÁ ÄÁÎÎÙÍÉ */

#ifdef MY_DRIVER_BIG_ENDIAN
typedef union mmr_reg_comm
{
	u_short		  wr_mmr_reg_comm;
	struct
		{
			u_short  bit1511  : 5;  /* áÄÒÅÓ ïõ */
			u_short  bit10 	  : 1;  /* îÁÐÒÁ×ÌÅÎÉÅ ÐÅÒÅÄÁÞÉ */
			u_short  bit0905  : 5;  /* ðÏÄÁÄÒÅÓ/òÅÖÉÍ ÕÐÒÁ×ÌÅÎÉÑ */
			u_short  bit0400  : 5;  /* þÉÓÌÏ óä/ëÏÄ ËÏÍÁÎÄÙ */
		} as_bits_2;
} mmr_reg_comm_t;
#else
typedef union mmr_reg_comm
{
	u_short		  wr_mmr_reg_comm;
	struct
		{
			u_short  bit0400  : 5;  /* þÉÓÌÏ óä/ëÏÄ ËÏÍÁÎÄÙ */
			u_short  bit0905  : 5;  /* ðÏÄÁÄÒÅÓ/òÅÖÉÍ ÕÐÒÁ×ÌÅÎÉÑ */
			u_short  bit10 	  : 1;  /* îÁÐÒÁ×ÌÅÎÉÅ ÐÅÒÅÄÁÞÉ */
			u_short  bit1511  : 5;  /* áÄÒÅÓ ïõ */
		} as_bits_2;
} mmr_reg_comm_t;
#endif /* MY_DRIVER_BIG_ENDIAN */

/* áÄÒÅÓ ïõ */
#define addr_term      		as_bits_2.bit1511
/* îÁÐÒÁ×ÌÅÎÉÅ ÐÅÒÅÄÁÞÉ */
#define set_trans   		as_bits_2.bit10
/* ðÏÄÁÄÒÅÓ/òÅÖÉÍ ÕÐÒÁ×ÌÅÎÉÑ */
#define subaddr_cntrl   	as_bits_2.bit0905
/* þÉÓÌÏ óä/ëÏÄ ËÏÍÁÎÄÙ */
#define numword_codecomm  	as_bits_2.bit0400

/* óÔÒÕËÔÕÒÁ ÓÌÏ×Á ÄÁÎÎÙÈ */
#ifdef MY_DRIVER_BIG_ENDIAN
typedef union mmr_word_date
{
	u_short		  wr_mmr_word_date;
	struct
		{
			u_short  bit1511  : 5;  /* áÄÒÅÓ ïõ */
			u_short  bit10 	  : 1;  /* ÎÅ ÉÓÐ */
			u_short  bit0905  : 5;  /* ðÏÄÁÄÒÅÓ */
			u_short  bit0400  : 5;  /* ôÅËÕÝÉÊ ÎÏÍÅÒ óä */
		} as_bits_5;
} mmr_word_date_t;
#else
typedef union mmr_word_date
{
	u_short		  wr_mmr_word_date;
	struct
		{
			u_short  bit0400  : 5;  /* ôÅËÕÝÉÊ ÎÏÍÅÒ óä */
			u_short  bit0905  : 5;  /* ðÏÄÁÄÒÅÓ */
			u_short  bit10 	  : 1;  /* ÎÅ ÉÓÐ */
			u_short  bit1511  : 5;  /* áÄÒÅÓ ïõ */
		} as_bits_5;
} mmr_word_date_t;
#endif /* MY_DRIVER_BIG_ENDIAN */

/* áÄÒÅÓ ïõ */
#define addr_trmnl      	as_bits_5.bit1511 
/* ðÏÄÁÄÒÅÓ */
#define subaddr				as_bits_5.bit0905
/* ôÅËÕÝÉÊ ÎÏÍÅÒ óä */
#define serial_num_word 	as_bits_5.bit0400 

/* óÔÒÕËÔÕÒÁ ÏÔ×ÅÔÎÏÇÏ ÓÌÏ×Á */
#ifdef MY_DRIVER_BIG_ENDIAN
typedef union mmr_answer_word
{
	u_short		  wr_mmr_answer_word;
	struct
		{
			u_short  bit1511  : 5;  /* áÄÒÅÓ ïõ */
			u_short  bit10 	  : 1;  /* ïÛÉÂËÁ × ÓÏÏÂÝÅÎÉÉ */
			u_short  bit09    : 1;  /* ðÅÒÅÄÁÞÁ ïó */
			u_short  bit08    : 1;  /* úÁÐÒÏÓ ÎÁ ÏÂÓÌÕÖÉ×ÁÎÉÅ */
			u_short  bit0705  : 3;  /* ÒÅÚÅÒ× */
			u_short  bit04    : 1;  /* ðÒÉÎÑÔÁ ÇÒÕÐÐÏ×ÁÑ ËÏÍÁÎÄÁ */
			u_short  bit03 	  : 1;  /* áÂÏÎÅÎÔ ÚÁÎÑÔ */
			u_short  bit02    : 1;  /* îÅÉÓÐÒÁ×ÎÏÓÔØ ÁÂÏÎÅÎÔÁ */
			u_short  bit01    : 1;  /* ðÒÉÎÑÔÏ ÕÐÒÁ×ÌÅÎÉÅ ÉÎÔÅÒÆÅÊÓÏÍ */
			u_short  bit00    : 1;  /* îÅÉÓÐÒÁ×ÎÏÓÔØ ïõ */
		} as_bits_3;
} mmr_answer_word_t;
#else
typedef union mmr_answer_word
{
	u_short		  wr_mmr_answer_word;
	struct
		{
			u_short  bit00    : 1;  /* îÅÉÓÐÒÁ×ÎÏÓÔØ ïõ */
			u_short  bit01    : 1;  /* ðÒÉÎÑÔÏ ÕÐÒÁ×ÌÅÎÉÅ ÉÎÔÅÒÆÅÊÓÏÍ */
			u_short  bit02    : 1;  /* îÅÉÓÐÒÁ×ÎÏÓÔØ ÁÂÏÎÅÎÔÁ */
			u_short  bit03 	  : 1;  /* áÂÏÎÅÎÔ ÚÁÎÑÔ */
			u_short  bit04    : 1;  /* ðÒÉÎÑÔÁ ÇÒÕÐÐÏ×ÁÑ ËÏÍÁÎÄÁ */
			u_short  bit0705  : 3;  /* ÒÅÚÅÒ× */
			u_short  bit08    : 1;  /* úÁÐÒÏÓ ÎÁ ÏÂÓÌÕÖÉ×ÁÎÉÅ */	
			u_short  bit09    : 1;  /* ðÅÒÅÄÁÞÁ ïó */
			u_short  bit10 	  : 1;  /* ïÛÉÂËÁ × ÓÏÏÂÝÅÎÉÉ */
			u_short  bit1511  : 5;  /* áÄÒÅÓ ïõ */
		} as_bits_3;
} mmr_answer_word_t;
#endif /* MY_DRIVER_BIG_ENDIAN */

/* áÄÒÅÓ ïõ */
#define addr_terminal		as_bits_3.bit1511
/* ïÛÉÂËÁ × ÓÏÏÂÝÅÎÉÉ */
#define err_msg				as_bits_3.bit10
/* ðÅÒÅÄÁÞÁ ïó */
#define trans_AW			as_bits_3.bit09
/* úÁÐÒÏÓ ÎÁ ÏÂÓÌÕÖÉ×ÁÎÉÅ */
#define demand_upkeep		as_bits_3.bit08
/* ðÒÉÎÑÔÁ ÇÒÕÐÐÏ×ÁÑ ËÏÍÁÎÄÁ */
#define generic_comm_accept	as_bits_3.bit04
/* áÂÏÎÅÎÔ ÚÁÎÑÔ */
#define abonent_buzy		as_bits_3.bit03
/* îÅÉÓÐÒÁ×ÎÏÓÔØ ÁÂÏÎÅÎÔÁ */
#define abonent_faulty		as_bits_3.bit02
/* ðÒÉÎÑÔÏ ÕÐÒÁ×ÌÅÎÉÅ ÉÎÔÅÒÆÅÊÓÏÍ */
#define ctrl_interfice		as_bits_3.bit01
/* îÅÉÓÐÒÁ×ÎÏÓÔØ ïõ */
#define terminal_faulty		as_bits_3.bit00

/* óÔÒÕËÔÕÒÁ ÒÅÇÉÓÔÒÁ ÷óë */
#ifdef MY_DRIVER_BIG_ENDIAN
typedef union mmr_reg_word_mntr
{
	u_short		  wr_mmr_reg_word_mntr;
	struct
		{
			u_short  bit15  : 1;
			u_short  bit14  : 1;
			u_short  bit13  : 1;
			u_short  bit12  : 1;
			u_short  bit11  : 1;
			u_short  bit10 	: 1;
			u_short  bit09  : 1;
			u_short  bit08  : 1;
			u_short  bit07  : 1;
			u_short  bit06  : 1;
			u_short  bit05  : 1;
			u_short  bit04  : 1;
			u_short  bit03 	: 1;
			u_short  bit02  : 1;
			u_short  bit01  : 1;
			u_short  bit00  : 1;
		} as_bits_4;
} mmr_reg_word_mntr_t;
#else
typedef union mmr_reg_word_mntr
{
	u_short		  wr_mmr_reg_word_mntr;
	struct
		{
			u_short  bit00  : 1;
			u_short  bit01  : 1;
			u_short  bit02  : 1;
			u_short  bit03  : 1;
			u_short  bit04  : 1;
			u_short  bit05  : 1;
			u_short  bit06 	: 1;
			u_short  bit07  : 1;
			u_short  bit08  : 1;
			u_short  bit09  : 1;
			u_short  bit10  : 1;
			u_short  bit11  : 1;
			u_short  bit12  : 1;
			u_short  bit13 	: 1;
			u_short  bit14  : 1;
			u_short  bit15  : 1;
		} as_bits_4;
} mmr_reg_word_mntr_t;
#endif /* MY_DRIVER_BIG_ENDIAN */

/* ÷ÙÐÏÌÎÅÎÁ ËÏÍÁÎÄÁ ÕÐÒÁ×ÌÅÎÉÑ 
#define command_fulfill			as_bits_4.bit15 */
/* ëÏÍÁÎÄÁ ÎÅ ×ÙÐÏÌÎÅÎÁ ÚÁ 800 ÍËÓ */
#define command_not_carr		as_bits_4.bit14 
/* õðäí ÎÅ ÍÏÖÅÔ ×Ï×ÒÅÍÑ ÏÂÓÌÕÖÉÔØ ÚÁÐÒÏÓ */
#define serve_not_demand		as_bits_4.bit13
/* ëÏÍÁÎÄÁ ×ÙÐÏÌÎÅÎÁ 
#define command_carr_out 		as_bits_4.bit12 */
/* ïËÏÎÅÞÎÏÅ ÕÓÔÒÏÊÓÔ×Ï Ë ÍÏÍÅÎÔÕ ×ÙÄÁÞÉ ÏÔ×ÅÔÎÏÇÏ ÓÌÏ×Á */
/* ÎÅ ÐÏÌÕÞÉÌÏ ÏÔ×ÅÔ ÉÚ õðäí */
#define term_output_reciv_word  as_bits_4.bit11
/* ðÒÉÅÍ - ÐÅÒÅÄÁÞÁ */
#define reciv_trans				as_bits_4.bit10
/* ïÛÉÂËÁ ÞÅÔÎÏÓÔÉ */
#define error_parity			as_bits_4.bit09
/* äÌÉÔÅÌØÎÏÓÔØ ÉÍÐÕÌØÓÁ ÂÉÔÁ ÉÎÆÏÒÍÁÃÉÉ ÎÅ ÓÏÏÔ×ÅÔÓÔ×ÕÅÔ çïóô */
#define durat_impulse_not		as_bits_4.bit08
/* äÌÉÔÅÌØÎÏÓÔØ ×ÔÏÒÏÊ ÐÏÌÏ×ÉÎÙ ÓÉÎÈÒÏÉÍÐÕÌØÓÁ ÎÅ ÓÏÏÔ×ÅÔÓÔ×ÕÅÔ çïóô */
#define durat_second_not		as_bits_4.bit07
/* äÌÉÔÅÌØÎÏÓÔØ ÐÅÒ×ÏÊ ÐÏÌÏ×ÉÎÙ ÓÉÎÈÒÏÉÍÐÕÌØÓÁ ÎÅ ÓÏÏÔ×ÅÔÓÔ×ÕÅÔ çïóô */
#define durat_first_not			as_bits_4.bit06
/* îÏÍÅÒ ÁËÔÉ×ÎÏÇÏ ËÁÎÁÌÁ 
#define num_active_chnl			as_bits_4.bit05 */
/* ïËÏÎÅÞÎÏÅ ÕÓÔÒÏÊÓÔ×Ï ÐÒÉÎÑÌÏ ÎÅÄÏÐÕÓÔÉÍÕÀ ËÏÍÁÎÄÕ ÕÐÒÁ×ÌÅÎÉÑ */
#define accept_inadm_comm		as_bits_4.bit04
/* úÁÎÑÔ ÒÅÇÉÓÔÒ ÄÁÎÎÙÈ Ë ÍÏÍÅÎÔÕ ÐÒÉÅÍÁ óä ÉÚ íëéï */
#define reg_datas_occup			as_bits_4.bit03
/* ðÕÓÔ ÒÅÇÉÓÔÒ ÄÁÎÎÙÈ Ë ÍÏÍÅÎÔÕ ×ÙÄÁÞÉ óä × íëéï */
#define reg_datas_empty			as_bits_4.bit02
/* ëÏÎÔÒÏÌÌÅÒ ÐÒÉÎÑÌ ÏÔ×ÅÔÎÏÅ ÓÌÏ×Ï Ó ÎÅ×ÅÒÎÙÍ ÁÄÒÅÓÏÍ */
/* ÏËÏÎÅÞÎÏÇÏ ÕÓÔÒÏÊÓÔ×Á */
#define accept_incorr_addr		as_bits_4.bit01
/* ëÏÎÔÒÏÌÌÅÒ ÎÅ ÐÒÉÎÑÌ ÏÔ×ÅÔÎÏÅ ÓÌÏ×Ï. */
/* ïËÏÎÅÞÎÏÅ ÕÓÔÒÏÊÓÔ×Ï ×Ï×ÒÅÍÑ ÎÅ ÐÅÒÅÄÁÌÏ ÏÔ×ÅÔÎÏÅ ÓÌÏ×Ï. */
#define  not_recip_word			as_bits_4.bit00

#ifndef MMR_OLD_VERSION
/* óÔÒÕËÔÕÒÁ ÒÅÇÉÓÔÒÁ ÏÂÝÅÇÏ ÐÒÉ ÐÏÌÕÞÅÎÉÉ ÐÒÅÒÙ×ÁÎÉÑ */
#ifdef MY_DRIVER_BIG_ENDIAN
typedef union mmr_reg_common
{
	u_int          wr_mmr_reg_common;
	struct
		{
			u_int  bit3128 : 4; /* ÕËÁÚÁÔÅÌØ ÎÁ ÎÁ ÓÕÂÂÌÏË, Ó */
								 /* ËÏÔÏÒÙÍ ÂÙÌ ÏÂÍÅÎ ÄÌÑ âä */
			u_int  bit2724 : 4; /* ÕËÁÚÁÔÅÌØ ÎÁ ÎÁ ÓÕÂÂÌÏË, Ó */
								 /* ËÏÔÏÒÙÍ ÂÙÌ ÏÂÍÅÎ ÄÌÑ âë */
			u_int  bit2319 : 5; /* ÕËÁÚÁÔÅÌØ ÎÁ ÎÁ ÂÌÏË, Ó */
								 /* ËÏÔÏÒÙÍ ÂÙÌ ÏÂÍÅÎ ÄÌÑ âä */
			u_int  bit1814	: 5; /* ÕËÁÚÁÔÅÌØ ÎÁ ÎÁ ÂÌÏË, Ó */
								 /* ËÏÔÏÒÙÍ ÂÙÌ ÏÂÍÅÎ ÄÌÑ âë */
			u_int  bit13	: 1; /* ËÏÌ-×Ï ÚÁÐÏÌÎÅÎÎÙÈ ÂÌÏËÏ× 1 - 1 ÂÌÏË */
								 /* 0 - 2 ÂÌÏËÁ */
			u_int  bit1206 : 7; /* ÎÏÍÅÒ ×ÅÒÓÉÉ ÁÄÁÐÔÅÒÁ ÉÌÉ ÂÕÆÅÒÁ ÏÂÍÅÎÁ */
			u_int  bit0500 : 6; /* ÎÅ ÉÓÐÏÌØÚÕÅÔÓÑ */
		} as_bits_5;
} mmr_reg_common_t;
#else
typedef union mmr_reg_common
{
	u_int          wr_mmr_reg_common;
	struct
		{
			u_int  bit0500 : 6; /* ÎÅ ÉÓÐÏÌØÚÕÅÔÓÑ */
			u_int  bit1206 : 7; /* ÎÏÍÅÒ ×ÅÒÓÉÉ ÁÄÁÐÔÅÒÁ ÉÌÉ ÂÕÆÅÒÁ ÏÂÍÅÎÁ */
			u_int  bit13   : 1; /* ËÏÌ-×Ï ÚÁÐÏÌÎÅÎÎÙÈ ÂÌÏËÏ× 1 - 1 ÂÌÏË, 0 - 2 ÂÌÏËÁ */
			u_int  bit1814 : 5; /* ÕËÁÚÁÔÅÌØ ÎÁ ÂÌÏË, Ó ËÏÔÏÒÙÍ ÂÙÌ ÏÂÍÅÎ ÄÌÑ âë */
			u_int  bit2319 : 5; /* ÕËÁÚÁÔÅÌØ ÎÁ ÂÌÏË, Ó ËÏÔÏÒÙÍ ÂÙÌ ÏÂÍÅÎ ÄÌÑ âä */
			u_int  bit2724 : 4; /* ÕËÁÚÁÔÅÌØ ÎÁ ÓÕÂÂÌÏË, Ó ËÏÔÏÒÙÍ ÂÙÌ ÏÂÍÅÎ ÄÌÑ âë */
			u_int  bit3128 : 4; /* ÕËÁÚÁÔÅÌØ ÎÁ ÓÕÂÂÌÏË, Ó ËÏÔÏÒÙÍ ÂÙÌ ÏÂÍÅÎ ÄÌÑ âä */
		} as_bits_5;
} mmr_reg_common_t;
#endif /* MY_DRIVER_BIG_ENDIAN */

/* õËÁÚÁÔÅÌØ ÎÁ ÎÁ ÓÕÂÂÌÏË, Ó ËÏÔÏÒÙÍ ÂÙÌ ÏÂÍÅÎ ÄÌÑ âä */
#define pointer_subblock_exch  	as_bits_5.bit3128 
/* õËÁÚÁÔÅÌØ ÎÁ ÎÁ ÓÕÂÂÌÏË, Ó ËÏÔÏÒÙÍ ÂÙÌ ÏÂÍÅÎ ÄÌÑ âë */
#define pointer_subblock_comm  	as_bits_5.bit2724
/* õËÁÚÁÔÅÌØ ÎÁ ÎÁ ÓÕÂÂÌÏË, Ó ËÏÔÏÒÙÍ ÂÙÌ ÏÂÍÅÎ ÄÌÑ âä */
#define pointer_block_exch		as_bits_5.bit2319
/* õËÁÚÁÔÅÌØ ÎÁ ÎÁ ÓÕÂÂÌÏË, Ó ËÏÔÏÒÙÍ ÂÙÌ ÏÂÍÅÎ ÄÌÑ âë */
#define pointer_block_comm		as_bits_5.bit1814
/* ëÏÌ-×Ï ÚÁÐÏÌÎÅÎÎÙÈ ÂÌÏËÏ× */
#define num_full_block			as_bits_5.bit13
/* îÏÍÅÒ ×ÅÒÓÉÉ ÁÄÁÐÔÅÒÁ */
#define num_version_or_buf		as_bits_5.bit1206
#endif /* MMR_OLD_VERSION */

/* þÔÅÎÉÑ/ÚÁÐÉÓØ ÓÏÄÅÒÖÉÍÏÇÏ ÒÅÇÉÓÔÒÁ íMR */
typedef struct arg_reg {
	int		reg_addr;	/* ÁÄÒÅÓ ÒÅÇÉÓÔÒÁ */
	u_int		reg_value;	/* ×ÏÚ×ÒÁÝÁÅÍÁÑ/ÐÅÒÅÄÁ×ÁÅÍÁÑ ×ÅÌÉÞÉÎÁ */
} mmr_arg_reg_t;

/* óÔÒÕËÔÕÒÁ ÚÁÇÏÌÏ×ËÁ ÂÕÆÅÒÁ ÏÂÍÅÎÁ */
typedef struct mmr_iosubd_desc {
	u_char	cur_num_subarray;  /* ÔÅËÕÝÉÊ ÎÏÍÅÒ ÐÏÄÍÁÓÓÉ×Á */
	u_char	next_num_subarray; /* ÓÌÅÄÕÀÝÉÊ ÎÏÍÅÒ ÐÏÄÍÁÓÓÉ×Á */
	u_short	unused1;		   /* ÎÅ ÉÓÐÏÌØÚÕÅÔÓÑ */
/* íÁÓÓÉ× ÓÞÅÔÞÉËÏ× ËÏÌ-×Á ÓÌÏ× (íðëï) × ÐÏÄÍÁÓÓÉ×Å âä */
	u_char	amount_words[8];
	u_short	cur_addr_subarray_del;   /* ÔÅËÕÝÉÊ ÁÄÒÅÓ ÐÏÄÍÁÓÓÉ×Á */
	u_short	next_addr_subarray_del;  /* ÓÌÅÄÕÀÝÉÊ ÁÄÒÅÓ ÐÏÄÍÁÓÓÉ×Á */
	short	buf_num;        /* ÎÏÍÅÒ ÂÕÆÅÒÁ ÏÂÍÅÎÁ */
	u_short io_flags; 		/* ËÏÄ ÏÐÅÒÁÃÉÉ ÏÂÍÅÎÁ */
	u_short data_size; 		/* ÍÁËÓÉÍÁÌØÎÁÑ ÄÌÉÎÁ × ÂÁÊÔÁÈ ÍÁÓÓÉ×Á ÏÂÍÅÎÁ */
	short	unused5; 		/* ÎÅ ÉÓÐÏÌØÚÕÅÔÓÑ */
	int	unused_word6; 	/* ÎÅ ÉÓÐÏÌØÚÕÅÔÓÑ */
	int	unused_word7; 	/* ÎÅ ÉÓÐÏÌØÚÕÅÔÓÑ */
} mmr_iosubd_desc_t;

/*	óÔÒÕËÔÕÒÁ ÂÕÆÅÒÁ ÄÁÎÎÙÈ ÄÌÑ ÏÂÍÅÎÁ.
	âÕÆÅÒ ÄÁÎÎÙÈ ÐÒÅÄÓÔÁ×ÌÑÅÔ ÓÏÂÏÊ ÏÂÙÞÎÙÊ ÍÁÓÓÉ× É × ÄÁÎÎÏÊ ÓÔÒÕËÔÕÒÅ ÏÎ
	ÐÒÅÄÓÔÁ×ÌÅÎ × ×ÉÄÅ ÏÂßÅÄÉÎÅÎÉÑ ÍÁÓÓÉ×Ï× ÒÁÚÎÙÈ ÆÏÒÍÁÔÏ×.
	òÁÚÍÅÒ ÂÕÆÅÒÁ ÐÒÉ ÎÅÏÂÈÏÄÉÍÏÓÔÉ ËÏÒÒÅËÔÉÒÕÅÔÓÑ ÄÌÑ ÏÂÅÓÐÅÞÅÎÉÑ
	ËÒÁÔÎÏÓÔÉ ÂÌÏËÕ ÏÂÍÅÎÁ
*/
typedef union mmr_data_buf_ {
	u_short
								/* ÍÁÓÓÉ× ÓÌÏ× ÄÁÎÎÙÈ ÏÂÍÅÎÁ */
								/* ÓÌÏ×Ï × ÓÍÙÓÌÅ ËÁÎÁÌÁ MMR */
								/* (2 ÂÁÊÔÁ, 16 ÂÉÔ) */
		words[TU_MMR_DMA_BURST_SIZE_ALIGN(MMR_MAX_DATA_BUF_SIZE /
				sizeof(u_short),
			sizeof(u_short), 0, MMR_DMA_BURST_SIZE)];
	u_int
								/* ÍÁÓÓÉ× ÓÌÏ× ÏÓÎÏ×ÎÏÊ */
								/* ÐÁÍÑÔÉ (4 ÂÁÊÔÁ 32 ÂÉÔ) */
		longs[TU_MMR_DMA_BURST_SIZE_ALIGN(MMR_MAX_DATA_BUF_SIZE /
			sizeof(u_int), sizeof(u_int), 0,
			MMR_DMA_BURST_SIZE)];
	u_char
								/* ÍÁÓÓÉ× ÂÁÊÔÏ× */
		bytes[TU_MMR_DMA_BURST_SIZE_ALIGN(MMR_MAX_DATA_BUF_SIZE,
			sizeof(u_char), 0, MMR_DMA_BURST_SIZE)];
} mmr_data_buf_t;

/*  ïÐÉÓÁÎÉÑ ÓÔÒÕËÔÕÒÙ ÂÕÆÅÒÁ ÏÂÍÅÎÁ.
	äÒÁÊ×ÅÒ ÓÏÚÄÁÅÔ É ÉÎÉÃÉÁÌÉÚÉÒÕÅÔ ÏÂÝÉÅ ÂÕÆÅÒÁ ËÁË ËÏÎËÒÅÔÎÙÅ
	ÜËÚÅÍÐÌÑÒÙ ÉÍÅÎÎÏ ÄÁÎÎÏÊ ÓÔÒÕËÔÕÒÙ. ðÒÉ ÜÔÏÍ × ÚÁÇÏÌÏ×ËÅ ÂÕÆÅÒÁ
	ÉÎÉÃÉÁÌÉÚÉÒÏ×ÁÎÙ ×ÓÅ ÐÏÌÑ Ó ÆÉËÓÉÒÏ×ÁÎÎÙÍÉ É
	ÐÏÓÔÏÑÎÎÙÍÉ ÚÎÁÞÅÎÉÑÍÉ.
*/
typedef struct mmr_iosubdbuf {
	mmr_iosubd_desc_t       buf_desc;  /* ÄÅÓËÒÉÐÔÏÒ ÂÕÆÅÒÁ É ÒÅÚÕÌØÔÁÔÏ× */
	mmr_data_buf_t  	data_buf;  /* ÏÂÌÁÓÔØ ÂÕÆÅÒÁ ÄÁÎÎÙÈ ÄÌÑ ÐÅÒÅÄÁÞÉ */
} mmr_iosubdbuf_t;

/*  ïÐÉÓÁÎÉÑ ÓÔÒÕËÔÕÒÙ ÜÌÅÍÅÎÔÁ ËÁÒÔÙ ÏÂÍÅÎÏ×.
	äÁÎÎÁÑ ÓÔÒÕËÔÕÒÁ Ñ×ÌÑÅÔÓÑ ×ÎÕÔÒÅÎÎÉÍ ÐÒÅÄÓÔÁ×ÌÅÎÉÅÍ ËÁÒÔÙ ÏÂÍÅÎÏ×
	É ÎÁ ÐÏÌØÚÏ×ÁÔÅÌÑ ÎÅÐÏÓÒÅÄÓÔ×ÅÎÎÏ ÎÅ ×ÙÈÏÄÉÔ.
*/
typedef struct mmr_iomap_subd {
	mmr_iosubdbuf_t	write;  /* ÂÕÆÅÒ ÐÅÒÅÄÁÔÞÉËÁ */
	mmr_iosubdbuf_t	read;	/* ÂÕÆÅÒ ÐÒÉÅÍÎÉËÁ */

} mmr_iomap_subd_t;

/*  ïÐÉÓÁÎÉÑ ËÁÒÔÙ ÏÂÍÅÎÏ× - ÍÁÓÓÉ× ÂÕÆÅÒÏ× ×ÓÅÈ ÐÏÄÕÓÔÒÏÊÓÔ×.
	äÁÎÎÁÑ ÓÔÒÕËÔÕÒÁ Ñ×ÌÑÅÔÓÑ ×ÎÕÔÒÅÎÎÉÍ ÐÒÅÄÓÔÁ×ÌÅÎÉÅÍ ËÁÒÔÙ ÏÂÍÅÎÏ×
	ÏËÏÎÅÞÎÉËÁ É ÎÁ ÐÏÌØÚÏ×ÁÔÅÌÑ ÎÅÐÏÓÒÅÄÓÔ×ÅÎÎÏ ÎÅ ×ÙÈÏÄÉÔ.
*/
typedef mmr_iomap_subd_t		mmr_iomap_t;

/* ïÐÉÓÁÎÉÅ ÓÔÒÕËÔÕÒÙ ÐÁÒÁÍÅÔÒÏ× ÉÎÉÃÉÁÌÉÚÁÃÉÉ ÂÕÆÅÒÏ× ÏÂÍÅÎÁ ÄÁÎÎÙÍÉ */
typedef struct mmr_init_iomap {
	u_short		buf_num;		/* ÞÉÓÌÏ ÂÕÆÅÒÏ× ÏÂÍÅÎÁ ÄÁÎÎÙÍÉ */
	u_short		max_data_buf_trans_size;
					/* ÍÁËÓÉÍÁÌØÎÙÊ ÒÁÚÍÅÒ ÐÅÒÅÄÁÀÝÅÇÏ ÂÕÆÅÒÁ ÄÁÎÎÙÈ */
	u_short		max_data_buf_reciv_size;
					/* ÍÁËÓÉÍÁÌØÎÙÊ ÒÁÚÍÅÒ ÐÒÉÅÍÎÏÇÏ ÂÕÆÅÒÁ ÄÁÎÎÙÈ */
	size_t		*real_buf_size_p;
								/* ÕËÁÚÁÔÅÌØ ÎÁ ÐÅÒÅÍÅÎÎÕÀ */
								/* ÄÌÑ ÚÁÐÉÓÉ ÒÅÁÌØÎÏÇÏ */
								/* ÒÁÚÍÅÒÁ ÂÕÆÅÒÁ ËÁÒÔÙ */
								/* ÏÂÍÅÎÏ× c ÕÞÅÔÏÍ */
								/* ÎÅÏÂÈÏÄÉÍÙÈ ËÒÁÔÎÏÓÔÅÊ */
								/* ÁÄÒÅÓÏ× É ÒÁÚÍÅÒÏ×*/
	int		*error_code_p;				/* ÕËÁÚÁÔÅÌØ ÎÁ ÐÅÒÅÍÅÎÎÕÀ */
								/* ÄÌÑ ÚÁÐÉÓÉ ËÏÄÁ ÏÛÉÂËÉ, */
								/* ÅÓÌÉ ÔÁËÏ×ÁÑ ÂÕÄÅÔ */
								/* ÏÂÎÁÒÕÖÅÎÁ × ÐÒÏÃÅÓÓÅ */
								/* ÉÎÉÃÉÁÌÉÚÁÃÉÉ */
	int		flag_board;				/* ÐÒÉÚÎÁË ÕÓÔÒÏÊÓÔ×Á: */
								/* ËÏÎÔÒÏÌÌÅÒ ÉÌÉ ïõ */
} mmr_init_iomap_t;


/* ïÐÉÓÁÎÉÅ ÓÔÒÕËÔÕÒÙ ÐÁÒÁÍÅÔÒÏ× ÏÓÔÁÎÏ×Á ÏÂÍÅÎÏ× É ÚÁËÒÙÔÉÑ ËÁÎÁÌÁ */
#ifdef MMR_OLD_VERSION
typedef struct mmr_halt_trans {
	int		waiting_time;		/* ×ÒÅÍÑ ÏÖÉÄÁÎÉÑ ÚÁ×ÅÒÛÅÎÉÑ */
								/* ÐÏÓÌÅÄÎÅÇÏ ÏÂÍÅÎÁ É */
								/* ÚÁËÒÙÔÉÑ ËÁÎÁÌÁ, ÐÏÓÌÅ */
								/* ËÏÔÏÒÏÇÏ ×ÓÅ ÂÕÆÅÒÁ */
								/* ÏÂÍÅÎÏ× ÕÄÁÌÑÀÔÓÑ */
								/* ÅÓÌÉ 0, ÔÏ ÏÖÉÄÁÎÉÑ ÎÅÔ */
								/* ÂÕÆÅÒÁ ÂÕÄÕÔ ÕÄÁÌÅÎÙ ÐÒÉ */
								/* ÓÌÅÄÕÀÝÅÊ ÉÎÉÃÉÁÌÉÚÁÃÉÉ */
								/* ÉÌÉ ÐÒÉ ÚÁËÒÙÔÉÉ ÕÓÔ-×Á */
								/* ( close() ) */
								/* ÅÓÌÉ < 0, ÔÏ ÏÖÉÄÁÔØ */
								/* ÓÌÅÄÕÅÔ ÎÅ ÂÏÌÅÅ ×ÒÅÍÅÎÉ */
								/* ÚÁÄÁÎÎÏÇÏ ÐÒÉ ÏÔËÒÙÔÉÉ */
								/* × ËÁÞÅÓÔ×Å ÚÎÁÞÅÎÉÑ */
								/* ÔÁÊÍÅÒÁ ÄÌÑ ËÏÎÔÒÏÌÑ */
								/* ÚÁ×ÉÓÁÎÉÑ ÏÂÍÅÎÏ× */
								/* ÅÓÌÉ > 0, ÔÏ ÜÔÏ ×ÒÅÍÑ × */
								/* ÍÉËÒÏÓÅËÕÎÄÁÈ, ÐÏÓÌÅ */
								/* ËÏÔÏÒÏÇÏ ×ÓÅ ÂÕÆÅÒÁ  */
								/* ÏÂÍÅÎÏ× ÕÄÁÌÑÀÔÓÑ */
} mmr_halt_trans_t;
#else
typedef struct mmr_halt_trans {
	int		waiting_time;		/* ×ÒÅÍÑ ÏÖÉÄÁÎÉÑ ÚÁ×ÅÒÛÅÎÉÑ */
								/* ÐÏÓÌÅÄÎÅÇÏ ÏÂÍÅÎÁ É */
								/* ÚÁËÒÙÔÉÑ ËÁÎÁÌÁ, ÐÏÓÌÅ */
								/* ËÏÔÏÒÏÇÏ ×ÓÅ ÂÕÆÅÒÁ */
								/* ÏÂÍÅÎÏ× ÕÄÁÌÑÀÔÓÑ */
	hrtime_t	max_time_waiting; /* ÍÁËÓ. ô ÏÖÉÄÁÎÉÑ ×ÙÐÏÌÎÅÎÉÑ ËÏÍÁÎÄÙ */
	hrtime_t	med_time_waiting; /* ÓÒÅÄÎÅÅ ô ÏÖÉÄÁÎÉÑ ×ÙÐÏÌÎÅÎÉÑ ËÏÍÁÎÄÙ */
	hrtime_t	min_time_waiting; /* ÍÉÎ. ô ÏÖÉÄÁÎÉÑ ×ÙÐÏÌÎÅÎÉÑ ËÏÍÁÎÄÙ */
	u_int       	allocation_time_intr[GISTOGR];
	hrtime_t	max_time_adapter; /* ÍÁËÓ. ô ×ÙÐÏÌÎÅÎÉÑ ËÏÍÁÎÄÙ ÁÄÁÐÔÅÒÏÍ */
	hrtime_t	med_time_adapter; /* ÓÒÅÄÎÅÅ ô ×ÙÐÏÌÎÅÎÉÑ ËÏÍÁÎÄÙ ÁÄÁÐÔÅÒÏÍ */
	hrtime_t	min_time_adapter; /* ÍÉÎ. ô ×ÙÐÏÌÎÅÎÉÑ ËÏÍÁÎÄÙ ÁÄÁÐÔÅÒÏÍ */
	u_int       	allocation_time_adapter[GISTOGR];
	u_int		max_comm; /* ÍÁËÓ. ËÏÌ-×Ï ÎÅ ÏÂÓÌÕÖÅÎÎÙÈ ËÏÍÁÎÄ */
	u_int      	allocation_comm[GISTOGR_COMM];
} mmr_halt_trans_t;
#endif /* MMR_OLD_VERSION */
/*  óÔÒÕËÔÕÒÁ, ÏÐÉÓÙ×ÁÀÝÁÑ ÉÎÆÏÒÍÁÃÉÀ Ï Ó×ÑÚÉ ÕÓÔÒÏÊÓÔ×Á Ó ÏÔËÒÙÔÙÍ
	ÄÅÓËÒÉÐÔÏÒÏÍ ÆÁÊÌÁ.
	ðÏÄÁÅÔÓÑ ÄÌÑ ÚÁÐÏÌÎÅÎÉÑ × ÓÏÏÔ×ÅÔÓÔ×ÕÀÝÕÀ ËÏÍÁÎÄÕ, ÒÅÁÌÉÚÏ×ÁÎÎÕÀ ÞÅÒÅÚ
	ioctl() ×ÙÚÏ×
*/
typedef	struct mmr_dev_info {
	int			instance;	/* ÜËÚÅÍÐÌÑÒ MMR */
	int			channel;	/* ÎÏÍÅÒ ËÁÎÁÌÁ */
} mmr_dev_info_t;

#ifndef MMR_OLD_VERSION
typedef struct mmr_drv_info
{
	int			sbus_clock_freq;	/* ÞÁÓÔÏÔÁ ÓÉÎÈÒÏÎÉÚÁÃÉÉ SBus */
	int			sbus_nsec_cycle;	/* ÐÅÒÉÏÄ ÓÌÅÄÏ×ÁÎÉÑ tick-Ï× SBus */
	int			mp_clock_freq;		/* ÞÁÓÔÏÔÁ ÓÉÎÈÒÏÎÉÚÁÃÉÉ SBus */
							/* ÍÉËÒÏÐÒÏÃÅÓÓÏÒÁ */
	int			mp_nsec_cycle;		/* ÐÅÒÉÏÄ ÓÌÅÄÏ×ÁÎÉÑ tick-Ï× íð */
	hrtime_t    		cur_hr_time;		/* ÔÅËÕÝÅÅ ×ÒÅÍÑ × ÎÓÅË */
}	mmr_drv_info_t;
#endif /* MMR_OLD_VERSION */

/* óÔÒÕËÔÕÒÁ, ÏÐÉÓÙ×ÁÀÝÁÑ ÉÎÆÏÒÍÁÃÉÀ ÄÌÑ ËÏÍÁÎÄÙ MMRIO_INTR_TIME_WAIT */
#ifdef MMR_OLD_VERSION 
typedef struct mmr_intr_wait {
	u_long	intr_wait_time;			/* ×ÒÅÍÑ ÏÖÉÄÁÎÉÑ ÐÒÅÒÙ×ÁÎÉÑ (ÍËÓÅË) */
	u_long	event_intr;			/* ËÏÄ ÓÏÂÙÔÉÑ */
	u_long	board_error;		 	/* ×ÎÕÔÒÅÎÎÑÑ ÏÛÉÂËÁ ÐÌÁÔÙ */
	u_long	pointer_reciv_comm;		/* ÕËÁÚÁÔÅÌØ ÎÁ ÚÁÐÉÓÁÎÎÕÀ ËÏÍÁÎÄÕ */
						/* × ÂÕÆÅÒÅ ËÏÍÁÎÄ æð */
	u_long	cur_num_comm;			/* ËÏÌ-×Ï ÚÁÐÉÓÁÎÎÙÈ ËÏÍÁÎÄ */
} mmr_intr_wait_t;
#else
typedef struct mmr_intr_wait {
	u_long		intr_wait_time;			/* ×ÒÅÍÑ ÏÖÉÄÁÎÉÑ ÐÒÅÒÙ×ÁÎÉÑ (ÍËÓÅË) */
	u_long		event_intr;			/* ËÏÄ ÓÏÂÙÔÉÑ */
	u_long		board_error;		 	/* ×ÎÕÔÒÅÎÎÑÑ ÏÛÉÂËÁ ÐÌÁÔÙ */
	u_int		num_reciv_comm;			/* ËÏÌ-×Ï ÚÁÐÉÓÁÎÎÙÈ ËÏÍÁÎÄ × ÂÕÆÅÒ */
							/* ËÏÍÁÎÄ æð ÐÏ ÉÎÆ. ÁÄÁÐÔÅÒÁ */
	u_int		cur_num_comm;			/* ËÏÌ-×Ï ÚÁÐÉÓÁÎÎÙÈ ËÏÍÁÎÄ */
	u_int		intr_device; 			/* òïâ ÐÒÉ ÐÏÌÕÞÅÎÉÉ ÐÒÅÒÙ×ÁÎÉÑ ÏÔ ÁÄÁÐÔÅÒÁ */
	hrtime_t	time_get_intr_device;		/* ô ÐÏÌÕÞÅÎÉÑ ÐÒÅÒÙ×ÁÎÉÑ ÏÔ ÁÄÁÐÔÅÒÁ ÎÓÅË */
	hrtime_t	time_get_comm; 			/* ô ÐÏÌÕÞÅÎÉÑ ÄÒÁÊ×ÅÒÏÍ ËÏÍÁÎÄÙ ÎÁ ×ÙÄÁÞÕ */
							/* ÐÒÅÒÙ×ÁÎÉÑ ÎÓÅË */
} mmr_intr_wait_t;

#endif /* MMR_OLD_VERSION */

#ifndef MMR_OLD_VERSION
/* ðÏÌÕÞÅÎÉÅ ÉÎÆÏÒÍÁÃÉÉ Ï ÐÒÅÒÙ×ÁÎÉÑÈ ÐÏ òïû */
typedef struct intr_rosh
{
	u_short		num_intr_rosh;			/* ËÏÌ-×Ï ÐÒÅÒÙ×ÁÎÉÊ ÐÏ òïû */
} mmr_intr_rosh_t;
#endif /* MMR_OLD_VERSION */

/* âÕÆÅÒÁ ÄÁÎÎÙÈ ÁÄÁÐÔÅÒÁ */
typedef struct buf_data {
	u_int	area_subbuf0[8];
	u_int	area_subbuf1[8];
} buf_data_t;

/* õÐÒÁ×ÌÑÀÝÁÑ ÉÎÆÏÒÍÁÃÉÑ ÂÕÆÅÒÏ× ÁÄÁÐÔÅÒÁ */
typedef struct buf_args {
	u_int	USK; /* õóë */
	u_int	AC0; /* áó0 */
	u_int	SKB; /* óëâ */
	u_int	AC1; /* áó1 */
} buf_args_t;

/* ó×ÑÚØ ÐÒÏÇÒÁÍÍÙ ÔÅÓÔÁ É âïúõ */
typedef struct mmr_area_bozu {
	buf_data_t  /* éÎÆÏÒÍÁÃÉÑ ÂÕÆÅÒÏ× ÄÁÎÎÙÈ ÁÄÁÐÔÅÒÁ */
				buf_data[MMR_BUF_ADAPTER_NUM*2];
	buf_args_t  /* õÐÒÁ×ÌÑÀÝÁÑ ÉÎÆÏÒÍÁÃÉÑ ÂÕÆÅÒÏ× ÄÁÎÎÙÈ ÁÄÁÐÔÅÒÁ */
				init_buf_data[MMR_BUF_ADAPTER_NUM*2];
	buf_data_t  /* éÎÆÏÒÍÁÃÉÑ ÂÕÆÅÒÁ ËÏÍÁÎÄ ÁÄÁÐÔÅÒÁ */
				buf_comm;
	buf_args_t  /* õÐÒÁ×ÌÑÀÝÁÑ ÉÎÆÏÒÍÁÃÉÑ ÂÕÆÅÒÁ ËÏÍÁÎÄ ÁÄÁÐÔÅÒÁ */
				init_buf_comm;
} mmr_area_bozu_t;

#ifdef	__cplusplus
}
#endif

#endif	/* _UAPI_LINUX_MMR_IO_H__ */
