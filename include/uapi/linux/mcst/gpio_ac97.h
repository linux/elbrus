
/*
 * Copyright (c) 1997, by MCST.
 */

#ifndef	_GPIO_AC97_VAR_H
#define	_GPIO_AC97_VAR_H

/*
 * Definition of relationship between dev_t and interrupt numbers
 * instance, #intr, in/out  <=> minor
 */
#include <linux/ioctl.h>

#define LINES_NUM	8
#define LINES_MASK	((1 << LINES_NUM) - 1)

#define MAGIC_NUMBER 240

#define IOCTL_GPIO_GET_STATUS		_IO( MAGIC_NUMBER, 2)
#define IOCTL_GPIO_SET_STATUS		_IO( MAGIC_NUMBER, 3)
#define IOCTL_GPIO_WAIT_INTERRUPT	_IO( MAGIC_NUMBER, 4)

typedef struct gpio_status {
	unsigned gpio_ctrl;	// 00 set direction 0-3 -> 4-7 bit register
	unsigned gpio_data;	// 04 regiser data   0-7 bit
	unsigned gpio_int_cls;	// 08 int ctrl
	unsigned gpio_int_lvl;	// 0Ó
	unsigned gpio_int_en;	// 10 interrupt enable
	unsigned gpio_int_sts;
} gpio_status_t;

/*
gpio_cotrol: ÒÅÇÉÓÔÒ ÕÐÒÁ×ÌÅÎÉÑ
               1 - ÓÏÏÔ×ÅÔÓÔ×ÕÀÝÉÊ ×Ù×ÏÄ Ñ×ÌÑÅÔÓÑ ×ÙÈÏÄÏÍ, 0 - ×ÈÏÄÏÍ,
               ÏÔÎÏÓÉÔÅÌØÎÙÊ ÁÄÒÅÓ 0x0; ÒÅÇÉÓÔÒ RW

gpio_data:   ÒÅÇÉÓÔÒ ÄÁÎÎÙÈ; ÄÌÑ ×ÙÈÏÄÏ× ÓÏÄÅÒÖÉÔ ÄÁÎÎÙÅ, ËÏÔÏÒÙÅ ÎÁÄÏ          
               ÐÅÒÅÄÁÔØ, ÄÌÑ ×ÈÏÄÏ× - ÓÏÓÔÏÑÎÉÅ ÐÉÎÏ×;
               ÏÔÎÏÓÉÔÅÌØÎÙÊ ÁÄÒÅÓ 0x4; ÂÉÔÙ, ÓÏÏÔ×ÅÔÓÔ×ÕÀÝÉÅ ×ÙÈÏÄÁÍ RW,       
             ×ÈÏÄÁÍ - RO

gpio_int_cls: ÒÅÇÉÓÔÒ ÉÓÔÏÞÎÉËÁ ÐÒÅÒÙ×ÁÎÉÊ, 0 - ÕÒÏ×ÅÎØ, 1 - ÆÒÏÎÔ
               ÏÔÎÏÓÉÔÅÌØÎÙÊ ÁÄÒÅÓ 0x8; ÒÅÇÉÓÔÒ RW

gpio_int_lvl: 0 - ÐÒÅÒÙ×ÁÎÉÅ ÏÔ ÎÉÚËÏÇÏ ÕÒÏ×ÎÑ ÉÌÉ ÏÔÒÉÃÁÔÅÌØÎÏÇÏ ÆÒÏÎÔÁ
                1 - ÐÒÅÒÙ×ÁÎÉÅ ÏÔ ×ÙÓÏËÏÇÏ ÕÒÏ×ÎÑ ÉÌÉ ÐÏÌÏÖÉÔÅÌØÎÏÇÏ
                ÆÒÏÎÔÁ; ÏÔÎÏÓÉÔÅÌØÎÙÊ ÁÄÒÅÓ 0xó; ÒÅÇÉÓÔÒ RW

gpio_int_en: ÒÅÇÉÓÔÒ ÒÁÚÒÅÛÅÎÉÑ ÐÒÅÒÙ×ÁÎÉÊ
               0 - ÚÁÐÒÅÝÅÎÏ
               1 - ÒÁÚÒÅÛÅÎÏ
               ÏÔÎÏÓÉÔÅÌØÎÙÊ ÁÄÒÅÓ 0x10; ÒÅÇÉÓÔÒ RW

gpio_int_sts: ÒÅÇÉÓÔÒ ÓÔÁÔÕÓÁ ÐÒÅÒÙ×ÁÎÉÊ;
                ÏÔÎÏÓÉÔÅÌØÎÙÊ ÁÄÒÅÓ 0x14; ÒÅÇÉÓÔÒ RWC (ÓÂÒÁÓÙ×ÁÅÔÓÑ
                ÚÁÐÉÓØÀ ÅÄÉÎÉÃÙ)
*/

typedef struct wait_int {
	unsigned pin;		/*ÎÏÍÅÒ ÌÉÎÉÉ Ó ËÏÔÏÒÏÊ ÏÖÉÄÁÔØ ÐÒÅÒÙ×ÁÎÉÅ */
	unsigned timeout;	/*× ÍÉËÒÏÓÅËÕÎÄÁÈ */
	unsigned disable;	/*ÚÁÐÒÅÝÁÔØ ÌÉ ÐÒÅÒÙ×ÁÎÉÅ ÐÏ ÐÅÒ×ÏÍÕ ÓÒÁÂÁÔÙ×ÁÎÉÀ */
} wait_int_t;

#ifdef __KERNEL__

#define DEV_NAME		"gpio_as97"
#define GPIO_DSBL_INT		1
/*
 * driver state per instance
 */
typedef struct gpio_state {
	unsigned long start_io;	/* ÎÁÞÁÌÏ ÏÂÌÁÓÔÉ ÐÏÒÔÏ× ××ÏÄÁ/×Ù×ÏÄÁ */
	unsigned long end_io;	/* ËÏÎÅÃ ÏÂÌÁÓÔÉ ÐÏÒÔÏ× ××ÏÄÁ/×Ù×ÏÄÁ */
	unsigned long len_io;	/* ÄÌÉÎÁ ÏÂÌÁÓÔÉ ÐÏÒÔÏ× ××ÏÄÁ/×Ù×ÏÄÁ */
	struct semaphore mux;	/* open/close mutex     */
	wait_queue_head_t pollhead[LINES_NUM];
	unsigned line_st[LINES_NUM];
	unsigned int major;
	struct pci_dev *dev;	/* ÕËÁÚÁÔÅÌØ ÎÁ ÓÔÒÕËÔÕÒÕ Ó PCI-ÄÁÎÎÙÍÉ ÕÓÔÒÏÊÓÔ×Á */
	char revision_id;
} gpio_state_t;

#endif				/* __KERNEL__ */

#endif				/* _GPIO_AC97_VAR_H */
