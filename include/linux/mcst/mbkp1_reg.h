#ifndef __LINUX_MBKP1_REG_H__
#define __LINUX_MBKP1_REG_H__

#ifdef	__cplusplus
extern "C" {
#endif

/* 

ÍÏÄÕÌÉ: MBKP1 - Send + Receive, MBKP2 - Receive + Receive 
   xxx1 - ÐÒÉÅÍÎÉË × MBKP1 (SR) ÉÌÉ ÐÒÉÅÍÎÉË 1 × MBKP2 (RR) 
   xxx2 - ÐÒÉÅÍÎÉË 2 × MBKP2 (RR) 			     

*/

#define BYTE_IN_TRWD     4 


/*               óÍÅÝÅÎÉÑ ÒÅÇÉÓÔÒÏ× íâëð 		     */

/*  ÷ÉÒÔÕÁÌØÎÙÅ ÁÄÒÅÓÁ ÂÕÆÅÒÁ É ÓÞÅÔÞÉËÉ DVMA-ÏÂÍÅÎÏ×. 	     */ 
/*  úÁÐÉÓØ × CNT_* Ñ×ÌÑÅÔÓÑ ËÏÍÁÎÄÏÊ ÚÁÐÕÓËÁ ÏÂÍÅÎÁ ÂÕÆÅÒÏÍ  */


#define RESET_MBKP 0x40	/* ÏÂÝÉÊ ÓÂÒÏÓ íâëð (ÏÂÏÉÈ ËÁÎÁÌÏ×)	*/

#if NEW_MBKP
#define SGCNT_MASK  0x00ffffff /* ÍÁÓËÁ ÓÞÅÔÞÉËÁ ÓÏ ÚÎÁËÏÍ × ÄÏÐ ËÏÄÅ */
#else
#define SGCNT_MASK  0x001fffff /* ÍÁÓËÁ ÓÞÅÔÞÉËÁ ÓÏ ÚÎÁËÏÍ × ÄÏÐ ËÏÄÅ */
#endif /* NEW_MBKP */

#define MAX_CNT     0x000fffff  /* ÍÁÓËÁ ÓÞÅÔÞÉËÁ ÂÅÚ ÚÎÁËÁ  	    */
#define COMPL_MASK  0xfff00000  /* ÚÎÁË,ÓÞÅÔÞÉË => ÏÔÒÉÃ ÞÉÓÌÏ */

/* ÐÒÅÒÙ×ÁÎÉÑ É ÍÁÓËÉ  */

#define INTR_M_WD 0x18 /* ÓÌÏ×Ï ÍÁÓËÉ ÐÒÅÒÙ×ÁÎÉÊ		*/
#define INTR_M1   0x02 /* ÂÉÔÙ ÍÁÓËÉ: ÎÏÒÍÁÌØÎÙÅ É ÞÅÔÎÏÓÔØ 	*/	
#define INTR_M2   0x04 /* 	      Á×ÁÒÉÊÎÙÅ			*/ 
#define INTR_M3   0x08 /* ÒÅÚÅÒ×, ÐÏËÁ ÄÏÌÖÅÎ ÂÙÔØ 0		*/

#define WRDESC_BUSY 1  /* 1 - ÐÒÉÅÍÎÉË ÅÝÅ ÎÅ ÐÒÉÎÑÌ ×ÙÄÁÎÎÙÊ ÄÅÓËÒÉÐÔÏÒ */

#define INTR_EV_WD 0x30 /* ÓÌÏ×Ï ÓÏÂÙÔÉÊ ÐÒÅÒÙ×ÁÎÉÊ É		*/
		       /* ÉÈ ÂÉÔÙ, Mx=1 - ÒÁÚÒÅÛÁÀÝÁÑ ÍÁÓËÁ     */	


#if MBKP1 

#define BUF_TR    0x01 		/* ËÏÎÅÃ ÐÅÒÅÄÁÞÉ ÂÕÆÅÒÁ ÐÒÉÅÍÎÉËÁ 1	- M1 */
#define BUF_RCV1  0x02 		/* ËÏÎÅÃ ÐÒÉÅÍÁ ÂÕÆÅÒÁ ÐÒÉÅÍÎÉËÁ 1	- M1 */
#define PAR_RCV1  0x04 		/* ÞÅÔÎÏÓÔØ ÄÁÎÎÙÈ ÐÒÉÅÍÎÉËÁ 1		- M1 */
#define PAR_SBUS  0x08 		/* ÞÅÔÎÏÓÔØ ÐÒÉ ÐÒÉÅÍÅ ÉÚ SBUS		- M3 */

#define ERR_SBUS  0x10 		/* ÎÅËÏÒÒÅËÔÎÏÅ ÐÏÄÔ× ÉÌÉ Late_Error	- M2 */
#define DESC_RCV1 0x20 		/* ÐÒÉÎÑÔ ÄÅÓËÒÉÐÔÏÒ ÐÒÉÅÍÎÉËÏÍ 1	- M1 */		           
#define CH0_INT  (BUF_RCV1  | PAR_RCV1 | DESC_RCV1)  	/* ÐÒÅÒÙ×ÁÎÉÑ ËÁÎÁÌÁ 0 */
#define CH1_INT  (BUF_TR)  				/* ÐÒÅÒÙ×ÁÎÉÑ ËÁÎÁÌÁ 1 */
#define MOD_INT  ( PAR_SBUS | ERR_SBUS)		    	/* ÐÒÅÒÙ×ÁÎÉÑ ÍÏÄÕÌÑ */

#else

#define M2MAS_RCV1    0x02	/* ÐÒÉÎÑÔ ÍÁÓÓÉ× ÐÒÉÅÍÎÉËÏÍ 1              - M1 */
#define M2PAR_RCV1    0x04	/* ÞÅÔÎÏÓÔØ ÍÁÓÓÉ×Á ÐÒÉÅÍÎÉËÁ 1            - M1 */
#define M2SBUS_PAR    0x08	/* ÞÅÔÎÏÓÔØ ÐÏ sbus - ÏÄÎÏÍ ÉÚ ËÁÎÁÌÏ×     - M2 */

#define M2SBUS_LATERR 0x10	/* sbus - late error × ÏÄÎÏÍ ÉÚ ËÁÎÁÌÏ×    - M2 */
#define M2DESC_RCV1   0x20	/* ÐÒÉÎÑÔ ÄÅÓËÒÉÐÔÏÒ ÐÒÉÅÍÎÉËÏÍ 1          - M1 */
#define M2MAS_RCV2    0x40	/* ÐÒÉÎÑÔ ÍÁÓÓÉ× ÐÒÉÅÍÎÉËÏÍ 2              - M1 */
#define M2PAR_RCV2    0x80	/* ÞÅÔÎÏÓÔØ ÍÁÓÓÉ×Á ÐÒÉÅÍÎÉËÁ 2            - M1 */

#define M2DESC_RCV2  0x100	/* ÐÒÉÎÑÔ ÄÅÓËÒÉÐÔÏÒ ÐÒÉÅÍÎÉËÏÍ 2          - M1 */

#define CH0_INT  ( M2MAS_RCV1 | M2PAR_RCV1 | M2DESC_RCV1 )  /* ÐÒÅÒÙ×ÁÎÉÑ ËÁÎÁÌÁ 0 */
#define CH1_INT  ( M2MAS_RCV2 | M2PAR_RCV2 | M2DESC_RCV2 )  /* ÐÒÅÒÙ×ÁÎÉÑ ËÁÎÁÌÁ 1 */
#define MOD_INT  ( M2SBUS_PAR | M2SBUS_LATERR)		    /* ÐÒÅÒÙ×ÁÎÉÑ ÍÏÄÕÌÑ */

#endif

#define ALL_INT (CH0_INT | CH1_INT | MOD_INT)  

/* ÒÅÇÉÓÔÒÙ DVMA-ÁÄÒÅÓÏ× É ÓÞÅÔÞÉËÏ× */

#define VA_TR     0x08   /* íâëð1: DVMA-ÁÄÒÅÓ ÐÅÒÅÄÁÞÉ ÍÁÓÓÉ×Á */
#define VA_RCV2   0x08   /* íâëð1: DVMA-ÁÄÒÅÓ ÐÒÉÅÍÁ ÍÁÓÓÉ×Á ÐÏ ËÁÎÁÌÕ 2 */ 

#define VA_RCV1   0x10   /* DVMA-ÁÄÒÅÓ ÐÒÉÅÍÁ ÍÁÓÓÉ×Á ÐÏ ËÁÎÁÌÕ 1 */
#define CNT_RCV1  0x50   /* ÓÞÅÔÞÉË ÓÌÏ× ÐÒÉÅÍÁ ÍÁÓÓÉ×Á ÐÏ ËÁÎÁÌÕ 1 */

#define CNT_TR    0x60   /* íâëð1: ÓÞ ÓÌÏ× ÐÅÒÅÄÁÞÉ ÍÁÓÓÉ×Á ÐÏ ËÁÎÁÌÕ 1 */
#define CNT_RCV2  0x60   /* íâëð1: ÓÞ ÓÌÏ× ÐÅÒÅÄÁÞÉ ÍÁÓÓÉ×Á ÐÏ ËÁÎÁÌÕ 2 */


#define WRCMD_RCV1    0x20  /* íâëð1: ÚÐ - ÚÁÐÉÓØ ÄÅÓËÉÐÔÏÒÁ  × ËÁÎÁÌ */
#define SW_TRBA_RCV2  0x20  /* íâëð2: ÞÔ - ×ÙË, ÚÐ - ×ËÌ ÏÂÓÌÕÖÉ×ÁÎÉÅ ôòâá2 */

#define SW_TRBA_RCV1  0x28  /* ÚÐ - ×ËÌ, ÞÔ - ×ÙËÌ ÏÂÓÌÕÖÉ×ÁÎÉÅ ôòâá */
#define RD_DESC_RCV1  0x38  /* ÞÔ - ÞÉÔÁÔØ ÄÅÓËÒÉÐÔÏÒ ÐÏ ôòâá1 */
#define RD_DESC_RCV2  0x68  /* ÞÔ - ÞÉÔÁÔØ ÄÅÓËÒÉÐÔÏÒ ÐÏ ôòâá2 */

#define SCHAN_SEL     0x48   /* ÞÔ - ÏÓÎÏ×ÎÏÊ, ÚÐ - ÒÅÚÅÒ×ÎÙÊ ËÁÎÁÌ */

#define RESET_TR1     0x58  /* ÚÐ - ÏÂÎÕÌÉÔØ ÕÄÁÌÅÎÎÙÊ ÐÅÒÅÄÁÔÞÉË_1 */
#define RESET_TR2     0x70  /* ÚÐ - ÏÂÎÕÌÉÔØ ÕÄÁÌÅÎÎÙÊ ÐÅÒÅÄÁÔÞÉË_2 */

#define RD_FIFO_1     0x78  /* þÔÅÎÉÅ FIFO ÐÒÉÅÍÎÉËÁ 1 		    */
#define RD_FIFO_2     0x88  /* þÔÅÎÉÅ FIFO ÐÒÉÅÍÎÉËÁ 2		    */

#define RESET_PEER_RCV 0x68  /* ÚÐ - ÏÂÎÕÌÉÔØ ÕÄÁÌÅÎÎÙÊ ÐÒÉÅÍÎÉË */
#define RESET_PEER_TR  0x78  /* ÚÐ - ÏÂÎÕÌÉÔØ ÕÄÁÌÅÎÎÙÊ ÐÅÒÅÄÁÔÞÉË */

#ifdef	__cplusplus
}
#endif

#endif /* __LINUX_MBKP1_REG_H__ */
