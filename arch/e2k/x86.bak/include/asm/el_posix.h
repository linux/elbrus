#ifndef _ASM_EL_POSIX_ATOMIC_H
#define _ASM_EL_POSIX_ATOMIC_H

#ifdef __KERNEL__

#include <asm/errno.h>

#define ARCH_HAS_GET_CYCLES
#define ARCH_HAS_ATOMIC_CMPXCHG

#define el_atomic_cmpxchg(x, uaddr, oldval, newval) \
		__el_atomic_cmpxchg(&x, uaddr, oldval, newval)

static inline int __el_atomic_cmpxchg(int *x, int *uaddr, int oldval,
		int newval)
{
	int ret;

	asm volatile(
#ifdef CONFIG_SMP
		     "1:\tlock; cmpxchgl %4, %2\n"
#else
		     "1:\tcmpxchgl %4, %2\n"
#endif
		     "2:\t.section .fixup, \"ax\"\n"
		     "3:\tmov     %3, %1\n"
		     "\tjmp     2b\n"
		     "\t.previous\n"
		     _ASM_EXTABLE(1b, 3b)
		     : "=a" (oldval), "=r" (ret), "+m" (*uaddr)
		     : "i" (-EFAULT), "r" (newval), "0" (oldval), "1" (0)
		     : "memory"
	);

	*x = oldval;

	return ret;
}

#define el_atomic_cmpxchg_acq(...)	el_atomic_cmpxchg(__VA_ARGS__)
#define el_atomic_cmpxchg_rel(...)	el_atomic_cmpxchg(__VA_ARGS__)

#define el_atomic_xchg_acq(x, uaddr, value) \
		__el_atomic_xchg_acq(&x, uaddr, value)

static inline int __el_atomic_xchg_acq(int *x, int *uaddr, const int value)
{
	int ret, oldval;

	asm volatile(
		     "1:\txchgl %0, %2\n"
		     "2:\t.section .fixup,\"ax\"\n"
		     "3:\tmov\t%3, %1\n"
		     "\tjmp\t2b\n"
		     "\t.previous\n"
		     _ASM_EXTABLE(1b, 3b)
		     : "=r" (oldval), "=r" (ret), "+m" (*uaddr)
		     : "i" (-EFAULT), "0" (value), "1" (0)
	);

	*x = oldval;

	return ret;
}
#endif
#endif
