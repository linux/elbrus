/*
 * x86 TSC related functions
 */
#ifndef _ASM_X86_TSC_H
#define _ASM_X86_TSC_H

#include <asm/processor.h>

#define NS_SCALE	10 /* 2^10, carefully chosen */
#define US_SCALE	32 /* 2^32, arbitralrily chosen */

/*
 * Standard way to access the cycle counter.
 */
typedef unsigned long long cycles_t;

extern unsigned int cpu_khz;
extern unsigned int tsc_khz;

extern void disable_TSC(void);

static inline cycles_t get_cycles(void)
{
	unsigned long long ret = 0;

#ifndef CONFIG_X86_TSC
	if (!cpu_has_tsc)
		return 0;
#endif
	rdtscll(ret);

	return ret;
}

#ifdef CONFIG_MCST
#define	UNSET_CPU_FREQ	((u32)(-1))
extern u32 cpu_freq_hz;

static inline long long cycles_2nsec(cycles_t cycles)
{
#if BITS_PER_LONG == 64
	return cycles * 1000 / (cpu_freq_hz / 1000000);
#else
	long long long_res = cycles * 1000;
	do_div(long_res, (cpu_freq_hz / 1000000));
	return long_res;
#endif /* BITS_PER_LONG */
}

static inline long long cycles_2usec(cycles_t cycles)
{
#if BITS_PER_LONG == 64
	return cycles / (cpu_freq_hz / 1000000);
#else
	long long long_res = cycles;
	do_div(long_res, (cpu_freq_hz / 1000000));
	return long_res;
#endif /* BITS_PER_LONG */
}

static inline cycles_t usecs_2cycles(long long usecs)
{
#if BITS_PER_LONG == 64
	return usecs * cpu_freq_hz / 1000000;
#else
	long long long_res = usecs * cpu_freq_hz;
	do_div(long_res, 1000000);
        return long_res;
#endif /* BITS_PER_LONG */
}

#endif /* CONFIG_MCST */


static __always_inline cycles_t vget_cycles(void)
{
	/*
	 * We only do VDSOs on TSC capable CPUs, so this shouldn't
	 * access boot_cpu_data (which is not VDSO-safe):
	 */
#ifndef CONFIG_X86_TSC
	if (!cpu_has_tsc)
		return 0;
#endif
	return (cycles_t)__native_read_tsc();
}

extern void tsc_init(void);
extern void mark_tsc_unstable(char *reason);
extern int unsynchronized_tsc(void);
extern int check_tsc_unstable(void);
extern int check_tsc_disabled(void);
extern unsigned long native_calibrate_tsc(void);

extern int tsc_clocksource_reliable;

/*
 * Boot-time check whether the TSCs are synchronized across
 * all CPUs/cores:
 */
extern void check_tsc_sync_source(int cpu);
extern void check_tsc_sync_target(void);

extern int notsc_setup(char *);
extern void tsc_save_sched_clock_state(void);
extern void tsc_restore_sched_clock_state(void);

/* MSR based TSC calibration for Intel Atom SoC platforms */
unsigned long try_msr_calibrate_tsc(void);

#endif /* _ASM_X86_TSC_H */
