#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <asm/mcst_eth_mac.h>

unsigned char l_base_mac_addr[6] = {0};
int l_cards_without_mac = 0;

EXPORT_SYMBOL(l_cards_without_mac);
EXPORT_SYMBOL(l_base_mac_addr);

static int __init machine_mac_addr_setup(char *str)
{
	char *cur = str;
	int i;
	for (i = 0; i < 6; i++) {
		l_base_mac_addr[i] =
			(unsigned char)simple_strtoull(cur, &cur, 16);
		if (*cur != ':') {
			break;
		}
		cur++;
	}
	pr_info("MCST_base_mac_addr = %02x:%02x:%02x:%02x:%02x:%02x\n",
		l_base_mac_addr[0],
		l_base_mac_addr[1],
		l_base_mac_addr[2],
		l_base_mac_addr[3],
		l_base_mac_addr[4],
		l_base_mac_addr[5]);
	return 0;
}
__setup("mcst_mac=", machine_mac_addr_setup);

