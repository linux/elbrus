#ifndef _E2K_POSIX_TYPES_H_
#define _E2K_POSIX_TYPES_H_

/*
 * This file is generally used by user-level software, so you need to
 * be a little careful about namespace pollution etc.
 */

#include <asm-generic/posix_types.h>

#endif /* _E2K_POSIX_TYPES_H_ */
