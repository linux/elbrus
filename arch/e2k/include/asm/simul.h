
#ifndef _E2K_SIMUL_H_
#define	_E2K_SIMUL_H_

// Physical address 'BOOT_KERNEL_BASE' to load Linux image is now fixed
// for simulator. In real mode execution this address is variable
// pursuant physical memory configuration of machine and should be
// calculated by boot or loader module

// #define	BOOT_KERNEL_BASE	0x0000000000200000

#endif  /* _E2K_SIMUL_H_ */
