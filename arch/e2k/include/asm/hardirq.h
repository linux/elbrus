#ifndef __ASM_E2K_HARDIRQ_H
#define __ASM_E2K_HARDIRQ_H

#define __ARCH_IRQ_EXIT_IRQS_DISABLED

#include <asm-l/hardirq.h>

#endif /* __ASM_E2K_HARDIRQ_H */
