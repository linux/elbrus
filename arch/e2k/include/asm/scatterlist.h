#ifndef _E2K_SCATTERLIST_H_
#define _E2K_SCATTERLIST_H_

#define ISA_DMA_THRESHOLD (0xffffffffUL)

#include <asm-generic/scatterlist.h>

#endif /* _E2K_SCATTERLIST_H_ */
