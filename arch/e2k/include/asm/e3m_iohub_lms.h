#ifndef _E3M_IOHUB_LMS_H_
#define _E3M_IOHUB_LMS_H_

extern void __init boot_e3m_iohub_lms_setup_arch(void);
extern void __init e3m_iohub_lms_setup_arch(void);
extern void __init e3m_iohub_lms_setup_machine(void);

#endif /* _E3M_IOHUB_LMS_H_ */
