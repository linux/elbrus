/*
 * Per-cpu current frame pointer - the location of the last exception frame on
 * the stack, stored in the per-cpu area.
 *
 * Jeremy Fitzhardinge <jeremy@goop.org>
 */
#ifndef _ASM_E2K_IRQ_REGS_H
#define _ASM_E2K_IRQ_REGS_H

#include <asm-generic/irq_regs.h>

#endif /* _ASM_E2K_IRQ_REGS_H */
