/*
 *  linux/include/asm-e2k/hdreg.h
 *
 *  Copyright (C) 1994-1996  Linus Torvalds & authors
 */

#warning this file is obsolete, please do not use it


#ifndef _E2K_HDREG_H_
#define _E2K_HDREG_H_

typedef unsigned short ide_ioreg_t;

#endif /* _E2K_HDREG_H_ */
