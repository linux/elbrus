#ifndef _E2K_PROT_LOADER_H_
#define _E2K_PROT_LOADER_H_

#include <asm/e2k_ptypes.h>


#define USE_ELF64 0


#define	ARGS_AS_ONE_ARRAY

#define	E2k_ELF_ARGV_IND		0
#define	E2k_ELF_ENVP_IND		1
#define	E2k_ELF_AUX_IND			2
#define	E2k_ELF_ARG_NUM_AP	3


#define DT_TCT                          0x70001005
#define DT_GOTT                         0x7000100c
#define DT_GCTT                         0x7000100d
#define DT_GOMPT                        0x7000100e
#define DT_GOTTSZ                       0x7000100f
#define DT_GCTTSZ                       0x70001010
#define DT_GOMPTSZ                      0x70001011
#define DT_PLTGOTSZ                     0x7000101b
#define DT_INIT_GOT                     0x7000101c


typedef struct {
	e2k_pl_t			mdd_init_got;
	e2k_pl_t			mdd_init;
	e2k_pl_t			mdd_fini;
	e2k_pl_t			mdd_start;
	e2k_ptr_t                       mdd_got;
        /* ðÒÉ ×ÙÚÏ×Å ÓÀÄÁ ÐÏÍÅÝÁÀÔÓÑ ÄÅÓËÒÉÐÔÏÒÙ ÏÂÌÁÓÔÅÊ ÐÁÍÑÔÉ, ÓÏÄÅÒÖÁÝÉÈ ÚÁÇÏÔÏ×ËÉ
           (ÂÅÚ ×ÎÅÛÎÉÈ ÔÜÇÏ×) ÄÌÑ ÆÏÒÍÉÒÏ×ÁÎÉÑ ÔÜÇÉÒÏ×ÁÎÎÙÈ ÚÎÁÞÅÎÉÊ, ÒÁÚÍÅÝÁÅÍÙÈ × ÓÅËÃÉÑÈ
           .gott (OT), .gctt (CT) É .gompt (OMP) ÚÁÇÒÕÖÁÅÍÏÇÏ ÍÏÄÕÌÑ. */
        e2k_ptr_t                       mdd_gtt[3];
} umdd_t;
#define MDD_PROT_SIZE	((sizeof (umdd_t) + 15) & ~15)


typedef struct {
	u64		got_addr;
	u64		got_len;
        char            *src_gtt_addr[3];
        u64             src_gtt_len[3];
	u64		init_got_point;
	u64		entry_point;
	u64		init_point;
	u64		fini_point;
}	kmdd_t;



	/* It's here for compatibility with old loader    */

typedef enum
{
    RTL_FT_NONE,   /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½*/
    RTL_FT_EXE,    /**< ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ */
    RTL_FT_LIB,    /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ */
    RTL_FT_DRV     /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½*/
} rtl_FileType_t;

typedef struct rtl_Unit_s rtl_Unit_t;

struct rtl_Unit_s
{
    char *u_code;                        /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ ï¿½ï¿½ */
    char *u_data;                        /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ */
    char *u_name;                        /**< ï¿½ï¿½ï¿½ï¿½ï¿½ */
    char *u_fullname;                    /**< ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ */
    char *u_type_map;
    char *u_type_structs;                /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½
                                          * ï¿½ï¿½ï¿½ï¿½ */
    char *u_type_structs_end;            /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ */
    rtl_Unit_t *u_next;                  /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ */
    rtl_Unit_t *u_prev;                  /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ */
    char *u_init;                        /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ */
    char *u_fini;                        /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ */
    unsigned long long u_entry;          /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½*/
    rtl_FileType_t u_mtype;              /**< ï¿½ï¿½ï¿½ï¿½ï¿½ */
    unsigned int u_num;                  /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ */
    unsigned int u_tnum;                 /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ */
    unsigned int u_tcount;               /**< ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ */

    struct
    {
        unsigned long long ub_code;      /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ */
        unsigned long long ub_data;      /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ */
        unsigned long long ub_bss;
        unsigned long long ub_brk;       /**< ï¿½ï¿½ï¿½brk */
    } base;

    struct
    {
        unsigned long long uc_start;         /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ */
        unsigned long long uc_dataend;       /**< ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ */
        unsigned long long uc_allocend;      /**< ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ */
        unsigned long long uc_mapend;        /**< ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ */
        unsigned long long uc_mapoff;        /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ ï¿½ ï¿½ï¿½ï¿½*/
        unsigned int uc_prot;                /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ */
    } code;

    struct
    {
        unsigned long long ud_start;         /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ */
        unsigned long long ud_dataend;       /**< ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ */
        unsigned long long ud_allocend;      /**< ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ */
        unsigned long long ud_mapend;        /**< ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ */
        unsigned long long ud_mapoff;        /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ ï¿½ ï¿½ï¿½ï¿½*/
        unsigned int ud_prot;                /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ */
    } data;

    /* ELF ï¿½ï¿½ */
    char *u_eheader;                     /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½*/
    char *u_pheader;                     /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½*/
    char *u_symtab;                      /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½*/
    char *u_symtab_st;                   /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½*/
    char *u_strtab;                      /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½*/
    char *u_strtab_st;                   /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½*/
    unsigned int *u_hash;                /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ hash ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ */
    char *u_got;                         /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ */
    char *u_gtt;                         /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½*/
    char *u_type;                        /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½++ */
    char *u_dynrel;                      /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½*/
    char *u_gttrel;                      /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½GTT */
    char *u_typerel;                     /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½*/
    char *u_dyn;                         /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ */
    char *u_tobj;                        /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½*/
    char *u_tcast;                       /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ */
    char *u_typed;                       /**< ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½*/

    struct
    {
        unsigned long long ul_code;      /**< ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ */
        unsigned long long ul_data;      /**< ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ */
        unsigned long long ul_strtab;    /**< ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½*/
        unsigned long long ul_strtab_st; /**< ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½*/
        unsigned long long ul_type;      /**< ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½++ */
        unsigned long long ul_dynrel;    /**< ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½*/
        unsigned long long ul_gttrel;    /**< ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½GTT */
        unsigned long long ul_typerel;   /**< ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½*/
        unsigned int ul_symtab;          /**< ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½½*/
        unsigned int ul_symtab_st;       /**< ï¿½ï¿½ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½*/
        unsigned int ul_hash;            /**< ï¿½ï¿½ï¿½ ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½*/
        unsigned int ul_gtt;			/* 		*/
        unsigned int ul_tobj;			/* 		 */
        unsigned int ul_typed;		/*		*/
        unsigned int ul_tcast;			/*		 */
    } len;

};

/*    Global Type Table (GTT) correction. C++ stuff hadling. */
extern void rtl32_CorrectionType( rtl_Unit_t *unit_p );

extern	long sys_load_cu_elf32_3P(char *name, kmdd_t *mdd);
extern	long sys_load_cu_elf64_3P(char *name, kmdd_t *mdd);

#endif	/* _E2K_PROT_LOADER_H_ */
	
