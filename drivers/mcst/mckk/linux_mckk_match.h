/*
 * Copyright (c) 1998 by MCST.
 */

/*
 * E90 modules shared interface matching
 */


#ifndef	__LINUX_MCKK_MATCH_H__
#define	__LINUX_MCKK_MATCH_H__

#define __MCKK_BOARD_DRIVER__ 

#include "linux_mckk.h"
#include <linux/mcst/linux_me90_match.h>

typedef	mckk_state_t			me90drv_state_t;
typedef	mckk_chnl_state_t		me90drv_chnl_state_t;
typedef mckk_state_t			mcb_state_t;

#endif	/* __LINUX_MCKK_MATCH_H__ */
