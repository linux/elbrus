/* 
*	 Copyright (c) 2005 by MCST.
* 
* Written by Alexey V. Sitnikov, MCST 2005	
*/

#include "pdc_reg.h"

/*
*	 ÂÌÏË ÐÁÒÁÍÅÔÒÏ× IOCTL-ÏÐÅÒÁÃÉÊ
*/
typedef struct pdc_ioc_parm {
	unsigned long	data;		/* äÁÎÎÙÅ 	*/
	size_t	size;			/* ÒÁÚÍÅÒ ÄÁÎÎÙÈ (× ÂÁÊÔÁÈ) */
	int	err_no;			/* ÏÔ ÄÒÁÊ×ÅÒÁ: ËÏÄ ÏÛÉÂËÉ PDC_E_... */ 
	unsigned int	rwmode;		/* ÄÒÁÊ×ÅÒÕ:    ÍÏÄÉÆÉËÁÔÏÒ IOCTL-ÏÐÅÒÁÃÉÊ */
} pdc_ioc_parm_t;

/*
*	IOCTL ÏÐÅÒÁÃÉÊ ( × ÐÏÌÅ mbkp_ioc_parm.rwmode)
*/

#define PDC_CHECK		20 /* ðÒÏ×ÅÒËÁ ÕÓÔÒÏÊÓÔ×Á ÎÁ ÚÁÎÑÔÏÓÔØ */
#define PDC_USER_BUFFER		21 /* ðÅÒÅÄÁÔØ ÂÕÆÅÒ ÐÏÌØÚÏ×ÁÔÅÌÑ */	
#define PDC_USER_DATA		22 /* ðÅÒÅÄÁÔØ ÄÁÎÎÏÅ ÐÏÌØÚÏ×ÁÔÅÌÑ */


/*
*	ËÏÄ IOCTL ÏÐÅÒÁÃÉÊ ( kop )
*/ 

#define PDC_IOC_ALLOCB		0 /* ×ÙÄÅÌÉÔØ DMA ÂÕÆÅÒ. äÌÑ ÒÅÖÉÍÁ Master */
#define PDC_IOC_FREEB		1 /* ïÓ×ÏÂÏÄÉÔØ DMA ÂÕÆÅÒ. äÌÑ ÒÅÖÉÍÁ Master */	
#define PDC_WRR			2 /* ðÒÏÉÚ×ÅÓÔÉ ÏÐÅÒÁÃÉÀ ÚÁÐÉÓÉ × ÒÅÇÉÓÔÒ */
#define PDC_RDR			3 /* ðÒÏÉÚ×ÅÓÔÉ ÏÐÅÒÁÃÉÀ ÞÔÅÎÉÑ ÒÅÇÉÓÔÒÁ */
#define PDC_IOC_ALLOCB_ALIGNED  23 /* ÷ÙÄÅÌÉÔØ ÐÁÍÑÔØ äíá ×ÙÒÏ×ÎÅÎÎÕÀ ÐÏ i_size_bytes *i + size_bytes */
#define PDC_WRITE_MORE_THAN_16BITS_SIZE 24 /* ÐÅÒÅÄÁÔØ ÌÀÂÏÅ ËÏÌÉÞÅÓÔ×Ï ÐÁÍÑÔÉ */
#define PDC_READ_MORE_THAN_16BITS_SIZE 25  /* ÐÒÉÎÑÔØ ÌÀÂÏÅ ËÏÌÉÞÅÓÔ×Ï ÐÁÍÑÔÉ */
#define PDC_MB_RECIEVE		4 /* ðÏÌÕÞÉÔØ ÄÁÎÎÙÅ × ÂÕÆÅÒ DMA */
#define PDC_MB_TRANSMIT		8 /* ðÅÒÅÄÁÔØ ÄÁÎÎÙÅ DMA ÂÕÆÅÒÁ */
#define PDC_SB_RECIEVE		16 /* þÉÔÁÔØ ÄÁÎÎÙÅ ÉÚ ÁÐÐÁÒÁÔÎÏÇÏ Slave Recieve ÂÕÆÅÒÁ */ 
#define PDC_SB_TRANSMIT		32 /* ðÉÓÁÔØ ÄÁÎÎÙÅ × ÁÐÐÁÒÁÔÎÙÊ Slave Transmit ÂÕÆÅÒ */
#define PDC_WAITING_RMI_MASTER	64  /* ïÖÉÄÁÎÉÅ ÕÄÁÌÅÎÎÏÇÏ ÐÒÅÒÙ×ÁÎÉÑ × ÒÅÖÉÍÅ master */
#define PDC_WAITING_RMI_SLAVE	128 /* ïÖÉÄÁÎÉÅ ÕÄÁÌÅÎÎÏÇÏ ÐÒÅÒÙ×ÁÎÉÑ × ÒÅÖÉÍÅ slave */
#define PDC_SEND_PI		6 /* çÅÎÅÒÁÃÉÑ ÕÄÁÌÅÎÎÏÇÏ PI ÐÒÅÒÙ×ÁÎÉÑ */
#define PDC_SEND_NMI		7 /* çÅÎÅÒÁÃÉÑ ÕÄÁÌÅÎÎÏÇÏ NMI ÐÒÅÒÙ×ÁÎÉÑ */
#define PDC_RESET		10 /* ïÂÝÉÊ ÓÂÒÏÓ ÑÞÅÊËÉ */
#define PDC_CLEAR_MASTER_TASK	11 /* óÂÒÏÓ ÚÁÄÁÞÉ Master (ÂÉÔÙ MSize Á ÔÁËÖÅ MRB É MTB ÎÅ ÏÂÎÕÌÑÀÔÓÑ) */
#define PDC_CLEAR_SLAVE_TASK	12 /* óÂÒÏÓ ÚÁÄÁÞÉ Slave (ÂÉÔÙ SSize Á ÔÁËÖÅ SRB É STB ÎÅ ÏÂÎÕÌÑÀÔÓÑ) */
#define PDC_CLEAR_MTB		13 /* ïÞÉÓÔËÁ ÂÕÆÅÒÁ MTB */
#define PDC_CLEAR_MRB		14 /* ïÞÉÓÔËÁ ÂÕÆÅÒÁ MRB */
#define PDC_CLEAR_STB		9 /* ïÞÉÓÔËÁ ÂÕÆÅÒÁ STB */
#define PDC_CLEAR_SRB		15 /* ïÞÉÓÔËÁ ÂÕÆÅÒÁ SRB */
#define PDC_CLEAR_LAST_INT	17 /* ïÂÎÕÌÉÔØ ÉÎÆÏÒÍÁÃÉÀ Ï ÐÏÓÌÅÄÎÅÍ ÐÒÅÒÙ×ÁÎÉÉ × ÓÉÓÔÅÍÅ */
#define PDC_SHOW_LAST_INT	5 /* ÷ÅÒÎÕÔØ ÉÎÆÏÒÍÁÃÉÀ Ï ÐÏÓÌÅÄÎÅÍ ÐÒÅÒÙ×ÁÎÉÉ × ÓÉÓÔÅÍÅ */
#define PDC_INIT		18 /* äÅÆÏÌÔÎÁÑ ÉÎÉÃÉÁÌÉÚÁÃÉÑ ÉÎÓÔÁÎÓÁ */
#define PDC_SET_TIMER		19 /* õÓÔÁÎÏ×ËÁ ÔÁÊÍÅÒÁ */

/*
*	ËÏÄÙ ÚÁ×ÅÒÛÅÎÉÑ ÏÐÅÒÁÃÉÊ × ÐÏÌÅ pdc_ioc_parm_t.err_no
*	err_no!= PDC_E_NORMAL ÐÒÉ 
*/ 

#define PDC_E_NORMAL 	 	0 /* ÎÏÒÍÁÌØÎÏÅ ÚÁ×ÅÒÛÅÎÉÅ ÏÐÅÒÁÃÉÉ 	 */
#define PDC_E_PENDING	 	1 /* õÓÔÒÏÊÓÔ×Ï ÚÁÎÑÔÏ (ÐÒÉ ÐÏÐÙÔËÅ ×ÙÐÏÌÎÉÔØ ÚÁÄÁÞÕ) */
#define PDC_E_INVAL		2 /* îÅ×ÅÒÎÙÊ ÁÒÇÕÍÅÎÔ */	
#define PDC_E_INIT_MEM		4 /* ðÒÉ ×ÙÐÏÌÎÅÎÉÉ ÚÁÄÁÞÉ Master ÎÅ ×ÙÄÅÌÅÎ DMA ÂÕÆÅÒ */
#define PDC_E_ERTRANS		8 /* ÷ ÓÉÓÔÅÍÅ ÚÁÒÅÇÉÓÔÒÉÒÏ×ÁÎÏ ÐÒÅÒÙ×ÁÎÉÅ Err */
#define PDC_E_TIMER		16 /* ÷ÙÈÏÄ ÐÏ ÔÁÊÍÅÒÕ */
#define PDC_E_ERWAIT		32 /* ÷Ï ×ÒÅÍÑ ÏÐÅÒÁÃÉÉ PDC_WAITING_RMI ÚÁÒÅÇÉÓÔÒÉÒÏ×ÁÎÏ ÐÒÅÒÙ×ÁÎÉÅ Err */
#define PDC_E_SIZE		64 /* îÅ×ÅÒÎÏÅ ÒÁÚÍÅÒ ÚÁÐÒÁÛÉ×ÁÅÍÙÈ/ÐÅÒÅÄÁ×ÁÅÍÙÈ ÄÁÎÎÙÈ */
#define PDC_E_MEMORY_ALLOC	128 /* ïÛÉÂËÁ ÐÒÉ ×ÙÄÅÌÅÎÉÉ ÂÕÆÅÒÁ × ÒÅÖÉÍÅ PDC_USER_BUFFER */
#define PDC_E_DDI_COPYIN	256 /* ïÛÉÂËÁ ÐÒÉ ×ÙÐÏÌÎÅÎÉÉ ÏÐÅÒÁÃÉÉ copy_from_user */
#define PDC_E_DDI_COPYOUT	512 /* ïÛÉÂËÁ ÐÒÉ ×ÙÐÏÌÎÅÎÉÉ ÏÐÅÒÁÃÉÉ copy_to_user */
#define PDC_E_NOBUF		1024 /* ïÛÉÂËÁ ÐÒÉ ×ÙÄÅÌÅÎÉÉ DMA ÂÕÆÅÒÁ, ÏÐÅÒÁÃÉÑ PDC_IOC_ALLOCB */
#define PDC_E_ALREADY_WAIT	4096 /* õÖÅ ÎÁÈÏÄÉÍÓÑ × ÓÔÁÄÉÉ ÏÖÉÄÁÎÉÑ õÄÁÌÅÎÎÏÇÏ ÐÒÅÒÙ×ÁÎÉÑ × ÄÁÎÎÏÍ ÒÅÖÉÍÅ */
/*
*	ëÏÄÙ ÏÔÏÂÒÁÖÁÀÛÉÅ ÓÏÓÔÏÑÎÉÅ ÕÓÔÒÏÊÓÔ×Á ÐÉÛÕÔØÓÑ ÔÁËÖÅ ×  pdc_ioc_parm_t.err_no
*/

#define	PDC_BUSY	3	/* õÓÔÒÏÊÓÔ×Ï ÚÁÎÑÔÏ (ÐÒÉ ÐÒÏ×ÅÒËÅ ÎÁ ÚÁÎÑÔÏÓÔØ)  */
#define PDC_NOTRUN	5	/* õÓÔÒÏÊÓÔ×Ï Ó×ÏÂÏÄÎÏ (ÐÒÉ ÐÒÏ×ÅÒËÅ ÎÁ ÚÁÎÑÔÏÓÔØ) */
#define PDC_RMI		6	/* ÷ ÓÉÓÔÅÍÅ ÚÁÒÅÇÉÓÔÒÉÒÏ×ÁÎÏ õÄÁÌÅÎÎÏÅ ÐÒÅÒ×ÁÎÉÅ */
#define PDC_SIGNAL	2048	/* ðÏÌÕÞÅÎ ÓÉÇÎÁÌ */


struct code_msg {
	int code;
	char * msg;
};

typedef struct code_msg code_msg_t; 

code_msg_t iocerrs[] = {
	{PDC_E_NORMAL, "PDC_E_NORMAL"},
	{PDC_E_PENDING, "PDC_E_PENDING"},
	{PDC_E_INVAL, "PDC_E_INVAL"},
	{PDC_E_INIT_MEM, "PDC_E_INIT_MEM"},
	{PDC_E_ERTRANS, "PDC_E_ERTRANS"},
	{PDC_E_TIMER, "PDC_E_TIMER"},
	{PDC_E_ERWAIT, "PDC_E_ERWAIT"},
	{PDC_E_SIZE, "PDC_E_SIZE"},
	{PDC_E_MEMORY_ALLOC, "PDC_E_MEMORY_ALLOC"},
	{PDC_E_DDI_COPYIN, "PDC_E_DDI_COPYIN"},
	{PDC_E_DDI_COPYOUT, "PDC_E_DDI_COPYOUT"},
	{PDC_E_NOBUF, "PDC_E_NOBUF"},
	{PDC_E_ALREADY_WAIT, "PDC_E_ALREADY_WAIT"},
	{PDC_BUSY, "PDC_BUSY"},
	{PDC_NOTRUN, "PDC_NOTRUN"},
	{PDC_RMI, "PDC_RMI"},
	{PDC_SIGNAL, "PDC_SIGNAL"},
};

code_msg_t ioctls[] = {
	{PDC_IOC_ALLOCB, "PDC_IOC_ALLOCB"}, 
	{PDC_IOC_FREEB, "PDC_IOC_FREEB"},
	{PDC_WRR, "PDC_WRR"},
	{PDC_RDR, "PDC_RDR"},
	{PDC_MB_RECIEVE, "PDC_MB_RECIEVE"},
	{PDC_MB_TRANSMIT, "PDC_MB_TRANSMIT"},
	{PDC_SB_RECIEVE, "PDC_SB_RECIEVE"},
	{PDC_SB_TRANSMIT, "PDC_SB_TRANSMIT"},
	{PDC_WAITING_RMI_MASTER, "PDC_WAITING_RMI_MASTER"},
	{PDC_WAITING_RMI_SLAVE, "PDC_WAITING_RMI_SLAVE"},
	{PDC_SEND_PI, "PDC_SEND_PI"}, 
	{PDC_SEND_NMI, "PDC_SEND_NMI"},
	{PDC_RESET, "PDC_RESET"},
	{PDC_CLEAR_MASTER_TASK, "PDC_CLEAR_MASTER_TASK"},
	{PDC_CLEAR_SLAVE_TASK, "PDC_CLEAR_SLAVE_TASK"},
	{PDC_CLEAR_MTB, "PDC_CLEAR_MTB"},
	{PDC_CLEAR_MRB, "PDC_CLEAR_MRB"},
	{PDC_CLEAR_STB, "PDC_CLEAR_STB"},
	{PDC_CLEAR_SRB, "PDC_CLEAR_SRB"},
	{PDC_CLEAR_LAST_INT, "PDC_CLEAR_LAST_INT"}, 
	{PDC_SHOW_LAST_INT, "PDC_SHOW_LAST_INT"},
};

code_msg_t rwmods[] = {
	{PDC_CHECK, "PDC_CHECK"}, 
	{PDC_USER_BUFFER, "PDC_USER_BUFFER"},
	{PDC_USER_DATA, "PDC_USER_DATA"},
	{PDC_SB_RECIEVE, "PDC_SB_RECIEVE"},
	{PDC_SB_TRANSMIT, "PDC_SB_TRANSMIT"},
	{PDC_MB_RECIEVE, "PDC_MB_RECIEVE"},
	{PDC_MB_TRANSMIT, "PDC_MB_TRANSMIT"},
};

char * msg_by_code (int code, code_msg_t * v, int len) {
	code_msg_t * p;
	int i;
	for (i=0; i < len ; i++) {
		p = v + i;
		if (p->code == code) 
			return p->msg;
	}
	return " code=? ";
}
