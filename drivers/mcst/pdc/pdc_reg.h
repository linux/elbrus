/* 
*
* 	Written by Alexey V. Sitnikov, alexmipt@mcst.ru, 2005
*
*/

#define ONCE_OPENING	0

#define PDC_INT_TRACE	0



#define AS_WORD(x)		(x.word) 
#define AS_STRUCT(x)		(x.fields)

/*               		óÍÅÝÅÎÉÑ ÒÅÇÉÓÔÒÏ× PDC 		     		*/

/******				PLVC Control Register				******/

/* Rst 			- ÏÂÝÉÊ ÓÂÒÏÓ PDC								*/
/* Rmode		- òÅÖÉÍ ÒÁÂÏÔÙ ËÏÎÔÒÏÌÌÅÒÁ ÐÏ ÐÒÉÅÍÕ 
				 0 - ÐÒÑÍÏÊ: PCI Master <- PCI Master, PCI Slave <- PCI Slave 
				 0 - ÐÅÒÅËÒÅÓÔÎÙÊ: PCI Master <- PCI Slave, PCI Slave <- PCI Master 	*/
/* Hmode		- Hide ÒÅÖÉÍ.
				÷ ÜÔÏÍ ÒÅÖÉÍÅ ËÏÎÔÒÏÌÌÅÒ ÎÅ ÏÔËÌÉËÁÅÔÓÑ ÎÁ ÏÂÒÁÝÅÎÉÑ × Ó×ÏÅ 
				ËÏÎÆÉÇÕÒÁÃÉÏÎÎÏÅ ÐÒÏÓÔÒÁÎÓÔ×Ï 						*/
/* PI_pin3 - PI_pin0	- ïÐÒÅÄÅÌÑÀÔ ÎÏÍÅÒ ÎÏÖËÉ(ÅË) PCI ÐÒÅÒÙ×ÁÎÉÑ					*/
/* MV_NMI_en		- òÁÚÒÅÛÁÅÔ ×ÙÓÔÁ×ÌÅÎÉÅ NMI ÐÒÅÒÙ×ÁÎÉÑ ÐÒÉ ÓÂÒÏÓÅ ÂÉÔÁ MV 			*/
/* SV_NMI_en		- òÁÚÒÅÛÁÅÔ ×ÙÓÔÁ×ÌÅÎÉÅ NMI ÐÒÅÒÙ×ÁÎÉÑ ÐÒÉ ÓÂÒÏÓÅ ÂÉÔÁ SV 			*/
/* Err_NMI_en		- òÁÚÒÅÛÁÅÔ ×ÙÓÔÁ×ÌÅÎÉÅ NMI ÐÒÅÒÙ×ÁÎÉÑ ÐÒÉ ÎÅÕÓÔÒÁÎÉÍÏÊ ÏÛÉÂËÅ 			*/

/* MV_PI_en		- òÁÚÒÅÛÁÅÔ ×ÙÓÔÁ×ÌÅÎÉÅ PCI ÐÒÅÒÙ×ÁÎÉÑ ÐÒÉ ÓÂÒÏÓÅ ÂÉÔÁ MV 			*/
/* SV_PI_en		- òÁÚÒÅÛÁÅÔ ×ÙÓÔÁ×ÌÅÎÉÅ PCI ÐÒÅÒÙ×ÁÎÉÑ ÐÒÉ ÓÂÒÏÓÅ ÂÉÔÁ SV 			*/
/* Err_PI_en		- òÁÚÒÅÛÁÅÔ ×ÙÓÔÁ×ÌÅÎÉÅ PCI ÐÒÅÒÙ×ÁÎÉÑ ÐÒÉ ÎÅÕÓÔÒÁÎÉÍÏÊ ÏÛÉÂËÅ 			*/

typedef	struct Control_Reg_fields {	/* Structure of PLVC Control Register */
	u32	Rst		: 1;		/* [ 0: 0] 	*/
	u32	Rmode		: 1;		/* [ 1: 1] 	*/
	u32	Hmode		: 1;		/* [ 2: 2] 	*/
	u32	Reserved	: 17;		/* [19: 3] 	*/
	u32	PI_pin0		: 1;		/* [20:20]	*/
	u32	PI_pin1		: 1;		/* [21:21]	*/
	u32	PI_pin2		: 1;		/* [22:22]	*/
	u32	PI_pin3		: 1;		/* [23:23]	*/
	u32	Reserved1	: 1;		/* [24:24] 	*/
	u32	Err_PI_en	: 1;		/* [25:25]	*/
	u32	SV_PI_en	: 1;		/* [26:26]	*/
	u32	MV_PI_en	: 1;		/* [27:27]	*/
	u32	Reserved2	: 1;		/* [28:28]	*/
	u32	Err_NMI_en	: 1;		/* [29:29]	*/
	u32	SV_NMI_en	: 1;		/* [30:30]	*/
	u32	MV_NMI_en	: 1;		/* [31:31]	*/	
} Control_Reg_fields_t;

typedef union Control_Reg {
	Control_Reg_fields_t	fields;
	u32			word;
} Control_Reg_t;
	 

/******				PLVC Status Register				******/

/* NMI 			- âÉÔ ÓÔÁÔÕÓÁ ×ÎÅÛÎÅÇÏ ÐÒÅÒÙ×ÁÎÉÑ. 
			  ðÒÏÐÉÓØ 0 - ÓÎÑÔÉÅ ÐÒÅÒÙ×ÁÎÉÑ,
			  úÁÐÉÓØ 1 - ÇÅÎÅÒÁÃÉÑ ÕÄÁÌÅÎÎÏÇÏ ÐÒÅÒÙ×ÁÎÉÑ (c ÔÅËÕÝÉÍ ÒÅÇÉÓÔÒÏÍ óÔÁÔÕÓÁ
				ÎÉÞÅÇÏ ÎÅ ÐÒÏÉÓÈÏÄÉÔ).
			  ÷ÙÓÔÁ×ÌÑÅÔÓÑ:
				- ÐÒÉ ÓÂÒÁÓÙ×ÁÎÉÉ ÂÉÔÁ MV, ÅÓÌÉ MV_NMI_en ÕÓÔÁÎÏ×ÌÅÎ × 1 		
				- ÐÒÉ ÓÂÒÁÓÙ×ÁÎÉÉ ÂÉÔÁ SV, ÅÓÌÉ SV_NMI_en ÕÓÔÁÎÏ×ÌÅÎ × 1
				- ÐÒÉ ×ÏÚÎÉËÎÏ×ÅÎÉÉ ÎÅÕÓÔÒÁÎÉÍÏÊ ÏÛÉÂËÉ, ÅÓÌÉ Err_NMI_en ÕÓÔÁÎÏ×ÌÅÎ × 1
				- ÐÒÉ ÕÄÁÌÅÎÎÏÍ ×ÙÓÔÁ×ÌÅÎÉÉ NMI ÐÒÅÒÙ×ÁÎÉÑ 				*/

/* PI 			- âÉÔ ÓÔÁÔÕÓÁ PI ÐÒÅÒÙ×ÁÎÉÑ. 
			  ðÒÏÐÉÓØ 0 - ÓÎÑÔÉÅ ÐÒÅÒÙ×ÁÎÉÑ,
			  úÁÐÉÓØ 1 - ÇÅÎÅÒÁÃÉÑ ÕÄÁÌÅÎÎÏÇÏ ÐÒÅÒÙ×ÁÎÉÑ (c ÔÅËÕÝÉÍ ÒÅÇÉÓÔÒÏÍ óÔÁÔÕÓÁ
				ÎÉÞÅÇÏ ÎÅ ÐÒÏÉÓÈÏÄÉÔ).
			  ÷ÙÓÔÁ×ÌÑÅÔÓÑ:
				- ÐÒÉ ÓÂÒÁÓÙ×ÁÎÉÉ ÂÉÔÁ MV, ÅÓÌÉ MV_PI_en ÕÓÔÁÎÏ×ÌÅÎ × 1 		
				- ÐÒÉ ÓÂÒÁÓÙ×ÁÎÉÉ ÂÉÔÁ SV, ÅÓÌÉ SV_PI_en ÕÓÔÁÎÏ×ÌÅÎ × 1
				- ÐÒÉ ×ÏÚÎÉËÎÏ×ÅÎÉÉ ÎÅÕÓÔÒÁÎÉÍÏÊ ÏÛÉÂËÉ, ÅÓÌÉ Err_PI_en ÕÓÔÁÎÏ×ÌÅÎ × 1
				- ÐÒÉ ÕÄÁÌÅÎÎÏÍ ×ÙÓÔÁ×ÌÅÎÉÉ PI ÐÒÅÒÙ×ÁÎÉÑ 				*/

/* Err			- âÉÔ ÓÔÁÔÕÓÁ ÎÅÕÓÔÒÁÎÉÍÏÊ ÏÛÉÂËÉ. ðÒÏÐÉÓØ 0 - ÓÎÑÔÉÅ, ÚÁÐÉÓØ 1 - ÎÅ ÉÍÅÅÔ ÜÆÆÅËÔÁ */

/* NMI_Src_MV 		- ôÏÌØËÏ ÞÔÅÎÉÅ, ÓÂÒÏÓ ÂÉÔÁ MV - ÉÓÔÏÞÎÉË NMI ÐÒÅÒÙ×ÁÎÉÑ.  */
/* NMI_Src_SV 		- ôÏÌØËÏ ÞÔÅÎÉÅ, ÓÂÒÏÓ ÂÉÔÁ SV - ÉÓÔÏÞÎÉË NMI ÐÒÅÒÙ×ÁÎÉÑ.  */
/* NMI_Src_Err 		- ôÏÌØËÏ ÞÔÅÎÉÅ, ÎÅÕÓÔÒÁÎÉÍÁÑ ÏÛÉÂËÁ - ÉÓÔÏÞÎÉË NMI ÐÒÅÒÙ×ÁÎÉÑ.  */
/* NMI_Src_Rm 		- ôÏÌØËÏ ÞÔÅÎÉÅ, ÕÄÁÌÅÎÎÏÅ ×ÙÓÔÁ×ÌÅÎÉÅ - ÉÓÔÏÞÎÉË NMI ÐÒÅÒÙ×ÁÎÉÑ.  */

/* PI_Src_MV 		- ôÏÌØËÏ ÞÔÅÎÉÅ, ÓÂÒÏÓ ÂÉÔÁ MV - ÉÓÔÏÞÎÉË PI ÐÒÅÒÙ×ÁÎÉÑ.  */
/* PI_Src_SV 		- ôÏÌØËÏ ÞÔÅÎÉÅ, ÓÂÒÏÓ ÂÉÔÁ SV - ÉÓÔÏÞÎÉË PI ÐÒÅÒÙ×ÁÎÉÑ.  */
/* PI_Src_Err 		- ôÏÌØËÏ ÞÔÅÎÉÅ, ÎÅÕÓÔÒÁÎÉÍÁÑ ÏÛÉÂËÁ - ÉÓÔÏÞÎÉË PI ÐÒÅÒÙ×ÁÎÉÑ.  */
/* PI_Src_Rm 		- ôÏÌØËÏ ÞÔÅÎÉÅ, ÕÄÁÌÅÎÎÏÅ ×ÙÓÔÁ×ÌÅÎÉÅ - ÉÓÔÏÞÎÉË PI ÐÒÅÒÙ×ÁÎÉÑ.  */

typedef	struct Status_Reg_fields {	/* Structure of PLVC Status Register */
	u32	NMI		: 1;		/* [ 0: 0]	*/
	u32	PI		: 1;		/* [ 1: 1]	*/
	u32	Err		: 1;		/* [ 2: 2]	*/
	u32	Reserved	: 21;		/* [23: 3]	*/
	u32	PI_Src_Rm	: 1;		/* [24:24]	*/
	u32	PI_Src_Err	: 1;		/* [25:25]	*/
	u32	PI_Src_SV	: 1;		/* [26:26]	*/
	u32	PI_Src_MV	: 1;		/* [27:27]	*/
	u32	NMI_Src_Rm	: 1;		/* [28:28]	*/
	u32	NMI_Src_Err	: 1;		/* [29:29]	*/
	u32	NMI_Src_SV	: 1;		/* [30:30]	*/
	u32	NMI_Src_MV	: 1;		/* [31:31]	*/
} Status_Reg_fields_t;

typedef union Status_Reg {
	Status_Reg_fields_t	fields;
	u32			word;
} Status_Reg_t;


/******				Master Control Register				******/	

/* MV 			- ðÒÏÐÉÓØ 1 × ÜÔÏ ÐÏÌÅ ÚÁÐÕÓËÁÅÔ PCI Master. óÂÒÁÓÙ×ÁÅÔÓÑ 
			  Á×ÔÏÍÁÔÉÞÅÓËÉ ÐÏ ÚÁ×ÅÒÛÅÎÉÉ ÚÁÄÁÞÉ					*/

/* C_MRB		- ïÞÉÓÔËÁ Master Recieve Buffer ÐÒÉ ÚÁÐÉÓÉ 1, ÚÁÐÉÓØ 0 ÎÅ ÉÍÅÅÔ ÜÆÆÅËÔÁ		*/

/* C_MTB		- ïÞÉÓÔËÁ Master Transmit Buffer ÐÒÉ ÚÁÐÉÓÉ 1, ÚÁÐÉÓØ 0 ÎÅ ÉÍÅÅÔ ÜÆÆÅËÔÁ	*/

/* MCmd			- ëÏÍÁÎÄÁ, Ó ËÏÔÏÒÏÊ PCI Master ×ÙÈÏÄÉÔ ÎÁ ÛÉÎÕ 				*/

/* MSize		- òÁÚÍÅÒ ÐÒÉÅÍÁ/ÐÅÒÅÄÁÞÉ × DW (4 ÂÁÊÔÁ)						*/

typedef	struct Master_Control_Reg_fields {	/* Structure of PLVC Status Register */
	u32	MV		: 1;		/* [ 0: 0]	*/
	u32	C_MRB		: 1;		/* [ 1: 1]	*/
	u32	C_MTB		: 1;		/* [ 2: 2]	*/
	u32	Reserved	: 5;		/* [ 7: 3]	*/
	u32	MCmd		: 4;		/* [11: 8]	*/
	u32	Reserved1	: 4;		/* [15:12]	*/
	u32	MSize		: 16;		/* [31:16]	*/
} Master_Control_Reg_fields_t;

typedef union Master_Control_Reg {
	Master_Control_Reg_fields_t	fields;
	u32				word;
} Master_Control_Reg_t;

/******				Master Address Register				******/

/* MAddress		- áÄÒÅÓ, Ó ËÏÔÏÒÙÍ PCI Master ×ÙÈÏÄÉÔ ÎÁ ÛÉÎÕ 					*/

typedef struct Master_Address_Reg_fields {
	u32		MAddress;
} Master_Address_Reg_fields_t;

typedef union Master_Address_Reg {
	Master_Address_Reg_fields_t	fields;
	u32				word;
} Master_Address_Reg_t;

/******				Slave Control Register				******/	

/* SV 			- ðÒÏÐÉÓØ 1 × ÜÔÏ ÐÏÌÅ ÚÁÐÕÓËÁÅÔ PCI Slave. óÂÒÁÓÙ×ÁÅÔÓÑ 
			  Á×ÔÏÍÁÔÉÞÅÓËÉ ÐÏ ÚÁ×ÅÒÛÅÎÉÉ ÚÁÄÁÞÉ.
			  ðÒÏÐÉÓØ 0 × ÜÔÏ ÐÏÌÅ ÓÂÒÁÓÙ×ÁÅÔ ÚÁÄÁÞÕ.					*/

/* C_SRB		- ïÞÉÓÔËÁ Slave Recieve Buffer ÐÒÉ ÚÁÐÉÓÉ 1, ÚÁÐÉÓØ 0 ÎÅ ÉÍÅÅÔ ÜÆÆÅËÔÁ		*/

/* C_STB		- ïÞÉÓÔËÁ Slave Transmit Buffer ÐÒÉ ÚÁÐÉÓÉ 1, ÚÁÐÉÓØ 0 ÎÅ ÉÍÅÅÔ ÜÆÆÅËÔÁ	*/

/* SDir			- ïÐÒÅÄÅÌÑÅÔ ÎÁÐÒÁ×ÌÅÎÉÅ ÐÅÒÅÄÁÞÉ
				1 - ÐÅÒÅÄÁÞÁ ÄÁÎÎÙÈ ÐÏ ÞÔÅÎÉÀ ÎÁ PCI
				0 - ÐÅÒÅÄÁÞÁ ÄÁÎÎÙÈ ÐÏ ÚÁÐÉÓÉ ÎÁ PCI					*/

/* SSize		- òÁÚÍÅÒ ÐÒÉÅÍÁ/ÐÅÒÅÄÁÞÉ × DW (4 ÂÁÊÔÁ)						*/

typedef	struct Slave_Control_Reg_fields {	/* Structure of PLVC Status Register */
	u32	SV		: 1;		/* [ 0: 0]	*/
	u32	C_SRB		: 1;		/* [ 1: 1]	*/
	u32	C_STB		: 1;		/* [ 2: 2]	*/
	u32	Reserved	: 5;		/* [ 7: 3]	*/
	u32	SDir		: 1;		/* [ 8: 8]	*/
	u32	Reserved1	: 7;		/* [15:	9]	*/
	u32	SSize		: 16;		/* [31:16]	*/
} Slave_Control_Reg_fields_t;

typedef union Slave_Control_Reg {
	Slave_Control_Reg_fields_t	fields;
	u32				word;
} Slave_Control_Reg_t;

/******				Slave Data Register				******/

/* SData		- òÁÓÐÏÌÏÖÅÎÉÅ ÒÅÇÉÓÔÒÁ ÏÐÒÅÄÅÌÑÅÔ ÁÄÒÅÓ ÄÌÑ Slave ÄÏÓÔÕÐÁ Ë ÄÁÎÎÙÍ		*/

typedef struct Slave_Data_Reg_fields {
	u32		SData;
} Slave_Data_Reg_fields_t;

typedef union Slave_Data_Reg {
	Slave_Data_Reg_fields_t fields;
	u32	word;
} Slave_Data_Reg_t;

/******				Slave Recieve Buffer Count Register				******/

/* SRBC		- þÉÓÌÏ DW × Slave Recieve Buffer. äÏÓÔÕÁÅÎ ÔÏÌØËÏ ÐÏ ÞÔÅÎÉÀ		*/

typedef struct Slave_RBC_Reg_fields {
	u32		SRBC;
} Slave_RBC_Reg_fields_t;

typedef union Slave_RBC_Reg {
	Slave_RBC_Reg_fields_t fields;
	u32	word;
} Slave_RBC_Reg_t;

/******				Slave Transmit Buffer Count Register				******/

/* STBC		- þÉÓÌÏ DW × Slave Transmit Buffer. äÏÓÔÕÁÅÎ ÔÏÌØËÏ ÐÏ ÞÔÅÎÉÀ		*/

typedef struct Slave_TBC_Reg_fields {
	u32		STBC;
} Slave_TBC_Reg_fields_t;

typedef union Slave_TBC_Reg {
	Slave_TBC_Reg_fields_t fields;
	u32	word;
} Slave_TBC_Reg_t;

/******				Master Recieve Buffer Count Register				******/

/* MRBC		- þÉÓÌÏ DW × Master Recieve Buffer. äÏÓÔÕÁÅÎ ÔÏÌØËÏ ÐÏ ÞÔÅÎÉÀ		*/

typedef struct Master_RBC_Reg_fields {
	u32		MRBC;
} Master_RBC_Reg_fields_t;

typedef union Master_RBC_Reg {
	Master_RBC_Reg_fields_t fields;
	u32	word;
} Master_RBC_Reg_t;

/******				Master Transmit Buffer Count Register				******/

/* MTBC		- þÉÓÌÏ DW × Master Transmit Buffer. äÏÓÔÕÁÅÎ ÔÏÌØËÏ ÐÏ ÞÔÅÎÉÀ		*/

typedef struct Master_TBC_Reg_fields {
	u32		MTBC;
} Master_TBC_Reg_fields_t;

typedef union Master_TBC_Reg {
	Master_TBC_Reg_fields_t fields;
	u32	word;
} Master_TBC_Reg_t;

#if 0
#define CONTROL_REGISTER 	1
#define STATUS_REGISTER 	2
#define MASTER_CONTROL_REGISTER	3
#define MASTER_ADDRESS_REGISTER 4
#define SLAVE_CONTROL_REGISTER	5
#define SLAVE_DATA_REGISTER	6
#define SRBC_REGISTER		7
#define STBC_REGISTER		8
#define MRBC_REGISTER		9
#define MTBC_REGISTER		10

#else
#define CONTROL_REGISTER 	0
#define STATUS_REGISTER 	1
#define MASTER_CONTROL_REGISTER	2
#define MASTER_ADDRESS_REGISTER 3
#define SLAVE_CONTROL_REGISTER	4
#define SLAVE_DATA_REGISTER	5
#define SRBC_REGISTER		6
#define STBC_REGISTER		7
#define MRBC_REGISTER		8
#define MTBC_REGISTER		9
#endif

