/*
 *
 * Ported in Linux by Alexey V. Sitnikov, alexmipt@mcst.ru, MCST, 2004
 *
 */

#ifndef	_LINUX_MMR_H__
#define	_LINUX_MMR_H__

#include <linux/types.h>
#include <linux/errno.h>
#include <linux/time.h>
#include <linux/slab.h>

#include <linux/mcst/linux_mmr_io.h>

#ifndef cv_destroy
#define cv_destroy(arg)
#endif

//void debug_mmr(const char *fmt,...){}
//#define debug_mmr	delay_printk
#ifndef MMR_OLD_VERSION
/* ÷ÅÒÓÉÑ ÄÒÁÊ×ÅÒÁ ÷ë */
#define	VER_DRV_VK_MMR		0x17040504
#define	work_var_drv_vk		16

#define	MMR_MAX_SIZE_BUFFER_DMA	0x40000 /* íÁËÓ. ÒÁÚÍÅÒ ÂÕÆÅÒÁ ððä */
#endif /* MMR_OLD_VERSION */

/* óÐÉÓÏË ÐÒÉÞÉÎ ÐÒÅÒÙ×ÁÎÉÊ */
typedef enum   intr_rsn
{
	undefined_intr_reason   = 0, /* ÎÅÏÐÒÅÄÅÌÅÎÎÁÑ ÐÒÉÞÉÎÁ ÐÒÅÒÙ×ÁÎÉÑ */
	reject_intr_reason      = 1, /* ÐÒÅÒÙ×ÁÎÉÅ ÏÔËÌÏÎÑÅÔÓÑ */
	get_intr_reason_mmr     = 2, /* ÐÏÌÕÞÅÎÏ ÐÒÅÒÙ×ÁÎÉÅ ÏÔ ííò */
	board_error_intr_reason = 3  /* ×ÎÕÔÒÅÎÎÑÑ ÏÛÉÂËÁ ÐÌÁÔÙ */
} intr_reason_t;

/* õÐÒÁ×ÌÑÀÝÁÑ ÉÎÆÏÒÍÁÃÉÑ ÂÕÆÅÒÏ× ÄÁÎÎÙÈ */
typedef struct buf_datas_args {
	u_int	USK_TRANS; /* õóë ÐÅÒÅÄÁÔÞÉËÁ */
	u_int	AC0_TRANS; /* áó0 ÐÅÒÅÄÁÔÞÉËÁ */
	u_int	SKB_TRANS; /* óëâ ÐÅÒÅÄÁÔÞÉËÁ */
	u_int	AC1_TRANS; /* áó1 ÐÅÒÅÄÁÔÞÉËÁ */
	u_int	USK_RECIV; /* õóë ÐÒÉÅÍÎÉËÁ */
	u_int	AC0_RECIV; /* áó0 ÐÒÉÅÍÎÉËÁ */
	u_int	SKB_RECIV; /* óëâ ÐÒÉÅÍÎÉËÁ */
	u_int	AC1_RECIV; /* áó1 ÐÒÉÅÍÎÉËÁ */
} buf_datas_args_t;

/* õÐÒÁ×ÌÑÀÝÁÑ ÉÎÆÏÒÍÁÃÉÑ ÂÕÆÅÒÏ× ÄÁÎÎÙÈ */
typedef union ctrl_buf_datas {
	buf_datas_args_t
		init_buf_data[MMR_BUF_ADAPTER_NUM];
/* ÍÁËÓÉÍÁÌØÎÁÑ ÏÂÌÁÓÔØ ÐÁÍÑÔÉ */
	u_int	args_area[MMR_BUF_ADAPTER_NUM*8];
} ctrl_buf_datas_t;


/* õÐÒÁ×ÌÑÀÝÁÑ ÉÎÆÏÒÍÁÃÉÑ ÂÕÆÅÒÁ ËÏÍÁÎÄ */
typedef struct buf_comm_args {
	u_int	USK; /* õóë */
	u_int	AC0; /* áó0 */
	u_int	SKB; /* óëâ */
	u_int	AC1; /* áó1 */
} buf_comm_args_t;

/* âÕÆÅÒÁ ËÏÍÁÎÄ ÁÄÁÐÔÅÒÁ */
typedef struct buffer_data {
	u_int	area_subbuf0[8];
	u_int	area_subbuf1[8];
} buffer_data_t;

/* õÐÒÁ×ÌÑÀÝÁÑ ÉÎÆÏÒÍÁÃÉÑ ÂÕÆÅÒÁ ËÏÍÁÎÄ */
typedef union ctrl_buf_comm {
	buf_comm_args_t
		init_buf_comm;
/* ÍÁËÓÉÍÁÌØÎÁÑ ÏÂÌÁÓÔØ ÐÁÍÑÔÉ ÕÐÒÁ×ÌÑÀÝÅÊ ÉÎÆÏÒÍÁÃÉÉ ÂÕÆÅÒÁ ËÏÍÁÎÄ */
	u_int	args_area[4];
} ctrl_buf_comm_t;

/* ó×ÑÚØ ÄÒÁÊ×ÅÒÁ ÷ë É ÐÌÁÔÙ ííò */
typedef struct drv_comm_memory {
	ctrl_buf_datas_t   ctrl_buf_datas; /* õÐÒÁ×ÌÑÀÝÁÑ ÉÎÆÏÒÍÁÃÉÑ ÂÕÆÅÒÏ× ÄÁÎÎÙÈ */
	buffer_data_t  	   buffer_command; /* éÎÆÏÒÍÁÃÉÑ ÂÕÆÅÒÁ ËÏÍÁÎÄ ÁÄÁÐÔÅÒÁ */
	ctrl_buf_comm_t    ctrl_buf_comm;  /* õÐÒÁ×ÌÑÀÝÁÑ ÉÎÆÏÒÍÁÃÉÑ ÂÕÆÅÒÁ ËÏÍÁÎÄ */
} drv_comm_memory_t;

#define	MMR_DRV_COMM_FREE_TIMEOUT_DEF_VALUE	(1000000)	 /* 1 seconds */

/* ìÏËÁÌØÎÙÅ ÏÐÒÅÄÅÌÅÎÉÑ */

#define MMR_DEVN(d)	(getminor(d))		/* dev_t -> minor (dev_num) */
#define MMR_inst(m)	(m >> 4)		/* minor -> instance */
#define MMR_chan(m)	(m & 0xf)		/* minor -> channel */
#define MMR_MINOR(i,c)	((i << 4) | (c))	/* instance+channel -> minor */
#define MMR_INST(d)	MMR_inst(MMR_DEVN(d))	/* dev_t -> instance */
#define MMR_CHAN(d)	MMR_chan(MMR_DEVN(d))	/* dev_t -> channel */

#define	CHNL_NUM_TO_MASK(chnl)		(1 << chnl)

/* òÁÚÒÑÄÎÙÅ ÐÏÌÑ ÄÌÑ attach_flags: */

#define SOFT_STATE_ALLOCATED		0x0001
#define INTERRUPT_ADDED				0x0002
#define MUTEX_ADDED					0x0004
#define CHANNEL_CV_ADDED			0x0008
#define REGS_MAPPED					0x0010
#define MINOR_NODE_CREATED			0x0020
#define IOPB_ALLOCED				0x0040
#define ERRORS_SIGN					0x0080
#define IBLOCK_COOKIE_ADDED			0x0200
#define	INTR_IBLOCK_COOKIE_ADDED	0x0400
#define	INTR_MUTEX_ADDED			0x0800
#define	TRANS_HALTED_CV_ADDED		0x1000
#define	CNCT_POLLING_CV_ADDED		0x2000
#define	TRANS_STATE_CV_ADDED		0x4000

#ifndef MMR_OLD_VERSION
/* éÍÅÎÁ ÒÅË×ÉÚÉÔÏ× ÕÓÔÒÏÊÓÔ×Á */
#define SBUS_INTR_L_NAME_OF_PROP 	"interrupts"
#endif /* MMR_OLD_VERSION */

/* ïÂÏÂÝÅÎÎÙÅ ÓÔÒÕËÔÕÒÙ ÐÅÒÅÓÙÌÏË É ÒÅÚÕÌØÔÁÔÏ× */

typedef struct dma_struct {
	caddr_t		prim_buf_addr;
	size_t		real_size;
	dma_addr_t	busa;		/* Address in the SBus space,*/ 
					/* áÄÒÅÓ ÏÂÌÁÓÔÉ dma ÓÏ ÓÔÏÒÏÎÙ ÕÓÔÒÏÊÓÔ×Á */
	unsigned long	*mem; 	/* Address in the processor space,*/
					/*  áÄÒÅÓ ÏÂÌÁÓÔÉ dma ÓÏ ÓÔÏÒÏÎÙ ÐÒÏÃÅÓÓÏÒÁ */
	int				size;
} dma_struct_t;

/* óÔÒÕËÔÕÒÁ ÂÕÆÅÒÁ ÐÅÒÅÓÙÌËÉ */
typedef struct trbuf_desc {
	caddr_t			buf_address;	/* ×ÉÒÔÕÁÌØÎÙÊ ÁÄÒÅÓ ÂÕÆÅÒÁ ÐÅÒÅÓÙÌËÉ */
	size_t			buf_size;	/* ÂÁÊÔÏ×ÙÊ ÒÁÚÍÅÒ ÂÕÆÅÒÁ ÐÅÒÅÓÙÌËÉ */
/*	ddi_acc_handle_t	acc_handle;*/	/* ÂÕÆÅÒ ÏÂÒÁÂÏÔËÉ ÄÏÓÔÕÐÁ */
/*	ddi_dma_handle_t	dma_handle;*/	/* ÂÕÆÅÒ ÏÂÒÁÂÏÔËÉ DMA */
/*	ddi_dma_cookie_t	cookie;	   */	/* ÂÕÆÅÒ DMA ÍÁÒËÅÒÏ× */
/*	uint_t			ccount;	   */	/* ÞÉÓÌÏ ÂÕÆÅÒÏ× DMA ÍÁÒËÅÒÏ× */
	dma_struct_t		dma;		/* âÕÆÅÒ, ÏÐÉÓÙ×ÁÀÝÉÊ DMA */
} trbuf_desc_t;

/* óÔÒÕËÔÕÒÁ ÂÕÆÅÒÁ ÐÅÒÅÓÙÌËÉ.
 âÕÆÅÒ ÓÏÄÅÒÖÉÔ ÂÕÆÅÒÁ ÐÏÌØÚÏ×ÁÔÅÌÅÊ */
typedef struct trbuf_state {
	char		valid_flag;		/* ÄÏÐÕÓÔÉÍÙÊ ÂÕÆÅÒ ÐÅÒÅÓÙÌËÉ */
	trbuf_desc_t	trans_buf_desc; 	/* ÄÅÓËÒÉÐÔÏÒ ÂÕÆÅÒÁ ÐÅÒÅÓÙÌËÉ */
	caddr_t		user_buf_address;	/* ×ÉÒÔÕÁÌØÎÙÊ ÁÄÒÅÓ ÎÁÞÁÌØÎÏÇÏ */
						/* ÂÕÆÅÒÁ ÐÏÌØÚÏ×ÁÔÅÌÑ */
	size_t		user_buf_size;		/* ÂÁÊÔÏ×ÙÊ ÒÁÚÍÅÒ ÂÕÆÅÒÁ ÐÏÌØÚÏ×ÁÔÅÌÑ */
	int		max_user_buf_num;	/* ÍÁËÓ. ÞÉÓÌÏ ÂÕÆÅÒÏ× ÐÏÌØÚÏ×ÁÔÅÌÑ */
						/*  × ÂÕÆÅÒÅ ÄÒÁÊ×ÅÒÁ */
	caddr_t		user_trans_bufs[MMR_BUF_USER_NUM];		/* ÓÐÉÓÏË ÕËÁÚÁÔÅÌÅÊ ÂÕÆÅÒÏ× ÐÅÒÅÓÙÌËÉ */
									/*  ÐÏÌØÚÏ×ÁÔÅÌÑ */
	dma_addr_t	dma_trans_bufs[MMR_BUF_USER_NUM]; 		/* ÓÐÉÓÏË dma ÕËÁÚÁÔÅÌÅÊ ÂÕÆÅÒÏ× ÐÅÒÅÓÙÌËÉ */
									/* ÐÏÌØÚÏ×ÁÔÅÌÑ */
} trbuf_state_t;

/* ÷ÎÕÔÒÅÎÎÅÅ ÓÏÓÔÏÑÎÉÅ ËÁÎÁÌÁ */
typedef struct mmr_chnl_state {

	trbuf_state_t	trans_buf_state;	/* ÓÏÓÔÏÑÎÉÅ ÂÕÆÅÒÁ ÐÅÒÅÓÙÌËÉ */
	char	trans_state_is_init;		/* ÕÓÔÁÎÏ×ËÁ ÓÏÓÔÏÑÎÉÑ ÐÅÒÅÓÙÌËÉ */
	char	state_init_in_progress;		/* ×ÙÐÏÌÎÑÅÔÓÑ ÉÎÉÃÉÁÌÉÚÁÃÉÑ  */
	char	trans_state_is_halt;		/* ÓÏÓÔÏÑÎÉÅ ÐÅÒÅÓÙÌËÉ - ÏÓÔÁÎÏ× */
	char	all_trans_finish;			/* ×ÓÅ ÐÅÒÅÓÙÌËÉ ÚÁ×ÅÒÛÅÎÙ */
	char	init_as_trans_map;			/* ËÁÎÁÌ ÉÎÉÃÉÁÌÉÚÉÒÏ×ÁÎ × ÒÅÖÉÍÅ */
							/* ËÁÒÔÙ ÏÂÍÅÎÁ*/
	mmr_init_iomap_t init_iomap_state_spec;		/* ÓÏÓÔÏÑÎÉÅ ÉÎÉÃÉÁÌÉÚÁÃÉÉ ËÁÒÔÙ */
	size_t	full_data_buf_size;			/* ÐÏÌÎÙÊ ÒÁÚÍÅÒ ÂÕÆÅÒÁ ÄÁÎÎÙÈ */
	size_t	subdev_buf_trans_size;			/* ÒÁÚÍÅÒ ÐÅÒÅÄÁÀÝÅÇÏ ÂÕÆÅÒÁ, */
							/* ×ËÌÀÞÁÑ ÚÁÇÏÌÏ×ÏË */
	size_t	subdev_buf_reciv_size;			/* ÒÁÚÍÅÒ ÐÒÉÅÍÎÏÇÏ ÂÕÆÅÒÁ, */
							/* ×ËÌÀÞÁÑ ÚÁÇÏÌÏ×ÏË */
	int		dma_intr_handled;		/* ÐÒÅÒÙ×ÁÎÉÅ ÏÂÒÁÂÏÔÁÎÏ ×ÅÒÎÏ */
	u_short		trans_num;			/* ÎÏÍÅÒ ÐÁËÅÔÎÏÊ ÐÅÒÅÓÙÌËÉ */
} mmr_chnl_state_t;

/* ÷ÎÕÔÒÅÎÎÅÅ ÓÏÓÔÏÑÎÉÅ ÄÒÁÊ×ÅÒÁ */
typedef struct mmr_state {
	struct of_device	*op;
	dev_t		dev;
	int			dev_type;
	int			inst;			/* ÎÏÍÅÒ ÜËÚÅÍÐÌÑÒÁ */
	int			major;			/* ÍÁÖÏÒ ÜËÚÅÍÐÌÑÒÁ */
	int			irq;			/* ÎÏÍÅÒ ÐÒÅÒÙ×ÁÎÉÑ */
	int			flag_board;		/* ÔÉÐ ÕÓÔÒÏÊÓÔ×Á: ë ÉÌÉ ïõ */
	int			opened;			/* ÏÔËÒÙÔÏÅ ÓÏÓÔÏÑÎÉÅ */
	int			open_flags;		/* ÏÔËÒÙÔÏÅ ÓÏÓÔÏÑÎÉÅ Ó ÆÌÁÖËÏÍ */
	u_int		open_channel_map;	/* ÍÁÓËÁ ÏÔËÒÙÔÙÈ ËÁÎÁÌÏ× */
	/***********************************************/
	raw_spinlock_t	lock;
	kcondvar_t	channel_cv;		/* ÐÅÒÅÍÅÎÎÁÑ ÕÓÌÏ×ÉÊ */
							/* (ÒÅÖÉÍÁ, ÓÏÓÔÏÑÎÉÊ) ËÁÎÁÌÁ */
	kcondvar_t	drv_comm_cv;		/* ÏÂÌÁÓÔØ Ó×ÑÚÉ ÄÒÁÊ×ÅÒÁ: ÚÁÎÑÔÁÑ ÉÌÉ */
							/* Ó×ÏÂÏÄÎÁÑ, ÐÅÒÅÍÅÎÎÁÑ ÕÓÌÏ×ÉÑ */
	kcondvar_t	trans_state_cv; 	/* ÓÏÓÔÏÑÎÉÅ ËÁÎÁÌÁ ÐÅÒÅÓÙÌËÉ, */
							/* ÉÚÍÅÎÅÎÉÅ ÐÅÒÅÍÅÎÎÏÊ ÕÓÌÏ×ÉÑ */
	kcondvar_t	intr_cv;		/* ÐÅÒÅÍÅÎÎÁÑ ÕÓÌÏ×ÉÊ ÄÌÑ ÐÒÅÒÙ×ÁÎÉÑ */
        /***********************************************/
/*	ddi_iblock_cookie_t	iblock_cookie; */	/* ÄÌÑ mutexes. */
/*	struct pollhead		pollhead;      */	/* ÇÌÕÈÁÑ ÓÔÒÕËÔÕÒÁ ÄÌÑ ÏÐÒÏÓÁ */
	int			drv_comm_busy;		/* ÐÒÉÚÎÁË ÚÁÎÑÔÏÓÔÉ ÏÂÌÁÓÔÉ */
							/* Ó×ÑÚÉ ÄÒÁÊ×ÅÒÁ */
#ifdef MMR_OLD_VERSION
	int       	drv_general_modes;		/* ÏÂÝÉÅ ÐÒÉÚÎÁËÉ ÒÅÖÉÍÏ× ÄÒÁÊ×ÅÒÁ */
#endif /* MMR_OLD_VERSION */
	e90_unit_t	type_unit;			/* ÔÉÐ ÐÌÁÔÙ */

	char		intr_seted;			/* ÐÒÅÒÙ×ÁÎÉÅ ÕÓÔÁÎÏ×ÌÅÎÏ */
	char		intr_number;			/* ÞÉÓÌÏ ÐÒÅÒÙ×ÁÎÉÊ */
	ulong_t		io_flags_intr;		  	/* ÐÒÉÚÎÁË ÐÒÅÒÙ×ÁÎÉÑ ðÒð */
	ulong_t		flags_intr_rerr;		/* ÐÒÉÚÎÁË ÐÒÅÒÙ×ÁÎÉÑ ÐÏ òïû */
#ifdef MMR_OLD_VERSION
	ulong		pointer_reciv_comm;		/* ÕËÁÚÁÔÅÌØ ÎÁ ÚÁÐÉÓÁÎÎÕÀ ËÏÍÁÎÄÕ  */
							/* × ÂÕÆÅÒÅ ËÏÍÁÎÄ æð */
#else
	u_int		num_reciv_comm;			/* ËÏÌ-×Ï ÚÁÐÉÓÁÎÎÙÈ ËÏÍÁÎÄ × ÂÕÆÅÒ */
							/* ËÏÍÁÎÄ æð ÐÏ ÉÎÆ. ÁÄÁÐÔÅÒÁ */
#endif /* MMR_OLD_VERSION */
	u_int		cur_num_comm;			/* ËÏÌ-×Ï ÚÁÐÉÓÁÎÎÙÈ ËÏÍÁÎÄ */
#ifndef MMR_OLD_VERSION
	u_int		intr_dev;		    	/* òïâ ÐÒÉ ÐÏÌÕÞÅÎÉÉ ÐÒÅÒÙ×ÁÎÉÑ ÏÔ ÁÄÁÐÔÅÒÁ */
	hrtime_t	time_get_intr_dev;  		/* ô ÐÏÌÕÞÅÎÉÑ ÐÒÅÒÙ×ÁÎÉÑ ÏÔ ÁÄÁÐÔÅÒÁ */
#endif /* MMR_OLD_VERSION */
	mmr_reg_cntrl_t	mmr_reg_cntrl_dev;	 	/* ÓÔÒÕËÔÕÒÁ ÒÅÇÉÓÔÒÁ ÕÐÒÁ×ÌÅÎÉÑ ííò */
	int		system_burst;			/* DMA ÒÁÚÍÅÒÙ ÐÁÞËÉ, ÐÏÚ×ÏÌÅÎÎÙÅ SBUS */

	volatile caddr_t	MMR_BMEM;		/* base memory */

	mmr_chnl_state_t   	channel_state[1];       /*ÓÏÓÔÏÑÎÉÅ ËÁÎÁÌÁ ÐÌÁÔÙ */
/*	ddi_acc_handle_t	acc_regs; */ 		/* ÕËÁÚÁÔÅÌØ ÎÁ ÄÅÓËÒÉÐÔÏÒ ÄÏÓÔÕÐÁ Ë ÒÅÇÉÓÔÒÁÍ */
	caddr_t		regs_base;		/* ÂÁÚÏ×ÙÊ ÁÄÒÅÓ ÒÅÇÉÓÔÒÏ× */
	off_t		reg_array_size;   	/* ÒÁÚÍÅÒ ×ÙÄÅÌÅÎÎÏÊ ÏÂÌÁÓÔÉ */
							/* ÒÅÇÉÓÔÒÏ× */
#ifndef MMR_OLD_VERSION
	u_short		number_intr_rosh; 		/* ËÏÌ-×Ï ÐÒÅÒÙ×ÁÎÉÊ ÐÏ òïû */
#endif /* MMR_OLD_VERSION */	
} mmr_state_t;

/* íÁËÒÏËÏÍÁÎÄÙ ÄÌÑ ÏÂÒÁÝÅÎÉÑ Ë ÒÅÇÉÓÔÒÁÍ */
/* ÷ÉÒÔÕÁÌØÎÙÊ ÁÄÒÅÓ ÒÅÇÉÓÔÒÁ */
#define	MMR_REGISTER_ADDR(state, reg)	((ulong_t *)(state->regs_base + reg))
/* þÔÅÎÉÑ ÓÏÄÅÒÖÉÍÏÇÏ ÒÅÇÉÓÔÒÁ */
#define	READ_MMR_REGISTER(state, reg)	ddi_getl(state->dev_type, MMR_REGISTER_ADDR(state, reg))
/* úÁÐÉÓØ × ÒÅÇÉÓÔÒ */
#define	WRITE_MMR_REGISTER(state, reg, v)	ddi_putl(state->dev_type, MMR_REGISTER_ADDR(state, reg), v)

static long
mmr_ioctl(struct file *file, unsigned int cmd, unsigned long arg);

static int mmr_open(struct inode *inode, struct file *file);

static int mmr_mmap(struct file *file, struct vm_area_struct *vma);

static int mmr_close(struct inode *inode, struct file *file);

#endif	/* _LINUX_MMR_H__ */
