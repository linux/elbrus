#define ERRDMA_NULL_REQUEST		-1
#define ERRDMA_BAD_ABONENT		-2
#define ERRDMA_BAD_VDMA_TM		-3
#define ERRDMA_BAD_FADDR		-4
#define ERRDMA_BIG_FADDR		-5
#define ERRDMA_BAD_SIZE_FS		-6
#define ERRDMA_NULL_BUFFER		-7
#define ERRDMA_IN_INTERRUPT		-8
#define ERRDMA_BAD_STAT			-9  /* ÐÏÐÙÔËÁ ×ÙÐÏÌÎÉÔØ ÏÐÅÒÁÃÉÀ ÏÂÍÅÎÁ, ÎÅ ÄÏÖÄÁ×ÛÉÓØ ÏËÏÎÞÁÎÉÑ ÐÒÅÄÙÄÕÝÅÊ */
#define ERRDMA_SIGNAL			-10 /* ÐÏÔÏË ÚÁ×ÅÒÛÅÎ ÓÉÇÎÁÌÏÍ */
#define ERRDMA_BAD_SPIN			-11 /* ÐÏÐÙÔËÁ ÕÓÎÕÔØ Ó ÏÔËÒÙÔÙÍ ÓÅÍÁÆÏÒÏÍ */
#define ERRDMA_BAD_WAIT1		-12 /* ÏÛÉÂËÁ ÞÉÔÁÔÅÌÑ ÐÒÉ ÐÏÐÙÔËÅ ÕÓÎÕÔØ */
#define ERRDMA_TIMER			-13 /* ÔÁÊÍ-ÁÕÔ ÐÒÉ ÏÐÅÒÁÃÉÉ ÏÂÍÅÎÁ */
#define ERRDMA_BAD_IRQ_COUNT		-14 /* ÐÒÏÐÕÓË ÐÒÅÒÙ×ÁÎÉÑ ÐÒÉ ÐÒÉÅÍÅ */
#define ERRDMA_BAD_INT_AC1		-15 /* ÏÛÉÂËÁ ÄÒÁÊ×ÅÒÁ ÐÒÉ ÐÒÉÅÍÅ ÄÁÎÎÙÈ */
#define ERRDMA_BAD_SIZE			-16 /* ÎÅ×ÅÒÎÙÊ ÒÁÚÍÅÒ ÔÒÁÎÚÁËÃÉÉ */
#define ERRDMA_RD_MAX_SMC		-17
#define ERRDMA_BAD_STAT_MSG		-18 /* ÐÏÐÙÔËÁ ÐÅÒÅÄÁÔØ ÓÏÏÂÝÅÎÉÅ, ÎÅ ÄÏÖÄÁ×ÛÉÓØ ÐÅÒÅÄÁÞÉ ÐÒÅÄÙÄÕÝÅÇÏ */
#define ERRDMA_BAD_SEND_MSG		-19 /* ÏÛÉÂËÁ ÐÒÉ ÐÅÒÅÄÁÞÅ ÓÏÏÂÝÅÎÉÑ */
#define ERRDMA_RD_BAD_WAIT2		-20 /* ÏÛÉÂËÁ × ÆÕÎËÃÉÉ ÐÒÏÂÕÖÄÅÎÉÑ */
#define ERRDMA_RD_MAX_REPEATE		-21 /* ÄÌÉÔÅÌØÎÏÓÔØ ÐÒÉÅÍÁ ÂÏÌØÛÅ ÄÏÐÕÓÔÉÍÏÊ */
#define ERRDMA_RD_BAD_IRQ_COUNT1	-22 /* ÐÒÏÐÕÓË ÐÒÅÒÙ×ÁÎÉÑ ÐÒÉ ÐÒÉÅÍÅ */
#define ERRDMA_RD_MAX_COUNT_RDR_RBC	-23 /* ÄÌÉÔÅÌØÎÏÓÔØ ÐÒÉÅÍÁ ÂÏÌØÛÅ ÄÏÐÕÓÔÉÍÏÊ */
#define ERRDMA_RD_LOSS_RDC_2		-24 /* ÄÌÉÔÅÌØÎÏÓÔØ ÐÒÉÅÍÁ ÂÏÌØÛÅ ÄÏÐÕÓÔÉÍÏÊ */
#define ERRDMA_RD_LOSS_RDC_4		-25 /* ÄÌÉÔÅÌØÎÏÓÔØ ÐÒÉÅÍÁ ÂÏÌØÛÅ ÄÏÐÕÓÔÉÍÏÊ */
#define ERRDMA_RD_BAD_INT_AC2		-26 /* ÏÛÉÂËÁ ÄÒÁÊ×ÅÒÁ ÐÒÉ ÐÒÉÅÍÅ ÄÁÎÎÙÈ */
#define ERRDMA_WR_DSF			-27 /* ÁÐÐÁÒÁÔÎÁÑ ÏÛÉÂËÁ ÐÒÉ ÐÅÒÅÄÁÞÅ */
#define ERRDMA_BAD_CHANNEL		-28 /* ÎÅ×ÅÒÎÙÊ ÎÏÍÅÒ ËÁÎÁÌÁ */
#define ERRDMA_BAD_TIMER		-29 /* ÔÁÊÍ-ÁÕÔ ÐÒÉ ÏÐÅÒÁÃÉÉ ÏÂÍÅÎÁ */
#define ERRDMA_WR_BAD_DMA		-30 /* ÎÅ ÚÁÄÁÎ ÁÄÒÅÓ ÂÕÆÅÒÁ ÐÅÒÅÄÁÞÉ */
#define ERRDMA_GP0_EXIT			-31 /* ×ÙÈÏÄ ÉÚ ÒÉÄÅÒÁ ÐÏ ÐÒÉÈÏÄÕ GP0 */
