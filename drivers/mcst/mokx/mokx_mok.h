#ifndef	__MOKX_MOK_H__
#define	__MOKX_MOK_H__

/*
 * Define for MOK_X
 */
#define MOK_X_INFO_MSG(x...)		printk(x)
#define MOK_X_ERROR_MSG(x...)		printk(x)
#define MOK_X_DBG_MSG(x...)		printk(x)

#define MOK_X_PLD_DBG_MODE		1
#define MOK_X_PLD_DBG_ERROR_MSG(x...)	if (MOK_X_PLD_DBG_MODE) printk(x)

#define MOK_X_MDIO_DBG_MODE		1
#define MOK_X_MDIO_DBG_ERROR_MSG(x...)	if (MOK_X_MDIO_DBG_MODE) printk(x)

typedef	unsigned int mok_x_status_reg_t; /* single word (32 bits) */
typedef	struct mok_x_status_reg_fields {
	/* 0,    òÅÚÅÒ× 0 */
	mok_x_status_reg_t	reserv0			:1;
	/* 1,    òÅÚÅÒ× 1 */
	mok_x_status_reg_t	timeout_msg_receive	:1;
	/* 2,    òÅÚÅÒ× 2 */
	mok_x_status_reg_t	mode4			:1;
	/* 3,rw  òÅÖÉÍ ÒÁÂÏÔÙ MODE3 */
	mok_x_status_reg_t	mode3			:1;
	/* 4,rw  òÅÖÉÍ ÒÁÂÏÔÙ MODE2 */
	mok_x_status_reg_t	mode2			:1;
	/* 5,rw  òÅÖÉÍ ÒÁÂÏÔÙ MODE1 */
	mok_x_status_reg_t	mode1			:1;
	/* 6,rw  in_ready_to_receive - ÕËÁÚÙ×ÁÅÔ ÞÔÏ ÐÒÏÔÉ×ÏÐÏÌÏÖÎÁÑ ÓÔÏÒÏÎÁ */
	/* ÇÏÔÏ×Á ÐÒÉÎÉÍÁÔØ ÄÁÎÎÙÅ        				     */
	mok_x_status_reg_t	in_ready_to_receive	:1;
	/* 7,rw  granted_packet - ÕÓÔÁÎÏ×ËÁ ÜÔÏÇÏ ÂÉÔÁ ÚÁÄÅÊÓÔ×ÕÅÔ ÍÅÈÁÎÉÚÍ */
	/* ÇÁÒÁÎÔÉÒÏ×ÁÎÎÏÊ ÄÏÓÔÁ×ËÉ ×ÓÅÈ ÐÁËÅÔÏ×.			    */
	mok_x_status_reg_t	granted_packet		:1;
	/* 8,rw  granted_last_packet - ÕÓÔÁÎÏ×ËÁ ÜÔÏÇÏ ÂÉÔÁ ÚÁ ÄÅÊÓÔ×ÕÅÔ */
	/* ÍÅÈÁÎÉÚÍ  ÇÁÒÁÎÔÉÒÏ×ÁÎÎÏÊ ÄÏÓÔÁ×ËÉ ÐÏÓÌÅÄÎÅÇÏ ÐÁËÅÔÁ × ÏÂÍÅÎÅ.*/
	mok_x_status_reg_t	granted_last_packet	:1;
	/* 9,rw  ready_to_receive - ÂÉÔ, ÒÁÚÒÅÛÁÀÝÉÊ ÐÒÉÎÉÍÁÔØ ÄÁÎÎÙÅ. åÓÌÉ   */
	/* ÜÔÏÔ ÂÉÔ ÎÅ ÕÓÔÁÎÏ×ÌÅÎ, ÔÏ ×ÓÅ ×ÈÏÄÑÝÉÅ ÐÁËÅÔÙ ÄÁÎÎÙÈ ÓÏÈÒÁÎÑÀÔÓÑ  */
	/* × ÐÒÉ£ÍÎÏÍ ÂÕÆÅÒÅ É ÂÕÆÅÒÅ ÐÅÒÅÄÁÔÞÉËÁ ÎÁ ÐÒÏÔÉ×ÏÐÏÌÏÖÎÏÊ ÓÔÏÒÏÎÅ. */
	mok_x_status_reg_t	ready_to_receive	:1;
	/* 10,rw enable_receive - ÒÁÚÒÅÛÅÎÉÅ ÐÒÉ£ÍÁ ÄÁÎÎÙÈ. åÓÌÉ ÜÔÏÔ ÂÉÔ ÎÅ */
	/* ÕÓÔÁÎÏ×ÌÅÎ, ÔÏ ×ÓÅ ×ÈÏÄÑÝÉÅ ÐÁËÅÔÙ ÄÁÎÎÙÈ ÉÇÎÏÒÉÒÕÀÔÓÑ.           */
	mok_x_status_reg_t	receive_enable		:1;
	/* 11,rw enable_transmit - ÒÁÚÒÅÛÅÎÉÅ ÐÅÒÅÄÁÞÉ ÄÁÎÎÙÈ. */
	mok_x_status_reg_t	transmit_enable		:1;
	/* 12,rw slave - ÂÉÔ, ÕËÁÚÙ×ÁÀÝÉÊ ÎÁ ÔÏ ÞÔÏ ÜÔÁ ÓÔÏÒÏÎÁ ×ÅÄÏÍÁÑ. */
	mok_x_status_reg_t	slave			:1;
	/* 13,rw master - ÂÉÔ, ÕËÁÚÙ×ÁÀÝÉÊ ÎÁ ÔÏ ÞÔÏ ÜÔÁ ÓÔÏÒÏÎÁ ×ÅÄÕÝÁÑ. */
	mok_x_status_reg_t	master			:1;
	/* 14,r  enable - ÉÎÆÏÒÍÁÃÉÏÎÎÙÊ ÂÉÔ. õËÁÚÙ×ÁÅÔ ÎÁ ÔÏ, ÞÔÏ ÕÓÔÒÏÊÓÔ×Ï */
	/* ÍÏÖÅÔ ÂÙÔØ ÚÁÄÅÊÓÔ×Ï×ÁÎÏ ÄÌÑ ÐÒÉ£ÍÁ/ÐÅÒÅÄÁÞÉ ÄÁÎÎÙÈ. */
	mok_x_status_reg_t	enable			:1;
	/* 15,r  link - ÉÄÉËÁÔÏÒ ÓÏÅÄÉÎÅÎÉÑ. */
	mok_x_status_reg_t	link			:1;
	mok_x_status_reg_t	unused17		:1;
	mok_x_status_reg_t	unused16		:1;
	mok_x_status_reg_t	unused18		:1;
	mok_x_status_reg_t	unused19		:1;
	mok_x_status_reg_t	unused20		:1;
	mok_x_status_reg_t	unused21		:1;
	mok_x_status_reg_t	unused22		:1;
	mok_x_status_reg_t	unused23		:1;
	mok_x_status_reg_t	unused24		:1;
	mok_x_status_reg_t	unused25		:1;
	mok_x_status_reg_t	unused26		:1;
	mok_x_status_reg_t	unused27		:1;
	mok_x_status_reg_t	unused28		:1;
	mok_x_status_reg_t	unused29		:1;
	mok_x_status_reg_t	unused30		:1;
	mok_x_status_reg_t	unused31		:1;
} mok_x_status_reg_fields_t;

typedef	union mok_x_status_reg_struct {		/* Structure of word */
	mok_x_status_reg_fields_t	fields;	/* as fields */
	mok_x_status_reg_t		word;	/* as entire register */
} mok_x_status_reg_struct_t;

#endif  /*__MOKX_MOK_H__*/
