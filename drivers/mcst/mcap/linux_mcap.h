
/* òÅÄÁËÃÉÑ ÆÁÊÌÁ mcap.h:
					éí÷ó - 10.02.05; home - 22.04.04 */

#ifndef	__MCAP_H__
#define	__MCAP_H__
 
#include <linux/mcst/linux_mcap_io.h>

#ifdef	__cplusplus
extern "C" {
#endif

/* ÷ÅÒÓÉÑ ÄÒÁÊ×ÅÒÁ ÷ë */
#ifdef MCAP_OLD_VERSION
#define	VER_DRV_VK_MC19		0x04030507
#define	work_var_drv_vk			13
#else
#define VER_DRV_VK_MCAP         0x14010608
#define work_var_drv_vk                 18
#endif /* MCAP_OLD_VERSION */

#define LOAD		2
#define HALT		1
#define BOOT		0

#ifndef cv_destroy
#define cv_destroy(arg)
#endif

int	debug_mcap = 1;
/* õÐÒÁ×ÌÅÎÉÅ ×ÙÄÁÞÅÊ ÓÏÏÂÝÅÎÉÊ ÏÔÌÁÄËÉ:
	 if (debug_mcap == 0) {
		<ÓÏÏÂÝÅÎÉÅ ÎÅ ×ÙÄÁÇÔÓÑ>
	 };
	 if (debug_mcap == 1) {
		<ÓÏÏÂÝÅÎÉÅ ×ÙÄÁÇÔÓÑ>
	 };
*/

#ifndef MCAP_OLD_VERSION
#define MCAP_MP_ROM_DRV_INIT_ADDR               0x00400
#define MCAP_MAX_SIZE_BUFFER_DMA                0x40000 /* íÁËÓ. ÒÁÚÍÅÒ ÂÕÆÅÒÁ ððä */
#define MCAP_MP_INIT_AREA_BMEM_SIZE     	0x00010 /* size of MP init area */
#define MCAP_MP_HALT_OPCODE                     0xf4f4f4f4UL
#define MCAP_MP_ROM_DRV_INIT_CODE       	{0xfa33c08eUL, 0xc0c60604UL, \
                                                 0x0401f4f4UL, 1}
                                                 /* CLI; _ES = 0; mov byte ptr  */
                                                 /* ES:[404], 1 */

typedef union mcap_mp_init_area_t_
{
   char         as_chars  [MCAP_MP_INIT_AREA_BMEM_SIZE];
   u_char       as_u_chars[MCAP_MP_INIT_AREA_BMEM_SIZE];
   int  	as_longs  [MCAP_MP_INIT_AREA_BMEM_SIZE / sizeof(int)];
   u_int        as_u_longs[MCAP_MP_INIT_AREA_BMEM_SIZE / sizeof(u_int)];
} mcap_mp_init_area_t;

#define  MCAP_MP_INIT_AREA_char         as_chars
#define  MCAP_MP_INIT_AREA_u_char       as_u_chars
#define  MCAP_MP_INIT_AREA_long         as_longs
#define  MCAP_MP_INIT_AREA_u_long       as_u_longs

typedef struct mcap_mp_rom_drv {
        int     debug_drv_start;        /* ÎÅ ÉÓÐÏÌØÚÕÅÔÓÑ */
        int     rom_disable;            /* ÐÒÉÚÎÁË ÚÁÇÒÕÚËÉ 1 - ðúõ, 0 - ïúõ */
} mcap_mp_rom_drv_t;

#endif /* MCAP_OLD_VERSION */

/* óÔÒÕËÔÕÒÙ ÍÅÖÄÒÁÊ×ÅÒÎÏÊ Ó×ÑÚÉ */
/* óÐÉÓÏË ÎÏÍÅÒÏ× ÚÁÄÁÎÉÊ ÄÌÑ ÄÒÁÊ×ÅÒÁ íð */
typedef enum _mp_task_t
{
	no_mp_task                      = 0, 		/* Mð ÖÄÅÔ ÚÁÄÁÎÉÅ */
	init_driver_mp_task             = 1, 		/* ÉÎÉÃÉÁÌÉÚÁÃÉÉ ÄÒÁÊ×ÅÒÁ Mð */
	init_buffers_data_exchange_task = 2, 		/* ÉÎÉÃÉÁÌÉÚÁÃÉÉ ÂÕÆÅÒÏ× ÏÂÍÅÎÁ ÄÁÎÎÙÍÉ */
	mcap_halt_channel_data_exchange_task = 9, 	/* ÏÓÔÁÎÏ× ËÁÎÁÌÏ× ÏÂÍÅÎÁ ÄÁÎÎÙÍÉ */
	mcap_turn_off_channels_task = 10  		/* ÏÔËÌÀÞÅÎÉÅ ËÁÎÁÌÏ× ÏÔ ÌÉÎÉÊ Ó×ÑÚÉ */
} mp_task_t;

/* óÐÉÓÏË ÎÏÍÅÒÏ× ÚÁÄÁÎÉÊ ÄÌÑ ÄÒÁÊ×ÅÒÁ ÷ë */
typedef enum _sparc_task_t
{
	no_sparc_task              = 0  /* ÄÒÁÊ×ÅÒ ÷ë ÖÄÅÔ ÚÁÄÁÎÉÅ */
} sparc_task_t;

/* úÁÄÁÎÉÊ ÎÁ ÐÒÅÒÙ×ÁÎÉÅ ÄÒÁÊ×ÅÒÁ ÷ë */
typedef enum _intr_task_t
{
	no_intr_task         = 0,  /* ÄÒÁÊ×ÅÒ ÷ë ÖÄÅÔ ÚÁÄÁÎÉÅ */
	mcap_get_intr_driver = 12  /* ×ÙÄÁÞÁ ÐÒÅÒÙ×ÁÎÉÑ ÄÒÁÊ×ÅÒÕ ÷ë */
} intr_task_t;

/* óÐÉÓÏË ÓÏÓÔÏÑÎÉÊ íð */
typedef enum	mp_state_t_
{
	undef_mp_state,					/* ÎÅÏÐÒÅÄÅÌÅÎÎÏÅ ÓÏÓÔÏÑÎÉÅ íð */
	halted_mp_state,				/* íð ÎÁÈÏÄÉÔÓÑ × ÏÓÔÁÎÏ×ÌÅÎÎÏÍ ÓÏÓÔÏÑÎÉÉ */
	started_mp_state,				/* íð ÂÙÌ ÚÁÐÕÝÅÎ É ÆÕÎËÃÉÏÎÉÒÕÅÔ */
	hangup_mp_state,				/* ÚÁ×ÉÓÁÎÉÅ íð */
 	crash_mp_state,					/* Á×ÁÒÉÊÎÙÊ ÏÔËÁÚ íð */
	fault_mp_state,					/* ×ÎÕÔÒÅÎÎÑÑ ÎÅÉÓÐÒÁ×ÎÏÓÔØ ÐÌÁÔÙ ÉÌÉ íð */
	adapter_abend_mp_state,				/* Á×ÁÒÉÊÎÏÅ ÐÒÅËÒÁÝÅÎÉÅ ÒÁÂÏÔÙ ÁÄÁÐÔÅÒÁ */
	locked_mp_state,				/* íð ÎÁÈÏÄÉÔÓÑ × ÂÌÏËÉÒÏ×ÁÎÎÏÍ ÓÏÓÔÏÑÎÉÉ */
	restarted_mp_state				/* íð ÐÅÒÅÚÁÐÕÝÅÎ */
} mp_state_t;

/* óÐÉÓÏË ÐÒÉÞÉÎ ÐÒÅÒÙ×ÁÎÉÊ */
typedef enum   intr_rsn
{
	undefined_intr_reason        = 0, /* ÎÅÏÐÒÅÄÅÌÅÎÎÁÑ ÐÒÉÞÉÎÁ */
	reject_intr_reason           = 1, /* ÐÒÅÒÙ×ÁÎÉÑ ÎÅ ÖÄÕÔ */
	board_error_intr_reason      = 2, /* ×ÎÕÔÒÅÎÎÑÑ ÏÛÉÂËÁ ÐÌÁÔÙ */
	get_intr_driver_reason       = 12 /* ÐÏÌÕÞÅÎÏ ÐÒÅÒÙ×ÁÎÉÅ ÏÔ ÄÒ-ÒÁ íð */
} intr_reason_t;

/* òÅÚÕÌØÔÁÔÙ ÉÎÉÃÉÁÌÉÚÁÃÉÉ ÄÒÁÊ×ÅÒÁ Mð */
#ifdef MY_DRIVER_BIG_ENDIAN
typedef struct _mp_init_result_t
{
	u_short		mp_error_code;		/* ËÏÄ ÏÛÉÂËÉ ÉÎÉÃÉÁÌÉÚÁÃÉÉ ÄÒÁÊ×ÅÒÁ Mð */
	u_short		unused;
} mp_init_result_t;
#else
typedef struct _mp_init_result_t
{
	u_short		unused;
	u_short		mp_error_code;		/* ËÏÄ ÏÛÉÂËÉ ÉÎÉÃÉÁÌÉÚÁÃÉÉ ÄÒÁÊ×ÅÒÁ Mð */
} mp_init_result_t;
#endif /* MY_DRIVER_BIG_ENDIAN */

/* éÎÉÃÉÁÌÉÚÁÃÉÉ ÂÕÆÅÒÏ× ÏÂÍÅÎÁ ÄÁÎÎÙÍÉ */
#ifdef MY_DRIVER_BIG_ENDIAN
typedef struct init_bufers_exchange_data
{
	u_short		num_buf_user;		/* ËÏÌÉÞÅÓÔ×Ï ÂÕÆÅÒÏ× ÏÂÍÅÎÁ ÐÏÌØÚÏ×ÁÔÅÌÑ */
	u_short		max_size_buf_trans;	/* ÍÁËÓÉÍÁÌØÎÙÊ ÒÁÚÍÅÒ ÐÅÒÅÄÁÀÝÅÇÏ */
						/* ÂÕÆÅÒÁ ÏÂÍÅÎÁ ÐÏÌØÚÏ×ÁÔÅÌÑ (ÂÁÊÔ) */
	u_int		dma_trans_bufs[MCAP_SUBDEV_BUF_NUM];	/* ÕËÁÚÁÔÅÌÉ ÓÐÉÓËÁ ÐÏÌØÚÏ×ÁÔÅÌØËÉÈ */
								/* ÂÕÆÅÒÏ× ÏÂÍÅÎÁ ÄÁÎÎÙÍÉ */
} init_bufers_exchange_data_t;
#else
typedef struct init_bufers_exchange_data
{
	u_short		max_size_buf_trans;	/* ÍÁËÓÉÍÁÌØÎÙÊ ÒÁÚÍÅÒ ÐÅÒÅÄÁÀÝÅÇÏ */
						/* ÂÕÆÅÒÁ ÏÂÍÅÎÁ ÐÏÌØÚÏ×ÁÔÅÌÑ (ÂÁÊÔ) */
	u_short		num_buf_user;		/* ËÏÌÉÞÅÓÔ×Ï ÂÕÆÅÒÏ× ÏÂÍÅÎÁ ÐÏÌØÚÏ×ÁÔÅÌÑ */
	u_int		dma_trans_bufs[MCAP_SUBDEV_BUF_NUM];	/* ÕËÁÚÁÔÅÌÉ ÓÐÉÓËÁ ÐÏÌØÚÏ×ÁÔÅÌØËÉÈ */
								/* ÂÕÆÅÒÏ× ÏÂÍÅÎÁ ÄÁÎÎÙÍÉ */
} init_bufers_exchange_data_t;
#endif /* MY_DRIVER_BIG_ENDIAN */

/* òÅÚÕÌØÔÁÔÙ ÉÎÉÃÉÁÌÉÚÁÃÉÉ ÂÕÆÅÒÏ× ÏÂÍÅÎÁ ÄÁÎÎÙÍÉ */
#ifdef MY_DRIVER_BIG_ENDIAN
typedef struct init_bufers_exchange_data_res
{
	u_short		error_init_bufers;	/* ËÏÄ ÏÛÉÂËÉ, ÏÂÎÁÒÕÖÅÎÎÏÊ ÄÒÁÊ×ÅÒÏÍ Mð */
	u_short		unused;			/* ÎÅÉÓÐÏÌØÚÕÅÍÏÅ ÐÏÌÅ */
} init_bufers_exchange_data_res_t;
#else
typedef struct init_bufers_exchange_data_res
{
	u_short		unused;			/* ÎÅÉÓÐÏÌØÚÕÅÍÏÅ ÐÏÌÅ */
	u_short		error_init_bufers;	/* ËÏÄ ÏÛÉÂËÉ, ÏÂÎÁÒÕÖÅÎÎÏÊ ÄÒÁÊ×ÅÒÏÍ Mð */
} init_bufers_exchange_data_res_t;
#endif /* MY_DRIVER_BIG_ENDIAN */

/* ïÓÔÁÎÏ×ÉÔØ ËÁÎÁÌ ÏÂÍÅÎÁ ÄÁÎÎÙÍÉ (mcap_halt_channel_data_exchange_task) */
#ifdef MY_DRIVER_BIG_ENDIAN
typedef struct	halt_channel_data_exche {
	short	halt_channel_exchange; 	/* ÎÏÍÅÒ ÏÓÔÁÎÁ×ÌÉ×ÁÅÍÏÇÏ ËÁÎÁÌÁ ÁÄÁÐÔÅÒÁ */
	short	flag_restore; 		/* ÐÒÉÚÎÁË ÏÐÅÒÁÃÉÉ ×ÏÓÓÔÁÎÏ×ÌÅÎÉÑ õóë áó0 É óëâ ÷õ */
} mcap_halt_channel_data_exchange_t;
#else
typedef struct	halt_channel_data_exche {
	short	flag_restore; 		/* ÐÒÉÚÎÁË ÏÐÅÒÁÃÉÉ ×ÏÓÓÔÁÎÏ×ÌÅÎÉÑ õóë áó0 É óëâ ÷õ */
	short	halt_channel_exchange; 	/* ÎÏÍÅÒ ÏÓÔÁÎÁ×ÌÉ×ÁÅÍÏÇÏ ËÁÎÁÌÁ ÁÄÁÐÔÅÒÁ */
} mcap_halt_channel_data_exchange_t;
#endif /* MY_DRIVER_BIG_ENDIAN */

/* ïÔËÌÀÞÉÔØ ËÁÎÁÌÙ ÏÔ ÌÉÎÉÊ Ó×ÑÚÉ (mcap_turn_off_channels_task) */
#ifdef MY_DRIVER_BIG_ENDIAN
typedef struct	turn_ch {
	short	mode_functional_monitoring; /* ÒÅÖÉÍ áæë */
	short   unused;
} mcap_turn_off_channels_t;
#else
typedef struct	turn_ch {
	short   unused;
	short	mode_functional_monitoring; /* ÒÅÖÉÍ áæë */
} mcap_turn_off_channels_t;
#endif/* MY_DRIVER_BIG_ENDIAN */

/* ÷ÙÄÁÞÁ ÐÒÅÒÙ×ÁÎÉÑ ÐÏÌØÚÏ×ÁÔÅÌÀ */
#ifdef MY_DRIVER_BIG_ENDIAN
typedef struct reveal_result {
	u_short	channel_num;			 /* ÎÏÍÅÒ ËÁÎÁÌÁ */
	u_short	event_intr;			 /* ËÏÄ ÓÏÂÙÔÉÑ */
} reveal_result_t;
#else
typedef struct reveal_result {
	u_short	event_intr;			 /* ËÏÄ ÓÏÂÙÔÉÑ */
	u_short	channel_num;			 /* ÎÏÍÅÒ ËÁÎÁÌÁ */
} reveal_result_t;
#endif /* MY_DRIVER_BIG_ENDIAN */

/* ðÁÒÁÍÅÔÒÙ ÚÁÄÁÎÉÑ ÄÌÑ ÄÒÁÊ×ÅÒÁ Mð ÏÔ ÄÒÁÊ×ÅÒÁ ÷ë */
typedef union _mp_drv_args_t
{
	init_bufers_exchange_data_t init_buf_exch; 			/* ÉÎÉÃÉÁÌÉÚÁÃÉÑ ÂÕÆÅÒÏ× ÏÂÍÅÎÁ ÄÁÎÎÙÍÉ */
	mcap_halt_channel_data_exchange_t halt_channel_data_exch; 	/* ÏÓÔÁÎÏ× ËÁÎÁÌÁ ÏÂÍÅÎÁ */
	mcap_turn_off_channels_t turn_ch; 			    	/* ÏÔËÌÀÞÅÎÉÅ ËÁÎÁÌÏ× ÏÔ ÌÉÎÉÊ Ó×ÑÚÉ */
	u_int		args_area[35];	/* ÍÁËÓÉÍÁÌØÎÁÑ ÏÂÌÁÓÔØ ÐÁÒÁÍÅÔÒÏ× ÚÁÄÁÎÉÑ ÄÌÑ ÄÒÁÊ×ÅÒÁ Mð */
} mp_drv_args_t;

/* ðÁÒÁÍÅÔÒÙ ÚÁÄÁÎÉÑ ÄÌÑ ÄÒÁÊ×ÅÒÁ ÷ë ÏÔ ÄÒÁÊ×ÅÒÁ Mð */
typedef union _sparc_drv_args_t
{
	mp_init_result_t mp_init_results;			/* ÒÅÚÕÌØÔÁÔÙ ÉÎÉÃÉÁÌÉÚÁÃÉÉ ÄÒÁÊ×ÅÒÁ Mð */
	init_bufers_exchange_data_res_t	init_buf_exch_res;	/* ÒÅÚÕÌØÔÁÔÙ ÉÎÉÃÉÁÌÉÚÁÃÉÉ ÂÕÆÅÒÏ× */
								/* ÏÂÍÅÎÁ ÄÁÎÎÙÍÉ */
	u_int		args_area[15];				/* ÍÁËÓÉÍÁÌØÎÁÑ ÏÂÌÁÓÔØ ÐÁÒÁÍÅÔÒÏ× */
								/* ÚÁÄÁÎÉÊ ÄÒÁÊ×ÅÒÁ ÷ë */
} sparc_drv_args_t;

/* ðÁÒÁÍÅÔÒÙ ÐÒÅÒÙ×ÁÎÉÑ ÏÔ ÄÒÁÊ×ÅÒÁ Mð */
typedef union _intr_drv_args_t
{
	reveal_result_t	reveal_result;	/* ÒÅÚÕÌØÔÁÔ ÐÒÅÒÙ×ÁÎÉÑ ÏÔ ÁÄÁÐÔÅÒÁ */
	u_int		args_area[1];	/* ÍÁËÓÉÍÁÌØÎÁÑ ÏÂÌÁÓÔØ ÐÁÒÁÍÅÔÒÏ× */
} intr_drv_args_t;
/* ó×ÑÚØ ÄÒÁÊ×ÅÒÏ× ÷ë É íð */
typedef struct drv_intercom_t_
{
   mp_task_t         mp_task;       /* ÔÅËÕÝÅÅ ÚÁÄÁÎÉÅ ÄÌÑ ÄÒÁÊ×ÅÒÁ Mð */
   mp_drv_args_t     mp_args;       /* ÐÁÒÁÍÅÔÒÙ ÚÁÄÁÎÉÊ ÄÌÑ ÄÒÁÊ×ÅÒÁ Mð */
   sparc_task_t      sparc_task;    /* ÔÅËÕÝÅÅ ÚÁÄÁÎÉÅ ÄÌÑ ÄÒÁÊ×ÅÒÁ ÷ë */
   sparc_drv_args_t  sparc_args;    /* ÐÁÒÁÍÅÔÒÙ ÚÁÄÁÎÉÑ ÄÌÑ ÄÒÁÊ×ÅÒÁ ÷ë */
   u_int	     flag_mp;       /* ÐÒÉÚÎÁË ÒÁÂÏÔÙ íð */
   intr_task_t       intr_task;     /* ÐÒÅÒÙ×ÁÎÉÅ ÄÌÑ ÄÒÁÊ×ÅÒÁ ÷ë */
   intr_drv_args_t   intr_args;     /* ÐÁÒÁÍÅÔÒÙ ÐÒÅÒÙ×ÁÎÉÑ */
} drv_intercom_t;

/* ïÐÒÅÄÅÌÅÎÉÑ É ÓÔÒÕËÔÕÒÉÒÙ, ÉÓÐÏÌØÚÕÅÍÙÅ ÄÒÁÊ×ÅÒÏÍ É ÐÒÉÌÏÖÅÎÉÑÍÉ ÐÏÌØÚÏ×ÁÔÅÌÑ. */

#if defined(_KERNEL) || defined(_KMEMUSER)

/* Dev_ops ÄÌÑ ÜÔÏÇÏ ÍÏÄÕÌÑ */
/*struct	dev_ops	mcap_dev_ops;*/
static struct file_operations mcap_fops;

/* ïÂÏÂÝÅÎÎÙÅ ÓÔÒÕËÔÕÒÙ ÐÅÒÅÓÙÌÏË É ÒÅÚÕÌØÔÁÔÏ× */

typedef struct dma_struct {
	caddr_t		 prim_buf_addr;
	size_t		 real_size;
        dma_addr_t       busa;          /* Address in the SBus space,*/ 
					/* áÄÒÅÓ ÏÂÌÁÓÔÉ dma ÓÏ ÓÔÏÒÏÎÙ ÕÓÔÒÏÊÓÔ×Á */
    	unsigned long	 mem; 		/* Address in the processor space,*/
					/* áÄÒÅÓ ÏÂÌÁÓÔÉ dma ÓÏ ÓÔÏÒÏÎÙ ÐÒÏÃÅÓÓÏÒÁ */
        int              size;
  } dma_struct_t;

/* óÔÒÕËÔÕÒÁ ÂÕÆÅÒÁ ÐÅÒÅÓÙÌËÉ */
typedef struct trbuf_desc {
	caddr_t			buf_address;	/* ×ÉÒÔÕÁÌØÎÙÊ ÁÄÒÅÓ ÂÕÆÅÒÁ ÐÅÒÅÓÙÌËÉ */
	size_t			buf_size;	/* ÂÁÊÔÏ×ÙÊ ÒÁÚÍÅÒ ÂÕÆÅÒÁ ÐÅÒÅÓÙÌËÉ */
/*	ddi_acc_handle_t	acc_handle;*/	/* ÂÕÆÅÒ ÏÂÒÁÂÏÔËÉ ÄÏÓÔÕÐÁ */
/*	ddi_dma_handle_t	dma_handle;*/ 	/* ÂÕÆÅÒ ÏÂÒÁÂÏÔËÉ DMA */
/*	ddi_dma_cookie_t	cookie;*/	/* ÂÕÆÅÒ DMA ÍÁÒËÅÒÏ× */
/*	uint_t			ccount;*/	/* ÞÉÓÌÏ ÂÕÆÅÒÏ× DMA ÍÁÒËÅÒÏ× */
	dma_struct_t		dma;		/* âÕÆÅÒ, ÏÐÉÓÙ×ÁÀÝÉÊ DMA */
} trbuf_desc_t;

/* ïÐÉÓÁÎÉÅ ÂÕÆÅÒÁ ÐÅÒÅÓÙÌËÉ */
typedef struct trans_buf_ {
	struct trans_buf 	*next_trans_buf;		/* ÕËÁÚÁÔÅÌØ ÓÌÅÄÕÀÝÅÇÏ ÂÕÆÅÒÁ × ÓÐÉÓËÅ */
	trbuf_desc_t		trans_buf_desc; 		/* ÏÐÉÓÁÎÉÅ ÂÕÆÅÒÁ ÐÅÒÅÓÙÌËÉ */
} trans_buf_t;

/* óÔÒÕËÔÕÒÁ ÂÕÆÅÒÁ ÐÅÒÅÓÙÌËÉ.
 âÕÆÅÒ ÓÏÄÅÒÖÉÔ ÂÕÆÅÒÁ ÐÏÌØÚÏ×ÁÔÅÌÅÊ */
typedef struct trbuf_state {
	char		valid_flag;		/* ÄÏÐÕÓÔÉÍÙÊ ÂÕÆÅÒ ÐÅÒÅÓÙÌËÉ */
	trbuf_desc_t	trans_buf_desc; 	/* ÄÅÓËÒÉÐÔÏÒ ÂÕÆÅÒÁ ÐÅÒÅÓÙÌËÉ */
	caddr_t		user_buf_address;	/* ×ÉÒÔÕÁÌØÎÙÊ ÁÄÒÅÓ ÎÁÞÁÌØÎÏÇÏ ÂÕÆÅÒÁ ÐÏÌØÚÏ×ÁÔÅÌÑ */
	size_t		user_buf_size;		/* ÂÁÊÔÏ×ÙÊ ÒÁÚÍÅÒ ÂÕÆÅÒÁ ÐÏÌØÚÏ×ÁÔÅÌÑ */
	int		max_user_buf_num;	/* ÍÁËÓ. ÞÉÓÌÏ ÂÕÆÅÒÏ× ÐÏÌØÚÏ×ÁÔÅÌÑ × ÂÕÆÅÒÅ ÄÒÁÊ×ÅÒÁ */

	caddr_t		user_trans_bufs[MCAP_SUBDEV_BUF_NUM]; /* ÓÐÉÓÏË ÕËÁÚÁÔÅÌÅÊ ÂÕÆÅÒÏ× ÐÅÒÅÓÙÌËÉ ÐÏÌØÚÏ×ÁÔÅÌÑ */
	u_int		dma_trans_bufs[MCAP_SUBDEV_BUF_NUM];  /* ÓÐÉÓÏË dma ÕËÁÚÁÔÅÌÅÊ ÂÕÆÅÒÏ× ÐÅÒÅÓÙÌËÉ
								 ÐÏÌØÚÏ×ÁÔÅÌÑ */
} trbuf_state_t;

/* ÷ÎÕÔÒÅÎÎÅÅ ÓÏÓÔÏÑÎÉÅ ËÁÎÁÌÁ */
typedef struct mcap_chnl_state {
	trbuf_state_t	trans_buf_state;	/* ÓÏÓÔÏÑÎÉÅ ÂÕÆÅÒÁ ÐÅÒÅÓÙÌËÉ */
	char	trans_state_is_init;		/* ÕÓÔÁÎÏ×ËÁ ÓÏÓÔÏÑÎÉÑ ÐÅÒÅÓÙÌËÉ */
	char	state_init_in_progress;		/* ×ÙÐÏÌÎÑÅÔÓÑ ÉÎÉÃÉÁÌÉÚÁÃÉÑ  */
	char	trans_state_is_halt;		/* ÓÏÓÔÏÑÎÉÅ ÐÅÒÅÓÙÌËÉ - ÏÓÔÁÎÏ× */
	char	mp_trans_state_is_halt;		/* ÓÏÓÔÏÑÎÉÅ ÄÒÁÊ×ÅÒÁ - ÏÓÔÁÎÏ× */
	char	all_trans_finish;			/* ×ÓÅ ÐÅÒÅÓÙÌËÉ ÚÁ×ÅÒÛÅÎÙ */
	char	init_as_trans_map;			/* ËÁÎÁÌ ÉÎÉÃÉÁÌÉÚÉÒÏ×ÁÎ × ÒÅÖÉÍÅ ËÁÒÔÙ ÏÂÍÅÎÁ*/
	int	trans_halt_error;			/* ËÏÄ ÏÛÉÂËÉ ÏÓÔÁÎÏ×Á, ÅÓÌÉ ÏÓÔÁÎÏ× ÎÅ ÂÙÌ ÐÒÏÉÚ×ÅÄÅÎ */
	mcap_init_iomap_t	init_iomap_state_spec;	/* ÓÏÓÔÏÑÎÉÅ ÉÎÉÃÉÁÌÉÚÁÃÉÉ ËÁÒÔÙ */
	size_t	full_data_buf_size;			/* ÐÏÌÎÙÊ ÒÁÚÍÅÒ ÂÕÆÅÒÁ ÄÁÎÎÙÈ */
	size_t	subdev_buf_trans_size;		/* ÒÁÚÍÅÒ ÐÅÒÅÄÁÀÝÅÇÏ ÂÕÆÅÒÁ, ×ËÌÀÞÁÑ ÚÁÇÏÌÏ×ÏË */
	size_t	subdev_buf_reciv_size;		/* ÒÁÚÍÅÒ ÐÒÉÅÍÎÏÇÏ ÂÕÆÅÒÁ, ×ËÌÀÞÁÑ ÚÁÇÏÌÏ×ÏË */
	int		dma_intr_handled;			/* ÐÒÅÒÙ×ÁÎÉÅ ÏÂÒÁÂÏÔÁÎÏ ×ÅÒÎÏ */
	u_short		trans_num;				/* ÎÏÍÅÒ ÐÁËÅÔÎÏÊ ÐÅÒÅÓÙÌËÉ */
} mcap_chnl_state_t;

/* ÷ÎÕÔÒÅÎÎÅÅ ÓÏÓÔÏÑÎÉÅ ÄÒÁÊ×ÅÒÁ */
#ifdef MCAP_OLD_VERSION
typedef struct mcap_state {
	dev_info_t		*dip;			/* dip. */
	int			inst;			/* ÎÏÍÅÒ ÜËÚÅÍÐÌÑÒÁ */
	int			opened;			/* ÏÔËÒÙÔÏÅ ÓÏÓÔÏÑÎÉÅ */
	int			open_flags;		/* ÏÔËÒÙÔÏÅ ÓÏÓÔÏÑÎÉÅ Ó ÆÌÁÖËÏÍ */
	u_int			open_channel_map;	/* ÍÁÓËÁ ÏÔËÒÙÔÙÈ ËÁÎÁÌÏ× */
/*********************************************************************************************************/
	raw_spinlock_t		lock;
	kcondvar_t		channel_cv;		/* ÐÅÒÅÍÅÎÎÁÑ ÕÓÌÏ×ÉÊ (ÒÅÖÉÍÁ, ÓÏÓÔÏÑÎÉÊ) ËÁÎÁÌÁ */
	kcondvar_t		drv_comm_cv;		/* ÏÂÌÁÓÔØ Ó×ÑÚÉ ÄÒÁÊ×ÅÒÁ: ÚÁÎÑÔÁÑ ÉÌÉ
								Ó×ÏÂÏÄÎÁÑ, ÐÅÒÅÍÅÎÎÁÑ ÕÓÌÏ×ÉÑ */
	kcondvar_t		trans_state_cv; 	/* ÓÏÓÔÏÑÎÉÅ ËÁÎÁÌÁ ÐÅÒÅÓÙÌËÉ,
								ÉÚÍÅÎÅÎÉÅ ÐÅÒÅÍÅÎÎÏÊ ÕÓÌÏ×ÉÑ */
	kcondvar_t		intr_cv;		/* ÐÅÒÅÍÅÎÎÁÑ ÕÓÌÏ×ÉÊ ÄÌÑ ÐÒÅÒÙ×ÁÎÉÑ */
/*********************************************************************************************************/

/*	ddi_iblock_cookie_t	iblock_cookie;*/	/* ÄÌÑ mutexes. */
/*	struct pollhead		pollhead;*/		/* ÇÌÕÈÁÑ ÓÔÒÕËÔÕÒÁ ÄÌÑ ÏÐÒÏÓÁ */
	int			drv_comm_busy;		/* ÐÒÉÚÎÁË ÚÁÎÑÔÏÓÔÉ ÏÂÌÁÓÔÉ
									   Ó×ÑÚÉ ÄÒÁÊ×ÅÒÁ */
	int       		drv_general_modes;	/* ÏÂÝÉÅ ÐÒÉÚÎÁËÉ ÒÅÖÉÍÏ× ÄÒÁÊ×ÅÒÁ */
	e90_unit_t		type_unit;		/* ÔÉÐ ÐÌÁÔÙ */
	char			intr_seted;		/* ÐÒÅÒÙ×ÁÎÉÅ ÕÓÔÁÎÏ×ÌÅÎÏ */
	char			intr_number;		/* ÞÉÓÌÏ ÐÒÅÒÙ×ÁÎÉÊ */
	int			system_burst;		/* DMA ÒÁÚÍÅÒÙ ÐÁÞËÉ, ÐÏÚ×ÏÌÅÎÎÙÅ SBUS */
	char			mp_drv_loaded;		/* MP ÄÒÁÊ×ÅÒ ÚÁÇÒÕÖÅÎ */
	char			mp_debug_drv_flag;	/* debug driver startuped flag */
	char			mp_rom_drv_enable;	/* MP ROM ÄÒÁÊ×ÅÒ Ñ×ÌÑÅÔÓÑ
									   ÒÁÚÒÅÛÁÀÝÉÍ ÐÒÉÚÎÁËÏÍ */
	mp_state_t		mp_state;		/* ÔÅËÕÝÅÅ ÓÏÓÔÏÑÎÉÅ Mð */
	char			mp_drv_started;		/* MP ÄÒÁÊ×ÅÒ ÚÁÐÕÝÅÎ */
	char			set_tlrm;		/* ???? ÂÌÏËÉÒÏ×ËÁ ÕÓÔÁÎÏ×ËÉ ÓÂÒÏÓÁ
									   ÍÏÄÕÌÑ ÐÏ ÏÛÉÂËÅ  */
	bmem_trans_desk_t	mp_init_code;		/* ÄÅÓËÒÉÐÔÏÒ ÚÁÐÕÓËÁ ËÏÄÁ Mð */
	
	char						/* ÚÁÐÕÓË ËÏÄÁ Mð */
				mp_init_area_copy[ME90_MP_INIT_AREA_BMEM_SIZE];
	mp_drv_args_t					/* ÉÎÆÏÒÍÁÃÉÑ ÉÎÉÃÉÁÌÉÚÁÃÉÉ ÄÒÁÊ×ÅÒÁ Mð */
				mp_drv_init_info;
	volatile caddr_t	MCAP_BMEM;		/* ÂÁÚÏ×ÙÊ ÁÄÒÅÓ âïúõ */
	mcap_chnl_state_t               		/* ÓÏÓÔÏÑÎÉÅ ËÁÎÁÌÁ ÐÌÁÔÙ */
				channel_state[1];
/*	ddi_acc_handle_t	acc_regs;*/  		/* ÕËÁÚÁÔÅÌØ ÎÁ ÄÅÓËÒÉÐÔÏÒ */
							/* ÄÏÓÔÕÐÁ Ë ÒÅÇÉÓÔÒÁÍ */
	caddr_t			regs_base; 		/* ÂÁÚÏ×ÙÊ ÁÄÒÅÓ ÒÅÇÉÓÔÒÏ× */
	off_t			reg_array_size;   	/* ÒÁÚÍÅÒ ×ÙÄÅÌÅÎÎÏÊ ÏÂÌÁÓÔÉ */
										  /* ÒÅÇÉÓÔÒÏ× */
	u_short			io_flags_intr; 		/* ÐÒÉÚÎÁË ÎÁÌÉÞÉÑ ÐÒÅÒÙ×ÁÎÉÑ */
	u_short			event_intr_trans_ch[MCAP_SUBDEV_BUF_NUM];
							/* ËÏÄ ÓÏÂÙÔÉÑ ÐÅÒÅÄÁÀÝÉÈ ËÁÎÁÌÏ× */
	u_short			event_intr_reciv_ch[MCAP_SUBDEV_BUF_NUM];
							/* ËÏÄ ÓÏÂÙÔÉÑ ÐÒÉÅÍÎÙÈ ËÁÎÁÌÏ× */
	hrtime_t		time_get_intr_dev; 	/* ô ÐÏÌÕÞÅÎÉÑ ÐÒÅÒÙ×ÁÎÉÑ ÏÔ ÁÄÁÐÔÅÒÁ */

} mcap_state_t;
#else
typedef struct mcap_state {
	dev_info_t		*dip;			/* dip. */
	int			inst;			/* ÎÏÍÅÒ ÜËÚÅÍÐÌÑÒÁ */
	int			opened;			/* ÏÔËÒÙÔÏÅ ÓÏÓÔÏÑÎÉÅ */
	int			open_flags;		/* ÏÔËÒÙÔÏÅ ÓÏÓÔÏÑÎÉÅ Ó ÆÌÁÖËÏÍ */
	u_int			open_channel_map;	/* ÍÁÓËÁ ÏÔËÒÙÔÙÈ ËÁÎÁÌÏ× */
/*********************************************************************************************************/
	raw_spinlock_t		lock;
	kcondvar_t		channel_cv;		/* ÐÅÒÅÍÅÎÎÁÑ ÕÓÌÏ×ÉÊ (ÒÅÖÉÍÁ, ÓÏÓÔÏÑÎÉÊ) ËÁÎÁÌÁ */
	kcondvar_t		drv_comm_cv;		/* ÏÂÌÁÓÔØ Ó×ÑÚÉ ÄÒÁÊ×ÅÒÁ: ÚÁÎÑÔÁÑ ÉÌÉ
								Ó×ÏÂÏÄÎÁÑ, ÐÅÒÅÍÅÎÎÁÑ ÕÓÌÏ×ÉÑ */
	kcondvar_t		trans_state_cv; 	/* ÓÏÓÔÏÑÎÉÅ ËÁÎÁÌÁ ÐÅÒÅÓÙÌËÉ,
								ÉÚÍÅÎÅÎÉÅ ÐÅÒÅÍÅÎÎÏÊ ÕÓÌÏ×ÉÑ */
	kcondvar_t		intr_cv;		/* ÐÅÒÅÍÅÎÎÁÑ ÕÓÌÏ×ÉÊ ÄÌÑ ÐÒÅÒÙ×ÁÎÉÑ */
/*********************************************************************************************************/

/*	ddi_iblock_cookie_t	iblock_cookie;*/	/* ÄÌÑ mutexes. */
/*	struct pollhead		pollhead;*/		/* ÇÌÕÈÁÑ ÓÔÒÕËÔÕÒÁ ÄÌÑ ÏÐÒÏÓÁ */
	int			drv_comm_busy;		/* ÐÒÉÚÎÁË ÚÁÎÑÔÏÓÔÉ ÏÂÌÁÓÔÉ
							   Ó×ÑÚÉ ÄÒÁÊ×ÅÒÁ */
	e90_unit_t		type_unit;		/* ÔÉÐ ÐÌÁÔÙ */
	char			intr_seted;		/* ÐÒÅÒÙ×ÁÎÉÅ ÕÓÔÁÎÏ×ÌÅÎÏ */
	char			intr_number;		/* ÞÉÓÌÏ ÐÒÅÒÙ×ÁÎÉÊ */
	int			system_burst;		/* DMA ÒÁÚÍÅÒÙ ÐÁÞËÉ, ÐÏÚ×ÏÌÅÎÎÙÅ SBUS */
	char			mp_drv_loaded;		/* MP ÄÒÁÊ×ÅÒ ÚÁÇÒÕÖÅÎ */
	char			mp_debug_drv_flag;	/* debug driver startuped flag */
	char			mp_rom_drv_enable;	/* MP ROM ÄÒÁÊ×ÅÒ Ñ×ÌÑÅÔÓÑ
									   ÒÁÚÒÅÛÁÀÝÉÍ ÐÒÉÚÎÁËÏÍ */
	mp_state_t		mp_state;		/* ÔÅËÕÝÅÅ ÓÏÓÔÏÑÎÉÅ Mð */
	char			mp_drv_started;		/* MP ÄÒÁÊ×ÅÒ ÚÁÐÕÝÅÎ */
	mcap_bmem_trans_desk_t  mp_init_code;           /* ÄÅÓËÒÉÐÔÏÒ ÚÁÐÕÓËÁ ËÏÄÁ Mð */
                                                        /* ÚÁÐÕÓË ËÏÄÁ Mð */

	char			mp_init_area_copy[ME90_MP_INIT_AREA_BMEM_SIZE];
	mp_drv_args_t		mp_drv_init_info;	/* ÉÎÆÏÒÍÁÃÉÑ ÉÎÉÃÉÁÌÉÚÁÃÉÉ ÄÒÁÊ×ÅÒÁ Mð */
	volatile caddr_t	MCAP_BMEM;		/* ÂÁÚÏ×ÙÊ ÁÄÒÅÓ âïúõ */
	mcap_chnl_state_t               		/* ÓÏÓÔÏÑÎÉÅ ËÁÎÁÌÁ ÐÌÁÔÙ */
				channel_state[1];
/*	ddi_acc_handle_t	acc_regs;*/  		/* ÕËÁÚÁÔÅÌØ ÎÁ ÄÅÓËÒÉÐÔÏÒ */
							/* ÄÏÓÔÕÐÁ Ë ÒÅÇÉÓÔÒÁÍ */
	caddr_t			regs_base; 		/* ÂÁÚÏ×ÙÊ ÁÄÒÅÓ ÒÅÇÉÓÔÒÏ× */
	off_t			reg_array_size;   	/* ÒÁÚÍÅÒ ×ÙÄÅÌÅÎÎÏÊ ÏÂÌÁÓÔÉ */
										  /* ÒÅÇÉÓÔÒÏ× */
	u_short			io_flags_intr; 		/* ÐÒÉÚÎÁË ÎÁÌÉÞÉÑ ÐÒÅÒÙ×ÁÎÉÑ */
	u_short			event_intr_trans_ch[MCAP_SUBDEV_BUF_NUM];
							/* ËÏÄ ÓÏÂÙÔÉÑ ÐÅÒÅÄÁÀÝÉÈ ËÁÎÁÌÏ× */
	u_short			event_intr_reciv_ch[MCAP_SUBDEV_BUF_NUM];
							/* ËÏÄ ÓÏÂÙÔÉÑ ÐÒÉÅÍÎÙÈ ËÁÎÁÌÏ× */
	hrtime_t		time_get_intr_dev; 	/* ô ÐÏÌÕÞÅÎÉÑ ÐÒÅÒÙ×ÁÎÉÑ ÏÔ ÁÄÁÐÔÅÒÁ */
	u_short         	number_intr_rosh;       /* ËÏÌ-×Ï ÐÒÅÒÙ×ÁÎÉÊ ÐÏ òïû */
} mcap_state_t;
#endif /* MCAP_OLD_VERSION */

/* íÁËÒÏËÏÍÁÎÄÙ ÄÌÑ ÏÂÒÁÝÅÎÉÑ Ë ÒÅÇÉÓÔÒÁÍ */
/* ÷ÉÒÔÕÁÌØÎÙÊ ÁÄÒÅÓ ÒÅÇÉÓÔÒÁ */
#define	MCAP_REGISTER_ADDR(state, reg)	((ulong_t *)(state->regs_base + reg))
/* þÔÅÎÉÑ ÓÏÄÅÒÖÉÍÏÇÏ ÒÅÇÉÓÔÒÁ */
#define	READ_MCAP_REGISTER(state, reg)	ddi_getl(state->dip->dev_type/*acc_regs*/, MCAP_REGISTER_ADDR(state, reg))
/* úÁÐÉÓØ × ÒÅÇÉÓÔÒ */
#define	WRITE_MCAP_REGISTER(state, reg, v)	ddi_putl(state->dip->dev_type/*acc_regs*/, MCAP_REGISTER_ADDR(state, reg), v)

static int
mcap_ioctl(struct inode *inode, struct file *file,
                 unsigned int cmd, unsigned long arg);

static int mcap_open(struct inode *inode, struct file *file);

static int mcap_mmap(struct file *file, struct vm_area_struct *vma);

static int mcap_close(struct inode *inode, struct file *file);

static int  mcap_attach(dev_info_t  *dip);

int mcap_detach(dev_info_t *dip);

irqreturn_t mcap_intr_handler(int irq, void *arg, struct pt_regs *regs);

static int __init mcap_init(void);

static void __exit mcap_exit(void);

int mcap_attach_add(mcap_state_t *state, int *add_attach_flags);

void mcap_detach_add(mcap_state_t *state, int add_attach_flags, int uncondit_detach);

void Unmap_reg_sets(mcap_state_t	*state);

int rmv_dev(dev_info_t *dip, int channel);

int mcap_get_channel_to_init(
	mcap_state_t			*state,
	int				waiting_time,
	int				drv_comm_area_locked,
	int				user_request,
	int				state_recover);

void mcap_free_channel_to_init(mcap_state_t *state, int	mutex_locked);

int mcap_init_trans_map_state(
	mcap_state_t		*state,
	mcap_init_iomap_t	*init_state_args,
	int					drv_comm_area_locked,
	int					*error_code,
	int					state_recover);

int mcap_create_drv_iomap_buf(mcap_state_t	*state);

void mcap_delete_drv_trans_buf(mcap_state_t	*state);

void mcap_init_trans_buf_desc(
	trbuf_desc_t	*trans_buf_desc);

int mcap_halt_trans_state(
	mcap_state_t *		state,
	mcap_halt_trans_t	*halt_trans_state,
	int			drv_comm_area_locked,
	int			user_request,
	int			mutex_locked);

int mcap_wait_for_trans_state_halt(
	mcap_state_t 		*state,
	int			waiting_time);

int mcap_halt_transfers(
	mcap_state_t 		*state,
	int			waiting_time,
#ifdef MCAP_OLD_VERSION
	int			delete_rem_trans,
	int			mutex_locked,
#endif /* MCAP_OLD_VERSION */
	int			drv_comm_area_locked);

void mcap_init_subdev_buf(
	mcap_state_t		*state,
	mcap_iosubdbuf_t	*subdev_buf,
	int			io_flags,
	size_t			max_data_buf_size,
	int			subdev_buf_num);

void mcap_init_iomap_buf(
	mcap_state_t		*state,			 /* ÓÏÂÓÔ×ÅÎÎÁÑ ÉÎÆÏÒÍÁÃÉÑ ÄÒÁÊ×ÅÒÁ */
	mcap_iosubdbuf_t	*iomap_buf_desc, 	 /* ÄÅÓËÒÉÐÔÏÒ ÂÕÆÅÒÁ ÏÂÍÅÎÁ */
	size_t			subdev_buf_trans_size,   /* ÍÁËÓÉÍÁÌØÎÙÊ ÒÁÚÍÅÒ */
							 /* ÂÕÆÅÒÁ ÐÅÒÅÄÁÞÉ */
	size_t			subdev_buf_reciv_size,   /* ÍÁËÓÉÍÁÌØÎÙÊ ÒÁÚÍÅÒ */
							 /* ÂÕÆÅÒÁ ÐÒÉÅÍÁ */
	int			iomap_buf_num);

int mcap_start_task_drv_mp(
	mcap_state_t 		*state,
	mp_task_t		mp_task,
	mp_drv_args_t 		*task_args,
	sparc_drv_args_t	*mp_task_results
	);

int mcap_wait_make_task_drv_mp(
	mcap_state_t		*state,
	int			mp_restart,
	int			wait_mp_task_accept,
	int			wait_mp_rom_drv_disable);

int mcap_reset_general_regs(
	mcap_state_t		*state,
	int			mp_state);

void mcap_read_general_regs(
	mcap_state_t	*state,
	int		flaf_print);

int   mcap_calculate_work_hr_time(
	hrtime_t    start_time,             /* event start time */
	hrtime_t    end_time                /* event finish time */
	);

int  mcap_bmem_data_transfer(
	 mcap_state_t		*state,
#ifdef MCAP_OLD_VERSION
	 bmem_trans_desk_t	*transfer_desk,
#else
	 mcap_bmem_trans_desk_t *transfer_desk,
#endif /* MCAP_OLD_VERSION */
	 int			write_op,
	 int			char_data,
	 caddr_t		kmem_buf,
	 caddr_t		*kmem_area_p);

int mcap_alloc_trans_bufs(
	mcap_state_t	*state,
	trbuf_desc_t	*new_trans_buf,
	int		buf_byte_size);

void mcap_free_trans_bufs(
	mcap_state_t	*state,
	trbuf_desc_t	*trans_buf_desc);

int	mcap_write_base_memory(
	mcap_state_t	*state,
	caddr_t		address_from,
	caddr_t		address_to,
	size_t		byte_size,
	int		char_data);

u_int	mcap_rotate_word_bytes(u_int	source_word);

int mcap_map_registers(
	mcap_state_t	*state,
	e90_unit_t	type_unit);

int   mcap_startup_mp(
	mcap_state_t		*state,
	int			cmd);

int mcap_reset_module(
	mcap_state_t	*state,
	int		operation,
	int		clean_bmem);

void mcap_clean_base_memory(mcap_state_t	*state);

void mcap_clean_drv_communication(mcap_state_t	*state);

int mcap_ioctl(struct inode *inode, struct file *file,
                 unsigned int cmd, unsigned long arg);

void mcap_init_drv_state(mcap_state_t	*state);

void mcap_init_trans_buf_state(
	trbuf_state_t	*trans_buf_state);


#define	MCAP_DRV_COMM_FREE_TIMEOUT_DEF_VALUE	(1000000)
#define MCAP_TASK_ACCEPT_BY_MP_TIME		(100000)		/* usec */
/*#define MCAP_TASK_ACCEPT_BY_MP_TRYON		(1000)	ÉÓËÌÀÞ£Î */
#define MCAP_TASK_ACCEPT_BY_MP_DELAY_TIME	(1000)			/* usec */

/* ìÏËÁÌØÎÙÅ ÏÐÒÅÄÅÌÅÎÉÑ */

#define MCAP_DEVN(d)	(getminor(d))		/* dev_t -> minor (dev_num) */
#define MCAP_inst(m)	(m >> 4)		/* minor -> instance */
#define MCAP_chan(m)	(m & 0xf)		/* minor -> channel */
#define MCAP_MINOR(i,c)	((i << 4) | (c))	/* instance+channel -> minor */
#define MCAP_INST(d)	MCAP_inst(MCAP_DEVN(d))	/* dev_t -> instance */
#define MCAP_CHAN(d)	MCAP_chan(MCAP_DEVN(d))
#define	CHNL_NUM_TO_MASK(chnl)		(1 << chnl)

/* òÁÚÒÑÄÎÙÅ ÐÏÌÑ ÄÌÑ attach_flags: */

#define SOFT_STATE_ALLOCATED		0x0001
#define INTERRUPT_ADDED			0x0002
#define MUTEX_ADDED			0x0004
#define CHANNEL_CV_ADDED		0x0008
#define REGS_MAPPED			0x0010
#define MINOR_NODE_CREATED		0x0020
#define IOPB_ALLOCED			0x0040
#define ERRORS_SIGN			0x0080
#define IBLOCK_COOKIE_ADDED		0x0200
#define	INTR_IBLOCK_COOKIE_ADDED	0x0400
#define	INTR_MUTEX_ADDED		0x0800
#define	TRANS_HALTED_CV_ADDED		0x1000
#define	CNCT_POLLING_CV_ADDED		0x2000
#define	TRANS_STATE_CV_ADDED		0x4000

/* éÍÅÎÁ ÒÅË×ÉÚÉÔÏ× ÕÓÔÒÏÊÓÔ×Á */

#define SBUS_INTR_L_NAME_OF_PROP 	"interrupts"

#endif	/* defined(_KERNEL) || defined(_KMEMUSER) */

#ifdef	__cplusplus
}
#endif

#endif	/* __MCAP_H__ */
