/*
 * Copyright (c) 1998 by MCST.
 */

/*
 * E90 modules shared interface matching
 */


#ifndef	__LINUX_MCKP_MATCH_H__
#define	__LINUX_MCKP_MATCH_H__

#include "linux_mckp.h"
#include <linux/mcst/linux_me90_match.h>

typedef	mckp_state_t			me90drv_state_t;
typedef	mckp_chnl_state_t		me90drv_chnl_state_t;
typedef mckp_state_t			mcb_state_t;

#endif	/* __LINUX_MCKP_MATCH_H__ */
